OOMAster

Account:

	api/register/ - accepts POST request(username/email/password), for user registration. After registration an email with activation link is sent top user's email address.
	api/password_reset/ - accepts POST request, for password renewal. After that the user receives an email with link to password reset page.
	api/reset/<token>/ - accepts POST request with new password.
	api/register/activate/<token>/ - After the user makes GET(most likely from mail) request to this URL, the activation occurs.
	api/login/ - POST request returns token for user.
	api/users/ - GET request returns the list of all users. POST request creates new user.
	api/users/<pk> - GET request returns user details. Email and password required.
	api/resend_activation_key/ - accepts POST request(email). After successfully request an email with activation link is sent to top email address.
	api/password_change/ - accepts POST request(current pswd, new pswd, confirm new pswd). Only for authorized users.

Supported next filters:

api/users

	?username=<text> - returns a list of users who have an username that contains a variable text.
	?email=<text> - returns a list of users who have an email that contains a variable text.


Profiles:

	api/profiles - GET request returns list if all Profile.
	api/profiles/<pk> - GET request returns Profile details. POST update Profile.
	api/images - GET request returns list if all images. POST create image.
	api/images/<pk> - GET request returns image details.
	api/teacher-information - GET request returns list if all informations.
	api/teacher-information/<pk> - GET request returns information details.
	api/portfolios - GET request returns list if all portfolios. POST create portfolio.
	api/portfolios/<pk> - GET request returns porfolio details. POST update portfolio.
	api/teacher-subjects - GET request returns list if all subjects.
	api/teacher-subjects/<pk> - GET request returns subject details. POST update subject.
	api/certificates - GET request returns list if all certificates.
	api/certificates/<pk> - GET request returns certificate details. POST update subject.

Supported next filters:

api/profiles
	?user=<userID> - returns a list of profiles who have an user.
	?is_teacher=<True/False> - returns a list of profiles who have is teacher.
api/avatars
	?user=<userID> - returns a list of avatar who have an user.
	?profile=<userID> - returns a list of avatar who have an profile.
api/teacher-information
	?user=<userID> - request returns the list of the user.
api/portfolios
	?user=<userID> - request returns the list of the user.
	?teaching_subject=<teaching_subjectID> - request returns the list of the user.
	?title=<text>
api/certificates
	?user=<userID> - request returns the list of the user.
	?teaching_subject=<teaching_subjectID> - request returns the list of the user.
	?title=<text>
api/teacher-subjects
	?user=<userID> - request returns the list of the user.


Location:

	api/countries - GET request returns list if all countries.
	api/countries/<pk> - GET request returns country details.
	api/cities - GET request returns the list of all cities.
	api/cities/<pk> - GET request returns city details.

Supported next filters:

api/cities
    	?country_id=<countryId> - request returns the list of the cities in this country.


FlatPage:

    	api/flat-page - GET request returns the list of all flatpage.
	api/flat-page/<pk> - GET request returns flatpage details.

Notification settings:

	api/notification-settings - GET request returns list if all settings.
	api/notification-settings/<pk> - GET request returns setting details.
	api/notification-user-settings - GET request returns the list of all user settings.
	api/notification-user-settings/<pk> - GET request returns user setting details.

	type = 0 - web
	type = 1 - mob

Supported next filters:

api/notification-user-settings
	?user=<userID> - request returns the list of the user.
	?mode=[True|False] - request returns the list of the user settings in this mode.

Notification:

	api/notifications - GET request returns list if all notification.
	api/notifications/<pk> - GET request returns notification details.
	api/notification-users - GET request returns the list of all user notifications.
	api/notification-users/<pk> - GET request returns user notification details.


CourseRequest:

	api/course-requests - GET request returns list if all course request. POST create new request
	api/course-requests/<pk> - GET request returns course request details.


Earning:

	api/balances - GET request returns balanse of user.
	api/course-earning - GET request returns course earning of user.
	api/month-earning - GET request returns month earning of user.
	api/withdrawals - GET request returns withdrawals of user.

Supported next order:

api/month-earning
    	?ordering=created
	?ordering=-created
	?ordering=earnings
	?ordering=-earnings
	?ordering=course
	?ordering=-course
api/course-earning
    	?ordering=course__title
	?ordering=-course__title
	?ordering=earnings
	?ordering=-earnings
	?ordering=allowed_withdrawal
	?ordering=-allowed_withdrawal
	?ordering=withdrawal
	?ordering=-withdrawal
