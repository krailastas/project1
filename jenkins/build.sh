#!/bin/bash

DB_USER="jenkins"
DB_NAME="oomaster"
SETTINGS_MODULE="oomaster.settings.jenkins"
SETTINGS_DIR="oomaster/oomaster/settings"
MANAGE_PY_DIR="oomaster"

# Build virtualenv
if [ -d ".env" ]; then
  echo "**> virtualenv exists"
else
  echo  "**> creating virtualenv"
  virtualenv .env
fi

# Vitrualenv activation
source .env/bin/activate
# Installig requirements
.env/bin/pip install -U pip setuptools
.env/bin/pip install --exists-action=s -r jenkins/requirements.txt

# Copy jenkins settings from example file
echo "**>copy jenkins.py"
cp ./jenkins/jenkins.py.example $SETTINGS_DIR/jenkins.py

RESULT=`psql --list | grep $DB_NAME`

if [ -n "$RESULT" ] ; then
  echo "drop database $DB_NAME"
  dropdb $DB_NAME
  echo "create database $DB_NAME"
  createdb $DB_NAME
else
  echo "create database $DB_NAME"
  createdb $DB_NAME
fi

export DJANGO_SETTINGS_MODULE=$SETTINGS_MODULE

find . -name "*.pyc" -exec rm -f {} \;

cd $MANAGE_PY_DIR
python manage.py syncdb
python manage.py migrate cities_light
python manage.py migrate
python manage.py jenkins --enable-coverage
