Account End-point:

api/register/ - accepts POST request(username/email/password), for user registration. After registration an email with activation link is sent top user's email address.

api/password_reset/ - accepts POST request, for password renewal. After that the user receives an email with link to assword reset page.

api/reset/(<uid>/(<token>/ - accepts POST request with new password.

api/register/activate/<token>/ - After the user makes GET(most likely from mail) request to this URL, the activation occurs.

api/login/ - POST request returns token for user.

api/users/ - GET request returns the list of all users. POST request creates new user.

api/users/<pk> - GET request returns user details. Email and password required.

api/resend_activation_key/ - accepts POST request(email). After successfully request an email with activation link is sent to top email address.

api/flat-page/ - GET request returns the list of all flatpage.
api/flat-page/<pk> - GET request returns flatpage details.


NotificationSettings

api/notification-settings - GET request returns the list of all notification settings.
api/notification-settings/<pk> - GET request returns notification setting details.

Supported next filters:

api/notification-settings
    ?slug=<text> - request returns the list of the settings in this slug.

api/notification-user-settings/ - GET request returns the list of all notification user settings.
api/notification-user-settings/<pk> - GET request returns notification user setting details.

Supported next filters:

api/notification-user-settings?mode=[True|False] - request returns the list of the user settings in this mode.
