# encoding: utf-8
from __future__ import unicode_literals

from rest_framework.viewsets import GenericViewSet
from rest_framework import mixins
from rest_framework.permissions import IsAuthenticated

from course_request.models import CourseRequest, MessageRequest
from course_request.permissions import IsCourseRequestPermission, IsMessageRequestViewPermission
from course_request.serializers import CourseRequestSerializer, CourseRequestLikedSerializer, MessageRequestSerializer


class CourseRequestView(mixins.CreateModelMixin,
                        mixins.RetrieveModelMixin,
                        mixins.DestroyModelMixin,
                        mixins.ListModelMixin,
                        GenericViewSet):
    """
    **CourseRequest View**

        Return list of course requests.
        Create new course request.

        **returns:**

        * `user`
        * `request_title`
        * `course_request`
        * `country`
        * `category`
        * `description`
        * `liked`

        **filters:**

        * `?user=<int>`
        * `?city=<int>`
        * `?category=<int>`
    """
    queryset = CourseRequest.objects.filter(is_active=True, is_pre_active=False).select_related().prefetch_related(
        'liked')
    serializer_class = CourseRequestSerializer
    permission_classes = (IsCourseRequestPermission,)
    filter_fields = ('category', 'city', 'user', )
    ordering_fields = '__all__'

    def pre_save(self, obj):
        obj.user = self.request.user


class CourseRequestLikedView(mixins.RetrieveModelMixin,
                             mixins.UpdateModelMixin,
                             mixins.ListModelMixin,
                             GenericViewSet):
    """
    **CourseRequestLiked View**

        Return list of course requests.
        Update new course request.

        **returns:**

        * `user`
        * `request_title`
        * `course_request`
        * `country`
        * `category`
        * `description`
        * `liked`

        **filters:**

        * `?user=<int>`
        * `?city=<int>`
        * `?category=<int>`
    """
    queryset = CourseRequest.objects.filter(is_active=True, is_pre_active=False).prefetch_related('liked')
    serializer_class = CourseRequestLikedSerializer
    permission_classes = (IsAuthenticated,)
    filter_fields = ('category', 'city', 'user', )
    ordering_fields = '__all__'

    def post_save(self, obj, created=False):
        obj.save()


class MessageRequestView(mixins.CreateModelMixin,
                         mixins.RetrieveModelMixin,
                         mixins.ListModelMixin,
                         GenericViewSet):
    """
    **MessageRequest View**

        Return list of messages.
        Create new message.

        **returns:**

        * `user`
        * `course_request`
        * `message`

        **filters:**

        * `?user=<int>`
        * `?course_request=<int>`
    """
    queryset = MessageRequest.objects.select_related()
    serializer_class = MessageRequestSerializer
    permission_classes = (IsMessageRequestViewPermission,)
    filter_fields = ('course_request', 'user', )
    ordering_fields = '__all__'

    def pre_save(self, obj):
        obj.user = self.request.user
