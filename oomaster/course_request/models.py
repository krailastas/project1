# encoding: utf-8
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel
from cities_light.models import Country, City
from course.models import Course

from profiles.models import StudySubject, User
from common.tasks import CourseRequestTask


class CourseRequest(TimeStampedModel):
    """
    **CourseRequest Model**

        **list fields:**

        * `user` - ForeignKey - User
        * `request_title` - CharField
        * `city` - ForeignKey - cities_light.City
        * `country` - ForeignKey - cities_light.Country
        * `category` - ForeignKey - StudySubject
        * `description` - TextField
        * `liked` - ManyToManyField - User
        * `is_active` - BooleanField
        * `is_pre_active` - BooleanField
        * `course` - ForeignKey - Course
    """
    user = models.ForeignKey(User, verbose_name=_('user'), related_name='user_request')
    request_title = models.CharField(verbose_name=_('request title'), max_length=255)
    category = models.ForeignKey(StudySubject, verbose_name=_('category'), related_name='category_requests', blank=True,
                                 null=True)
    city = models.ForeignKey(City, verbose_name=_('city'), blank=True, null=True)
    country = models.ForeignKey(Country, verbose_name=_('country'), blank=True, null=True)
    description = models.TextField(verbose_name=_('description'))
    liked = models.ManyToManyField(User, verbose_name=_('liked'), related_name='liked_user', blank=True, null=True)
    is_active = models.BooleanField(verbose_name=_('active'), default=True)
    is_pre_active = models.BooleanField(verbose_name=_('pre_active'), default=False)
    course = models.ForeignKey(Course, verbose_name=_('course'), blank=True, null=True)

    class Meta:
        verbose_name = _('CourseRequest')
        verbose_name_plural = _('CourseRequests')

    def __str__(self):
        return self.request_title

    def save(self, *args, **kwargs):
        if self.is_pre_active and self.is_active:
            CourseRequestTask().delay(self)
        if self.course:
            self.is_active = False
        super(CourseRequest, self).save(*args, **kwargs)


class MessageRequest(TimeStampedModel):
    """
    **MessageRequest Model**

        **list fields:**

        * `user` - ForeignKey - User
        * `course_request` - ForeignKey - CourseRequest
        * `message` - TextField
    """
    user = models.ForeignKey(User, verbose_name=_('user'), related_name='message_request_user')
    course_request = models.ForeignKey(CourseRequest, verbose_name=_('request course'), related_name='course_requests')
    message = models.TextField(verbose_name=_('message'))

    class Meta:
        verbose_name = _('MessageRequest')
        verbose_name_plural = _('MessageRequests')

    def __str__(self):
        return self.message