# encoding: utf-8
from __future__ import unicode_literals

import factory
from django.contrib.webdesign import lorem_ipsum
from cities_light.models import City, Country

from profiles.models import User, StudySubject
from profiles.factories import UserFactory, StudySubjectFactory
from course_request.models import CourseRequest, MessageRequest
from custom_cities_light.factories import CityFactory, CountryFactory


class CourseRequestFactory(factory.DjangoModelFactory):
    class Meta:
        model = CourseRequest

    @factory.lazy_attribute
    def user(self):
        if not User.objects.all().exists():
            UserFactory.create_batch(5)
        return User.objects.all().order_by('?')[0]

    @factory.lazy_attribute
    def request_title(self):
        return lorem_ipsum.words(2, False)

    @factory.lazy_attribute
    def city(self):
        if not City.objects.all().exists():
            CityFactory.create_batch(5)
        return City.objects.all().order_by('?')[0]

    @factory.lazy_attribute
    def country(self):
        if not Country.objects.all().exists():
            CountryFactory.create_batch(5)
        return Country.objects.all().order_by('?')[0]

    @factory.lazy_attribute
    def category(self):
        if not StudySubject.objects.all().exists():
            StudySubjectFactory.create_batch(5)
        return StudySubject.objects.all().order_by('?')[0]

    @factory.lazy_attribute
    def description(self):
        return lorem_ipsum.words(50, False)


class MessageRequestFactory(factory.DjangoModelFactory):
    class Meta:
        model = MessageRequest

    @factory.lazy_attribute
    def user(self):
        if not User.objects.all().exists():
            UserFactory.create_batch(5)
        return User.objects.all().order_by('?')[0]

    @factory.lazy_attribute
    def course_request(self):
        if not CourseRequest.objects.all().exists():
            CourseRequestFactory.create_batch(5)
        return CourseRequest.objects.all().order_by('?')[0]

    @factory.lazy_attribute
    def message(self):
        return lorem_ipsum.words(20, False)