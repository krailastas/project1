# encoding: utf-8
from __future__ import unicode_literals

from rest_framework import serializers

from course_request.models import CourseRequest, MessageRequest


class CourseRequestSerializer(serializers.ModelSerializer):
    """
    **CourseRequest Serializer**
        Serializer for CourseRequest model.

        **fields:**
        
        * `id`
        * `user`
        * `request_title`
        * `city`
        * `country`
        * `category`
        * `description`
        * `liked`
    """
    class Meta:
        model = CourseRequest
        read_only_fields = ('user', 'liked')
        exclude = ('is_active', 'is_pre_active', 'course',)


class CourseRequestLikedSerializer(serializers.ModelSerializer):
    """
    **CourseRequestLiked Serializer**
        Serializer for CourseRequest model.

        **fields:**

        * `id`
        * `user`
        * `request_title`
        * `city`
        * `country`
        * `category`
        * `description`
    """
    class Meta:
        model = CourseRequest
        read_only_fields = ('user', 'request_title', 'city', 'country', 'category', 'description',)
        exclude = ('is_active', 'course',)


class MessageRequestSerializer(serializers.ModelSerializer):
    """
    **MessageRequest Serializer**
        Serializer for MessageRequest model.

        **fields:**

        * `id`
        * `user`
        * `course_request`
        * `message`
    """
    class Meta:
        model = MessageRequest
        read_only_fields = ('user',)