# encoding: utf-8
from __future__ import unicode_literals

from rest_framework.routers import SimpleRouter

from course_request.views import CourseRequestView, CourseRequestLikedView, MessageRequestView


router = SimpleRouter(trailing_slash=False)
router.register('course-requests', CourseRequestView, base_name='course_request')
router.register('course-request-like', CourseRequestLikedView, base_name='course_request_like')
router.register('course-request-messages', MessageRequestView, base_name='course_request_message')

urlpatterns = router.urls

