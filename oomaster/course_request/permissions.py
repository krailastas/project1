# encoding: utf-8
from __future__ import unicode_literals

from rest_framework import permissions
from rest_framework.permissions import BasePermission


class IsCourseRequestPermission(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated():
            return request.user and request.user.is_authenticated()
        elif request.method in permissions.SAFE_METHODS:
            return True

    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


class IsMessageRequestViewPermission(BasePermission):
    def has_permission(self, request, view):
        if request.user.is_authenticated():
            return request.user and request.user.is_authenticated()
        elif request.method in permissions.SAFE_METHODS:
            return True