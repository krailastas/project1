# encoding: utf-8
from __future__ import unicode_literals

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from profiles.factories import UserFactory, StudySubjectFactory
from course_request.factories import CourseRequestFactory, MessageRequestFactory
from custom_cities_light.factories import CountryFactory, CityFactory


class TestCourseRequestView(APITestCase):
    url = reverse('course_request-list')

    def setUp(self):
        super(TestCourseRequestView, self).setUp()
        CourseRequestFactory.create_batch(5)
        self.user = UserFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 5)

    def test_post(self):
        data = {
            'request_title': 'request_title',
            'category': StudySubjectFactory.create().pk,
            'city': CityFactory.create().pk,
            'country': CountryFactory.create().pk,
            'description': 'description',
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class TestCourseRequestLikeView(APITestCase):
    url = reverse('course_request_like-list')

    def setUp(self):
        super(TestCourseRequestLikeView, self).setUp()
        CourseRequestFactory.create_batch(5)
        self.course = CourseRequestFactory.create()
        self.user_liked = UserFactory.create()
        self.user = UserFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 6)

    def test_put(self):
        url = reverse('course_request_like-detail', kwargs={'pk': self.course.pk})
        data = {
            'liked': self.user_liked.pk
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class TestMessageRequestView(APITestCase):
    url = reverse('course_request_message-list')

    def setUp(self):
        super(TestMessageRequestView, self).setUp()
        MessageRequestFactory.create_batch(5)
        self.user = UserFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 5)

    def test_post(self):
        data = {
            'request course': CourseRequestFactory.create().pk,
            'message': 'message',
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)