from django.conf.urls import patterns, include, url
from django.conf import settings
from django.views.generic import TemplateView

from django.conf.urls.static import static
from django.contrib import admin
from common.views import api_root


import socketio.sdjango

socketio.sdjango.autodiscover()
admin.autodiscover()


urlpatterns = patterns('',
                       url(r'^$', TemplateView.as_view(template_name='base.html')),
                       url(r'^socket-test/$', TemplateView.as_view(template_name='notification/socket_test.html')),
                       url(r'^rosetta/', include('rosetta.urls')),
                       url(r'^api-auth/', include('rest_framework.urls'), name='rest_framework'),
                       url(r'^api-token-auth/', 'rest_framework.authtoken.views.obtain_auth_token'),
                       url(r'^api/', include('profiles.urls')),
                       url(r'^api/', include('flat.urls')),
                       url(r'^api/', include('notification_settings.urls')),
                       url(r'^api/', include('course.urls')),
                       url(r'^api/', include('notification.urls')),
                       url(r'^api/', include('earning.urls')),
                       url(r'^api/', include('rest_geoip.urls')),
                       url(r'^api/', include('course_request.urls')),
                       url(r'^api/', include('payment.urls')),
                       url(r'^ckeditor/', include('ckeditor.urls')),
                       url(r'^api/cities_light/', include('cities_light.contrib.restframework2')),
                       url(r'^api-url$', api_root, name='api_url'),
                       url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^socket\.io', include(socketio.sdjango.urls)),
                       url(r'^socket\.io', include(socketio.sdjango.urls)),
                       )

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += patterns('',
                            url(r'^__debug__/', include(debug_toolbar.urls)),
                            )
