"""Production settings and globals."""

from __future__ import absolute_import

from .base import *

DEBUG = False

ALLOWED_HOSTS = ['oomaster-backend.themanxgroup.tw', 'oomaster.themanxgroup.tw']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'oomaster',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

