# encoding: utf-8
from __future__ import unicode_literals

from django.conf.urls import patterns, url

from rest_framework.routers import SimpleRouter

from notification.views import NotificationView, NotificationUserView, NotificationCourseStudentsView


router = SimpleRouter(trailing_slash=False)
router.register('notifications', NotificationView, base_name='notifications')
router.register('notification-users', NotificationUserView, base_name='notification_user')
urlpatterns = router.urls


urlpatterns += patterns(
    'notifications.views',
    url(r'^notification-course-students/$', NotificationCourseStudentsView.as_view(), name='notification-course-students')
)
