# encoding: utf-8
from __future__ import unicode_literals

from django.contrib import admin

from notification.models import Notification, NotificationUser, \
    DeviceTokenNotification


class NotificationAdmin(admin.ModelAdmin):
    list_display = ('id', 'message', 'notification_settings')
    list_display_links = ('id', 'message',)


admin.site.register(Notification, NotificationAdmin)


class NotificationUserAdmin(admin.ModelAdmin):
    list_display = ('id', 'notification', 'user', 'is_read', 'is_send')
    list_display_links = ('id', 'notification',)


admin.site.register(NotificationUser, NotificationUserAdmin)

admin.site.register(DeviceTokenNotification)
