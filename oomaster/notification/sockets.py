# encoding: utf-8
from __future__ import unicode_literals

import logging

from gevent import Greenlet
from django.conf import settings
import gevent
import pika
from socketio.namespace import BaseNamespace
from socketio.mixins import RoomsMixin, BroadcastMixin
from socketio.sdjango import namespace

from notification.models import NotificationUser
from notification.serializers import SocketSerializer


@namespace('/notify')
class NotifyNamespace(BaseNamespace, RoomsMixin, BroadcastMixin):
    """
    **NotifyNamespace socket**

        Return a list of notification messages for current user. 
        The current user is the authorized user.
        Allows to mark notification as read.
        Should be connected socket.js and notification.js.
        Usage pattern stored in a file 'templates/notification/socket_test.html' or on '/socket-test/'.

        **returns:**

        * `messages`
    """

    def initialize(self):
        self.logger = logging.getLogger("socketio.notify")
        self.log("Socketio session started")

    def log(self, message):
        self.logger.info("[{0}] {1}".format(self.socket.sessid, message))

    def broadcast_event_only_me(self, event, *args):
        pkt = dict(type="event",
                   name=event,
                   args=args,
                   endpoint=self.ns_name)

        for sessid, socket in self.socket.server.sockets.iteritems():
            if socket is self.socket:
                socket.send_packet(pkt)

    def on_join(self, room):
        self.log('User joined to room: {0}'.format(room))
        self.room = room
        self.join(room)
        response = dict()
        messages = NotificationUser.objects.filter(user_id=int(room), is_read=False).select_related('notification')
        data = SocketSerializer(messages, many=True)
        response['messages'] = data.data
        response['action'] = 'connect'
        self.broadcast_event_only_me('message', response)
        io = self
        parameters = pika.URLParameters(settings.RABBIT_URL)
        connection = pika.BlockingConnection(parameters)
        self.connection = connection
        channel = connection.channel()
        channel.queue_declare(queue=room)

        def subscriber(io):
            while io.socket.connected:
                method_frame, header_frame, body = channel.basic_get(room)
                if method_frame:
                    channel.basic_ack(method_frame.delivery_tag)
                    response = dict()
                    response['message'] = '{}'.format(body)
                    response['action'] = 'message'
                    io.broadcast_event_only_me('message', response)
                gevent.sleep(1)

        Greenlet.spawn(subscriber, io)
        return True

    def on_mark_is_read(self, message_id):
        message = dict()
        NotificationUser.objects.filter(pk=message_id).update(is_read=True)
        message['action'] = 'is_read'
        message['message_id'] = message_id
        self.broadcast_event('message', message)


@namespace('/notify-count')
class NotifyCountNamespace(BaseNamespace, RoomsMixin, BroadcastMixin):
    """
    **NotifyCountNamespace socket**
            
        Return the count of messages for the current user.
        The current user is the authorized user.
        Should be connected socket.js and notification_count.js.

        **returns:**

        * `count`
    """

    def initialize(self):
        self.logger = logging.getLogger("socketio.notify-count")
        self.log("Socketio session started")

    def log(self, message):
        self.logger.info("[{0}] {1}".format(self.socket.sessid, message))

    def broadcast_event_only_me(self, event, *args):
        pkt = dict(type="event",
                   name=event,
                   args=args,
                   endpoint=self.ns_name)
        for sessid, socket in self.socket.server.sockets.iteritems():
            if socket is self.socket:
                socket.send_packet(pkt)

    def on_join(self, room):
        self.log('User joined to room: {0}'.format(room))
        self.room = room
        self.join(room)
        response = dict()
        try:
            count = NotificationUser.objects.filter(user_id=int(room), is_read=False).count()
        except:
            count = 0
        response['count'] = count
        response['action'] = 'connect'
        self.broadcast_event_only_me('count', response)
        io = self
        parameters = pika.URLParameters(settings.RABBIT_URL)
        connection = pika.BlockingConnection(parameters)
        self.connection = connection
        channel = connection.channel()
        channel.queue_declare(queue=room)

        def subscriber(io):
            while io.socket.connected:
                method_frame, header_frame, body = channel.basic_get(room)
                if method_frame:
                    channel.basic_ack(method_frame.delivery_tag)
                    response = dict()
                    response['count'] = '{}'.format(body)
                    response['action'] = 'count'
                    io.broadcast_event_only_me('count', response)
                gevent.sleep(1)

        Greenlet.spawn(subscriber, io)
        return True



