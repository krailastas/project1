# encoding: utf-8
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
from model_utils.models import TimeStampedModel
from django.db.models.signals import post_save

from notification_settings.models import NotificationSettings, \
    NotificationUserSettings
from profiles.models import User
from common.tasks import SendMailTask, ParseNotificationTask


class DeviceTokenNotification(models.Model):
    """
    **DeviceTokenNotification model**

        **list fields:**

        * `user` - ForeignKey - User
        * `deviceType` - CharField
        * `deviceToken` - CharField
    """
    user = models.ForeignKey(User, verbose_name=_('user'),
                             related_name='tokens')
    deviceType = models.CharField(verbose_name=_('deviceType'), max_length=255)
    deviceToken = models.CharField(verbose_name=_('deviceToken'),
                                   max_length=255)

    class Meta:
        verbose_name = _("DeviceTokenNotification")
        verbose_name_plural = _("DeviceTokenNotifications")

    def __str__(self):
        return self.deviceType


class Notification(TimeStampedModel):
    """
    **Notification model**

        **list fields:**

        * `message` - TextField
        * `notification_settings` - ForeignKey - NotificationSettings
    """
    message = models.TextField(verbose_name=_('message'))
    notification_settings = models.ForeignKey(NotificationSettings,
                                              verbose_name=_(
                                                  'notification settings'),
                                              related_name='message_settings')

    class Meta:
        verbose_name = _("Notification")
        verbose_name_plural = _("Notifications")

    def __str__(self):
        return self.message


class NotificationUser(TimeStampedModel):
    """
    **NotificationUser model**

        **list fields:**

        * `notification` - ForeignKey - Notification
        * `user` - ForeignKey - User
        * `is_read` - BooleanField
        * `is_send` - BooleanField
    """
    notification = models.ForeignKey(Notification,
                                     verbose_name=_('notification'),
                                     related_name='notifications')
    user = models.ForeignKey(User, verbose_name=_('user'),
                             related_name='user_notifications')
    is_read = models.BooleanField(verbose_name=_('read'), default=False)
    is_send = models.BooleanField(verbose_name=_('send'), default=False)

    class Meta:
        ordering = ['-created']
        verbose_name = _("User notification")
        verbose_name_plural = _("User notifications")


def send_email(sender, instance, created, **kwargs):
    if created:
        mode = NotificationUserSettings.objects.get(user=instance.user,
                                                    notification=instance.notification.notification_settings).mode
        if mode and not instance.is_send:
            if instance.notification.notification_settings.type == 0:  # if type is web
                ctx_dict = {
                    'user': instance.user,
                    'body': instance.notification.message,
                }
                SendMailTask().delay(instance, ctx_dict)
            elif instance.notification.notification_settings.type == 1:  # if type is mob
                for device in instance.user.tokens.all():
                    ctx_dict = {
                        'deviceType': device.deviceType,
                        'deviceToken': device.deviceToken,
                        'alert': instance.notification.message,
                    }
                    ParseNotificationTask().delay(instance, ctx_dict)
                instance.is_send = True
                instance.save()


post_save.connect(send_email, sender=NotificationUser)


def create_notification_user_settings(sender, instance, created, **kwargs):
    if created:
        for user in User.objects.all():
            notification_user_settings = NotificationUserSettings(user=user,
                                                                  notification=instance,
                                                                  mode=True)
            notification_user_settings.save()


post_save.connect(create_notification_user_settings,
                  sender=NotificationSettings)
