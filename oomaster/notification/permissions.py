# encoding: utf-8
from __future__ import unicode_literals

from rest_framework.permissions import BasePermission


class IsNotificationUserPermission(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated()

    def has_object_permission(self, request, view, obj):
        return obj.user == request.user
