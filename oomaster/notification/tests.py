# encoding: utf-8
from __future__ import unicode_literals

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from profiles.factories import UserFactory
from notification.factories import NotificationFactory, NotificationUserFactory
from notification.models import Notification, NotificationUser


class TestNotificationViewSet(APITestCase):
    url = reverse('notifications-list')

    def setUp(self):
        super(TestNotificationViewSet, self).setUp()
        NotificationFactory.create_batch(5)
        self.user = UserFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post(self):
        data = {
            'message': 'message',
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class TestNotificationUserViewSet(APITestCase):
    url = reverse('notification_user-list')

    def setUp(self):
        super(TestNotificationUserViewSet, self).setUp()
        NotificationUserFactory.create_batch(5)
        self.user = UserFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Notification.objects.all().count(), 5)

    def test_post(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_put(self):
        nu = NotificationUser.objects.all().last()
        pk = nu.pk
        url = reverse('notification_user-detail', kwargs={'pk': pk})
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(nu.user)
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)