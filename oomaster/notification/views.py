# encoding: utf-8
from __future__ import unicode_literals

from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.mixins import (UpdateModelMixin, RetrieveModelMixin,
                                   ListModelMixin)
from rest_framework.viewsets import GenericViewSet, ReadOnlyModelViewSet
from rest_framework.generics import GenericAPIView
from rest_framework.status import HTTP_201_CREATED

from notification.models import Notification, NotificationUser
from notification.serializers import (NotificationSerializer, NotificationUserSerializer,
                                      NotificationMyStudentsSerializer)
from notification.permissions import IsNotificationUserPermission


class NotificationView(ReadOnlyModelViewSet):
    """
    **Notification View**

        Return list of notification.

        **returns:**

        * `id`
        * `message`
        * `notification_settings`
    """
    model = Notification
    serializer_class = NotificationSerializer
    permission_classes = (IsAuthenticated,)


class NotificationUserView(UpdateModelMixin, RetrieveModelMixin, ListModelMixin, GenericViewSet):
    """
    **NotificationUser View**

        Return list of user notifications.
        Update notification.

        **returns:**

        * `id`
        * `notification`
        * `user`
        * `is_read`
    """
    queryset = NotificationUser.objects.all()
    serializer_class = NotificationUserSerializer
    permission_classes = (IsNotificationUserPermission, )

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)


class NotificationCourseStudentsView(GenericAPIView):
    serializer_class = NotificationMyStudentsSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.DATA, context={'request': request})
        if serializer.is_valid():
            serializer.save()
        return Response(status=HTTP_201_CREATED)
