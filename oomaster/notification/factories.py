# encoding: utf-8
from __future__ import unicode_literals

import factory
from django.contrib.webdesign import lorem_ipsum

from notification.models import Notification, NotificationUser
from notification_settings.models import NotificationSettings
from notification_settings.factories import NotificationSettingsActiveFactory
from profiles.models import User
from profiles.factories import UserFactory


class NotificationFactory(factory.DjangoModelFactory):
    class Meta:
        model = Notification

    @factory.lazy_attribute
    def message(self):
        return lorem_ipsum.words(5, False)

    @factory.lazy_attribute
    def notification_settings(self):
        if not NotificationSettings.activated.all().exists():
            NotificationSettingsActiveFactory.create_batch(5)
        return NotificationSettings.activated.all().order_by('?')[0]


class NotificationUserFactory(factory.DjangoModelFactory):
    class Meta:
        model = NotificationUser

    @factory.lazy_attribute
    def notification(self):
        if not Notification.objects.all().exists():
            NotificationFactory.create_batch(5)
        return Notification.objects.all().order_by('?')[0]

    @factory.lazy_attribute
    def user(self):
        if not User.objects.all().exists():
            UserFactory.create_batch(5)
        return User.objects.all().order_by('?')[0]