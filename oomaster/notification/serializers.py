# encoding: utf-8
from __future__ import unicode_literals

from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist

from rest_framework import serializers

from profiles.models import User
from course.models import Course
from notification.models import Notification, NotificationUser
from notification_settings.models import NotificationSettings, NotificationUserSettings


class NotificationSerializer(serializers.ModelSerializer):
    """
    **Notification Serializer**
        Serializer for Notification model.

        **fields:**

        * `id`
        * `message`
        * `notification_settings`
    """

    class Meta:
        model = Notification


class NotificationUserSerializer(serializers.ModelSerializer):
    """
    **NotificationUser Serializer**
        Serializer for NotificationUser model.

        **fields:**

        * `id`
        * `notification`
        * `user`
        * `is_read`
    """

    class Meta:
        model = NotificationUser
        exclude = ('is_send', )
        read_only_fields = ('user', 'notification', )


class NotificationMyStudentsSerializer(serializers.Serializer):
    course_id = serializers.IntegerField()
    message = serializers.CharField(max_length=512)

    def validate_course_id(self, attrs, sourse):
        try:
            course = Course.objects.get(id=attrs[sourse])
        except ObjectDoesNotExist:
            raise serializers.ValidationError(_("Course with this id does not exist."))

        request_user = self.context['request'].user
        if request_user != course.teacher:
            raise serializers.ValidationError(_("Only course teacher can send notifications to students."))

        if course.students.count() == 0:
            raise serializers.ValidationError(_("No students in this course."))
        return attrs

    def save(self, **kwargs):
        course = Course.objects.get(id=self.data['course_id'])

        # mobile
        try:
            notification_settings_mobile = NotificationSettings.objects.get(name='announcement from teacher', type=1)
        except ObjectDoesNotExist:
            raise serializers.ValidationError(_("Site administrator must create 'announcement from teacher' mobile notification setting"))
        else:
            notification = Notification.objects.create(
                message=self.object['message'],
                notification_settings=notification_settings_mobile
            )
            for student in course.students.all():
                notification_user = NotificationUser(notification=notification, user=student)
                notification_user.save()

        # web
        try:
            notification_settings_mobile = NotificationSettings.objects.get(name='announcement from teacher', type=0)
        except ObjectDoesNotExist:
            raise serializers.ValidationError(_("Site administrator must create 'announcement from teacher' web notification setting"))
        else:
            notification = Notification.objects.create(
                message=self.object['message'],
                notification_settings=notification_settings_mobile
            )
            for student in course.students.all():
                notification_user = NotificationUser(notification=notification, user=student)
                notification_user.save()


class SocketSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    notification = serializers.CharField()
