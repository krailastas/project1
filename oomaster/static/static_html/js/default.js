$(document).ready(function () {
   var $body = $('body');
   
	$body.on('click', '.navbar-toggle .sr-only, .navbar-toggle', function (e) {
	    e.stopPropagation();
	    if ($(window).width() > 990) return false;
	    $('.nav-collapse').slideToggle();
	});
/******************************************/
  $('.slider-block').owlCarousel({
    autoPlay : 2000,
    stopOnHover : true,
    navigation:true,
    paginationSpeed : 1000,
    goToFirstSpeed : 2000,
    singleItem : true,
    autoHeight : true,
    autoPlay : true,
    slideSpeed : 200,
    paginationSpeed : 800,
    rewindSpeed : 500,
     navigationText : ["",""],
    });

  $('body').on('click', '.nav-tabs li', function(e){
      var current  = $(e.currentTarget);
     var parent        = $('div.tab-content');
     var tabcontent = parent.find('div.tab-pane');

     current.addClass('active').siblings().removeClass('active')
     tabcontent.removeClass('active');
     current.parent().find('li').each(function(i) {    
        if($(this).html() == current.html()) {
        tabcontent.eq(i).addClass('active');
        return false;
      }
      
    });
    update_scroll (); 
    return false;
  });

  /******************************************/

	$('select').chosen({
		max_selected_options: 5,
		disable_search:true,
		inherit_select_classes:true,
		width:'100%'
	});

	$('body').click(function(e) {
     if (!$(e.target).parents().is('.profil'))    $('.profil').removeClass('active');
     if (!$(e.target).parents().is('.alert'))    $('.alert').removeClass('active');
     if (!$(e.target).parents().is('.login-block'))    $('.login').parent().removeClass('active');
   });

	$('.login-block .login').click(function(e){
		e.stopPropagation();
		$(this).parent().toggleClass('active');
	})

	$('.alert').click(function(e){
		e.stopPropagation();
		$(this).toggleClass('active');
	})

	$('body').on('click','.profil',function(e){
		e.stopPropagation();
		$(this).toggleClass('active');
	})

	

	$('.block-view li').click(function(){
		$('.block-view li').removeClass('active');
		$(this).addClass('active');
		if($(this).find('.icon').hasClass('list')) $('.block-courses').addClass('view-list');
		else $('.block-courses').removeClass('view-list');
	})

	$('input[type=checkbox]').tzCheckbox({labels:['on','off']});


	// style scroll
	if($('.scroll-pane').length!=0) {
		$('.scroll-pane').jScrollPane().bind('mousewheel', function(e){
	       e.preventDefault();
    });

   
	$(window).bind('resize',function(){
	   if($('.bg-tbody').length!=0) {
	      var parent = $('.bg-tbody').parents('.table');
	      
	      var width = 0;
	      
	      if($(window).width()<=640) {
	      	width =parent.outerWidth()-30;
	      }
	      else {
	      	width = parent.find('.thead').outerWidth();
	      	parent.find('.tbody').find('.delete').css('float','right');      	
	      }
	      parent.find('.tbody').css('width', width);	      
	      parent.find('.bg-tbody').css('width', width);
	   }	

       if($('.scroll-pane').length!=0) {

		 $('.scroll-pane').each(function(i) {
			var api = $(this).data('jsp');
			var throttleTimeout;

				if (!throttleTimeout) {
					throttleTimeout = setTimeout(
						function()
						{
							api.reinitialise();
							throttleTimeout = null;
						},
						50
					);
				}


			});
		}
   });

   	
   }
   
  $(window).trigger('resize');

 $(function () {
             

   $('.fileupload').fileupload({
        dataType: 'json',
         autoUpload: false,
        add: function (e, data) {
        	var node = $(e.target).parents('.load-file');
             $.each(data.files, function (index, file) {          	
                node.find('.files').prepend($('<span/>').addClass('file-name').text(file.name));
		        node.find('.files').addClass('load');
		        node.find('.fileinput-button').addClass('hide');
		    });

        },
        done: function (e, data) {
           
        }
    });

	$('body').on('click','.cancel', function () {
		
		var dataLoad = $(this).parents('.load-file').attr('data-item');

		if($('.load-file[data-item="'+dataLoad+'"]').length>1) {
			$(this).parents('.load-file').remove();
		}
		else{
			$(this).parents('.load-file').find('.fileinput-button').removeClass('hide');
			$(this).parent().removeClass('load').find('.file-name').text('');
		}
	});

});

	//raiting
	$(".reviews .rating").each(function( i ) {
     
     var countStar = $(this).attr('data-rating');
     $(this).find('span').css('width',15*countStar);
   
    });

    //datepicker
    $('.form-item.date').datepicker({
	   format:'dd.mm.yyyy',
	   weekStart:1,
	   startView:3,
	   startDate: '+0d',
	   autoclose: true
	});
	
	//datepicker
	
	$(".date input").keypress(function(event){
		var key, keyChar;
		if(!event) var event = window.event;
	
		if (event.keyCode) key = event.keyCode;
		else if(event.which) key = event.which;

		if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
		keyChar=String.fromCharCode(key);
	
		if(!/\d/.test(keyChar))	return false;
	});

	//Add more educations 

	$('body').on('click','.more-attach',function(e){
		e.preventDefault();
		/*var AddBlock = $('<div>',{
				class	: 'form-item load-file',
				html:	'<input type="text" placeholder="Input your education here"><input type="file"/>'
			});*/
		$(this).parent().before(AddBlock);
		AddBlock.find('input[type=file]').styleinput({
			labels:'Attach diplloma image',
			remove_parent:true,
			parentName:'.form-item'
		});
		return false;
	})




});


function update_scroll () {
	if($('.bg-tbody').length!=0) {
	      var parent = $('.bg-tbody').parents('.table');
	      
	      var width = 0;
	      
	      if($(window).width()<=640) {
	      	width =parent.outerWidth()-30;
	      }
	      else {
	      	width = parent.find('.thead').outerWidth();
	      	parent.find('.tbody').find('.delete').css('float','right');      	
	      }
	      parent.find('.tbody').css('width', width);	      
	      parent.find('.bg-tbody').css('width', width);
	   }
	   
	if($('.scroll-pane').length!=0) {
		$('.scroll-pane').each(function(i) {
			var api = $(this).data('jsp');
			api.reinitialise();
		});
	}
}