(function($){
	$.fn.tzCheckbox = function(options){
		
		// Default On / Off labels:
		
		options = $.extend({
			labels : ['ON','OFF']
		},options);
		
		return this.each(function(){
			var originalCheckBox = $(this),
				labels = [];

			// Checking for the data-on / data-off HTML5 data attributes:
			if(originalCheckBox.data('on')){
				labels[0] = originalCheckBox.data('on');
				labels[1] = originalCheckBox.data('off');
			}
			else labels = options.labels;

			// Creating the new checkbox markup:
			var checkBox = $('<span>',{
				class	: 'tzCheckBox '+(this.checked?'checked':''),
				html:	'<span class="tzCBContent">'+labels[this.checked?0:1]+
						'</span><span class="tzCBPart"></span>'
			});

			var ClassName = originalCheckBox.attr('class');

			// Inserting the new checkbox, and hiding the original:
			checkBox.insertAfter(originalCheckBox.hide()).addClass(ClassName);

			checkBox.click(function(){
				checkBox.toggleClass('checked');
				
				var isChecked = checkBox.hasClass('checked');
				
				// Synchronizing the original checkbox:
				originalCheckBox.attr('checked',isChecked);
				checkBox.find('.tzCBContent').html(labels[isChecked?0:1]);
			});
			
			// Listening for changes on the original and affecting the new one:
			originalCheckBox.bind('change',function(){
				checkBox.click();
			});
		});
	};


	$.fn.styleinput = function(options){
		
		// Default On / Off labels:
		
		options = $.extend({
			labels : 'Choose file...',
			remove_parent:false,
			parentName:'div'
		},options);
		
		return this.each(function(){
			var originalInput = $(this),
				labels;

			labels = options.labels;

			// Creating the new checkbox markup:
			var InputFile = $('<span>',{
				class	: 'box-input-file',
				html:	'<span class="file-name-box"><span class="file-name"></span>'+(options.remove_parent?'<span class="remove-box"><i class="icon remove-file"></i></span>':'') +'</span>'+
						'<button class="file-load">'+labels+'</button>'
			});

			var  InputFileRemove = InputFile.find('.remove-box');

			// Inserting the new checkbox, and hiding the original:
			InputFile.insertAfter(originalInput.hide());

			InputFile.find('.file-load').click(function(e){
				e.preventDefault();		
  
    			originalInput.trigger('click'); 				
			});

			InputFileRemove.click(function(event) {
				   $(this).parents(options.parentName).remove();
			});

			originalInput.change(function(e){

				if (originalInput.val()=='') {
					InputFile.find('.file-name').html('');
					InputFile.find('.file-name-box').removeClass('open-file');
				} else {
					var filename = originalInput.val().replace(/^.*[\\\/]/, '');
			    	var ext = filename.split('.').pop().toLowerCase();	    
		        	InputFile.find('.file-name').html(filename);
		        	InputFile.find('.file-name-box').addClass('open-file');
				}
			    
			    
			});

		
		});

     return this;
	};
})(jQuery);