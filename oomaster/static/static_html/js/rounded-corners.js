(function (H) {
    H.wrap(H.seriesTypes.column.prototype, 'translate', function (proceed) {
        var options = this.options,
            rTopLeft = options.borderRadiusTopLeft || 0,
            rTopRight = options.borderRadiusTopRight || 0,
            rBottomRight = options.borderRadiusBottomRight || 0,
            rBottomLeft = options.borderRadiusBottomLeft || 0;

        proceed.call(this);

        if (rTopLeft || rTopRight || rBottomRight || rBottomLeft) {
            H.each(this.points, function (point) {
                var shapeArgs = point.shapeArgs,
                    w = shapeArgs.width,
                    h = shapeArgs.height,
                    x = shapeArgs.x,
                    y = shapeArgs.y;
                  point.shapeType = 'path';
                var paramfill = 0.2+point.index*0.2;
                 point.color= "rgba(0, 176, 125, "+paramfill+")";
                point.shapeArgs = {
                    d: [
                        'M', x + rTopLeft, y,
                        // top side
                        'L', x + w - rTopRight, y,
                        // top right corner
                        'C', x + w - rTopRight / 2, y, x + w, y + rTopRight / 2, x + w, y + rTopRight,
                        // right side
                        'L', x + w, y + h - rBottomRight,
                        // bottom right corner
                        'C', x + w, y + h - rBottomRight / 2, x + w - rBottomRight / 2, y + h, x + w - rBottomRight, y + h,
                        // bottom side
                        'L', x + rBottomLeft, y + h,
                        // bottom left corner
                        'C', x + rBottomLeft / 2, y + h, x, y + h - rBottomLeft / 2, x, y + h - rBottomLeft,
                        // left side
                        'L', x, y + rTopLeft,
                        // top left corner
                        'C', x, y + rTopLeft / 2, x + rTopLeft / 2, y, x + rTopLeft, y,
                        'Z'
                    ]
                };
                    
            });
        }
    });
}(Highcharts));

$(function () {
var chart;
function drawChart (){
    var defaultOptions = {
        chart: {    
            renderTo: 'container-earnings',     
            type: 'column',
            marginBottom:50,
            marginLeft:60,
            marginRight:35,
            className: 'chart-wrap',
            events : {
               load : function(){
                 var f = this.renderer.g("axis-line").add();
                 var ss = $('.highcharts-axis').find('path').attr("d").split(" ");
                 var y = 45 + parseInt(ss[2]);
                 var x = parseInt(ss[1]) -68;
                 this.renderer.path(['M', ss[1], ss[2], 'L', x, y]).attr({"fill":"#bbc0c6","stroke":"#bbc0c6", "stroke-width":"2", "zIndex":"7", "visibility":"visible"}).add(f);
                 this.renderer.text("Earnings",35,236).attr({"fill":"#b6bcc2", "style":"color:#b6bcc2;cursor:default;font-size:12px;font-family:Roboto, sans-serif;","transform":"translate(0,0) rotate(325 10 283)"}).add(f);
                 this.renderer.text("Year/Month",30,257).attr({"fill":"#b6bcc2", "style":"color:#b6bcc2;cursor:default;font-size:12px;font-family:Roboto, sans-serif;","transform":"translate(0,0) rotate(325 10 283)"}).add(f);

               }
              },
    
        },
        credits : {
            enabled: false
        },
        xAxis: {
            type: 'category',
            title: {
                text: ''
            },
            min: 0,
            max: 8,           
     
            labels: {
                style: {
                    fontSize: '12px',
                    fontFamily: 'Roboto, sans-serif',
                    color:"#be4930"
                },
                format: '{value}',
                formatter: function () {
                   if(this.value>0|| $(window).width()<800)  return '';
                   return this.value;
                }
            }
        },
        yAxis: {
             gridLineWidth: 0,
             lineWidth: 2,

            title: {
                text: ''
            },
            labels: {
                enabled: false
            }
            
        },

        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Earnings: <b>{point.y}</b>'
        },
        
    plotOptions: {
        column: {
            pointWidth: ($('#container-earnings').width() / 13) + 1,
            borderWidth: 1,
            maxPointWidth: 12
        },

    },
        series: [{
            name: 'Earnings',

            data: [
                ['2014/March', 10000],
                ['2014/May', 16000],
                ['2014/june', 10000],
                ['2014/july', 16000],
                ['2015/March', 10000],
                ['2015/March', 21000]
            ],                
            dataLabels: {
                enabled: false
            },
             borderRadiusTopLeft: 10,
             borderRadiusTopRight: 10
          
        }]
    };

    chart = new Highcharts.Chart(defaultOptions);
}


$(window).resize(function(){
    if(chart!=undefined){
           chart.destroy();
        }   
    setTimeout(function(){
    drawChart ();
     }, 500);
    
  
})

});