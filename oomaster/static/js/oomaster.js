// Overriding store method. As we aren't going to save results.
hello.utils.store = function () {
};

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

hello.init(
    {
        'facebook': '1548517522035087',
        'google-oauth2': '845749723656-b4eadueojege1rkfmiutlh2iaackdmsg.apps.googleusercontent.com',
    },
    {
        oauth_proxy: '/api/social_login/'
    }
);

hello.on('auth.login', function (auth) {
    if (auth.network) {
        $.post('/api/register_by_token/', auth.authResponse, function (d) {
            console.log(d);
        });
    }
});
