var socket = io.connect("/notify", {port: 8000});

socket.on('connect', function () {
    socket.emit('join', stream_uuid);
});

socket.on('message', function (data) {
    var message_container = $('.message');
    var scrollTo = 0;
    console.log(data);
    if (data.action == 'message') {
        if (data.message != '') {
            message_container.contents().filter(function () {
                return (this.nodeType == 3);
            }).remove();
        }
        message_container.append("<p>" + data.message + "</p>");
        message_container.find('p').each(function (index, value) {
            scrollTo += $(value).outerHeight() + 25;
        });
        message_container.animate({
            scrollTop: scrollTo
        });
    } else if (data.action == 'connect') {
        if (data.messages != '') {
            message_container.contents().filter(function () {
                return (this.nodeType == 3);
            }).remove();
        }
        $.each(data.messages, function (index, value) {
            message_container.append("<p class='mess' id='d_" + value.id + "' ids=" + value.id + ">" + value.notification + "</p>");
        });
        $('.mess').on('click', function (data) {
            var msg_id = $(this).attr('ids')
            socket.emit('mark_is_read', msg_id);
        });
        message_container.find('p').each(function (index, value) {
            scrollTo += $(value).outerHeight() + 25;
        });
        message_container.animate({
            scrollTop: scrollTo
        });
    }
});
