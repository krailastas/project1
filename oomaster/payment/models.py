import sys
import ast

from paypal.standard.ipn.signals import payment_was_successful
from paypal.standard.ipn.signals import payment_was_flagged, payment_was_refunded, payment_was_reversed

from profiles.models import User
from course.models import Course


def update_profile_after_purchase(sender, **kwargs):
    print 'PAYMENT_WAS_SUCCESSFUL!!!'
    ipn_obj = sender
    if ipn_obj.payment_status == 'Completed':
        try:
            info = ast.literal_eval(ipn_obj.custom)
            user = User.object.get(id=int(info['user']))
            course = Course.objects.get(id=int(info['course']))
        except:
            print 'Error:', sys.exc_info()[0]
        else:
            if user not in course.students.all():
                course.students.add(user)
                print 'SUCCESSFULLY UPDATE COURSE'
            else:
                print 'USER ALREADY BOUGHT THIS COURSE BEFORE!'
    else:
        pass


def payment_was_flagged_foo(sender, **kwargs):
    print 222222222


def payment_was_refunded_foo(sender, **kwargs):
    print 333333


def payment_was_reversed_foo(sender, **kwargs):
    print 4444444


payment_was_successful.connect(update_profile_after_purchase)
payment_was_flagged.connect(payment_was_flagged_foo)
payment_was_refunded.connect(payment_was_refunded_foo)
payment_was_reversed.connect(payment_was_reversed_foo)
