from uuid import uuid4

from django.shortcuts import render_to_response
from django.conf import settings
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.views.generic import TemplateView
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404

from braces.views import LoginRequiredMixin

from course.models import Course
from paypal.standard.forms import PayPalPaymentsForm


class GetCoursePaypalBuyButton(LoginRequiredMixin,
                               TemplateView):

    template_name = 'payment/paypal_buy_button.txt'

    def get_context_data(self, **kwargs):
        context = super(GetCoursePaypalBuyButton, self).get_context_data(**kwargs)

        course_id = self.kwargs.get('course_id', None)
        if course_id:
            try:
                course = Course.objects.get(id=int(course_id))
            except ObjectDoesNotExist:
                raise Http404
            else:
                if self.request.user not in course.students.all():
                    # get site domen
                    current_site_domain = self.request.META['HTTP_HOST']
                    protocol = getattr(settings, 'MY_SITE_PROTOCOL', 'http')
                    port = getattr(settings, 'MY_SITE_PORT', '')
                    site_url = '{}://{}'.format(protocol, current_site_domain)
                    if port:
                        site_url += '{}'.format(port)

                    # set paypal dict
                    price = course.classes.count() * course.price_each_class
                    paypal_dict = {
                        "custom": str({'user': self.request.user.id, 'course': int(course_id)}),
                        "business": settings.PAYPAL_RECEIVER_EMAIL,
                        "amount": str(price),
                        "item_name": course.title,
                        "invoice": str(uuid4()),
                        "currency_code": course.currency.title,
                        "notify_url": site_url + reverse('paypal-ipn'),
                        "return_url": site_url + reverse('paypal_return'),
                        "cancel_return": site_url + reverse('paypal_cancel'),
                    }

                    paypal_form = PayPalPaymentsForm(initial=paypal_dict)
                    context['paypal_form'] = paypal_form
                else:
                    raise Http404
        return context


@login_required
@csrf_exempt
def paypal_return(request):
    context = {'page_title': 'You have successfully purchased reels wia paypal'}
    return render_to_response("payment/payment.html", context)


@login_required
def paypal_cancel(request):
    context = {'page_title': 'You canceled payment'}
    return render_to_response("payment/payment.html", context)
