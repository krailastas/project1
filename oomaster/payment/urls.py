# encoding: utf-8
from django.conf.urls import patterns, url, include

from payment.views import GetCoursePaypalBuyButton, paypal_return, paypal_cancel


urlpatterns = patterns(
    'payment.views',
    (r'^payment/paypal/', include('paypal.standard.ipn.urls')),
    url(
        regex=r'^get-course-paypal-buy-button/(?P<course_id>[\d]+)/$',
        view=GetCoursePaypalBuyButton.as_view(),
        name='get-course-paypal-buy-button'
    ),
    url(
        regex=r'^paypal_return/$',
        view=paypal_return,
        name='paypal_return'
    ),
    url(
        regex=r'^paypal_cancel/$',
        view=paypal_cancel,
        name='paypal_cancel'
    ),
)
