from datetime import datetime

from django.contrib import admin

from course.models import (Currency, AttachedFiles, TeacherInvitation,
                           Course, Class, Note, Message, FollowedTeacher,
                           TeacherInTeacherList, Review, CourseInWishlist,
                           CourseAttachedFile)


class CourseAdmin(admin.ModelAdmin):
    search_fields = ('title', 'teacher', 'creator')
    list_display = ('id', 'title', 'teacher', 'creator', 'location', 'category',
                    'date_begin', 'date_end', 'is_canceled', 'upcoming', 'currency')

    def upcoming(self, obj):
        return True if obj.date_begin > datetime.today().date() else False


class ClassAdmin(admin.ModelAdmin):
    search_fields = ('course', 'number', 'date')
    list_display = ('id', 'course', 'number', 'date', 'location', 'time_begin', 'duration')

    def upcoming(self, obj):
        return True if obj.date > datetime.today().date() else False


admin.site.register(CourseAttachedFile)
admin.site.register(CourseInWishlist)
admin.site.register(Currency)
admin.site.register(Review)
admin.site.register(AttachedFiles)
admin.site.register(Course, CourseAdmin)
admin.site.register(Class, ClassAdmin)
admin.site.register(FollowedTeacher)
admin.site.register(Note)
admin.site.register(Message)
admin.site.register(TeacherInTeacherList)
admin.site.register(TeacherInvitation)
