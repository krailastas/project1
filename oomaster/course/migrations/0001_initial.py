# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Currency'
        db.create_table(u'course_currency', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'course', ['Currency'])

        # Adding model 'AttachedFiles'
        db.create_table(u'course_attachedfiles', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal(u'course', ['AttachedFiles'])

        # Adding model 'MaterialForLearning'
        db.create_table(u'course_materialforlearning', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('attached_files', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'material_files', to=orm['course.AttachedFiles'])),
        ))
        db.send_create_signal(u'course', ['MaterialForLearning'])

        # Adding model 'Course'
        db.create_table(u'course_course', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('creator', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'cr_courses', to=orm['profiles.User'])),
            ('teacher', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'th_courses', to=orm['profiles.User'])),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('detail', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('date_begin', self.gf('django.db.models.fields.DateField')()),
            ('date_end', self.gf('django.db.models.fields.DateField')()),
            ('amount_classes', self.gf('django.db.models.fields.IntegerField')(default=1)),
            ('city', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cities_light.City'])),
            ('country', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['cities_light.Country'])),
            ('min_sits_count', self.gf('django.db.models.fields.PositiveIntegerField')(default=10)),
            ('max_sits_count', self.gf('django.db.models.fields.PositiveIntegerField')(default=20)),
            ('currency', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['course.Currency'])),
            ('price_each_class', self.gf('course.fields.PriceField')(default=0, max_digits=10, decimal_places=2)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('time_begin', self.gf('django.db.models.fields.TimeField')()),
            ('duration', self.gf('django.db.models.fields.PositiveIntegerField')(default=60)),
            ('image', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'category_courses', to=orm['profiles.StudySubject'])),
            ('materials_is_free', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'course', ['Course'])

        # Adding unique constraint on 'Course', fields ['teacher', 'title']
        db.create_unique(u'course_course', ['teacher_id', 'title'])

        # Adding M2M table for field matterials_for_learning on 'Course'
        m2m_table_name = db.shorten_name(u'course_course_matterials_for_learning')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('course', models.ForeignKey(orm[u'course.course'], null=False)),
            ('materialforlearning', models.ForeignKey(orm[u'course.materialforlearning'], null=False))
        ))
        db.create_unique(m2m_table_name, ['course_id', 'materialforlearning_id'])

        # Adding M2M table for field students on 'Course'
        m2m_table_name = db.shorten_name(u'course_course_students')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('course', models.ForeignKey(orm[u'course.course'], null=False)),
            ('user', models.ForeignKey(orm[u'profiles.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['course_id', 'user_id'])

        # Adding model 'Class'
        db.create_table(u'course_class', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('course', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'classes', to=orm['course.Course'])),
            ('number', self.gf('django.db.models.fields.IntegerField')()),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('time_begin', self.gf('django.db.models.fields.TimeField')()),
            ('duration', self.gf('django.db.models.fields.PositiveIntegerField')(default=60)),
        ))
        db.send_create_signal(u'course', ['Class'])

        # Adding unique constraint on 'Class', fields ['course', 'number']
        db.create_unique(u'course_class', ['course_id', 'number'])

        # Adding M2M table for field left_students on 'Class'
        m2m_table_name = db.shorten_name(u'course_class_left_students')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('class', models.ForeignKey(orm[u'course.class'], null=False)),
            ('user', models.ForeignKey(orm[u'profiles.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['class_id', 'user_id'])

        # Adding model 'Note'
        db.create_table(u'course_note', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('course', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'notes', to=orm['course.Course'])),
        ))
        db.send_create_signal(u'course', ['Note'])

        # Adding M2M table for field attached_files on 'Note'
        m2m_table_name = db.shorten_name(u'course_note_attached_files')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('note', models.ForeignKey(orm[u'course.note'], null=False)),
            ('attachedfiles', models.ForeignKey(orm[u'course.attachedfiles'], null=False))
        ))
        db.create_unique(m2m_table_name, ['note_id', 'attachedfiles_id'])

        # Adding model 'Message'
        db.create_table(u'course_message', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('created', self.gf('model_utils.fields.AutoCreatedField')(default=datetime.datetime.now)),
            ('modified', self.gf('model_utils.fields.AutoLastModifiedField')(default=datetime.datetime.now)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('course', self.gf('django.db.models.fields.related.ForeignKey')(related_name=u'messages', to=orm['course.Course'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profiles.User'])),
            ('is_active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'course', ['Message'])


    def backwards(self, orm):
        # Removing unique constraint on 'Class', fields ['course', 'number']
        db.delete_unique(u'course_class', ['course_id', 'number'])

        # Removing unique constraint on 'Course', fields ['teacher', 'title']
        db.delete_unique(u'course_course', ['teacher_id', 'title'])

        # Deleting model 'Currency'
        db.delete_table(u'course_currency')

        # Deleting model 'AttachedFiles'
        db.delete_table(u'course_attachedfiles')

        # Deleting model 'MaterialForLearning'
        db.delete_table(u'course_materialforlearning')

        # Deleting model 'Course'
        db.delete_table(u'course_course')

        # Removing M2M table for field matterials_for_learning on 'Course'
        db.delete_table(db.shorten_name(u'course_course_matterials_for_learning'))

        # Removing M2M table for field students on 'Course'
        db.delete_table(db.shorten_name(u'course_course_students'))

        # Deleting model 'Class'
        db.delete_table(u'course_class')

        # Removing M2M table for field left_students on 'Class'
        db.delete_table(db.shorten_name(u'course_class_left_students'))

        # Deleting model 'Note'
        db.delete_table(u'course_note')

        # Removing M2M table for field attached_files on 'Note'
        db.delete_table(db.shorten_name(u'course_note_attached_files'))

        # Deleting model 'Message'
        db.delete_table(u'course_message')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'cities_light.city': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('region', 'name'), ('region', 'slug'))", 'object_name': 'City'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'feature_code': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '5', 'blank': 'True'}),
            'longitude': ('django.db.models.fields.DecimalField', [], {'null': 'True', 'max_digits': '8', 'decimal_places': '5', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'db_index': 'True'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'population': ('django.db.models.fields.BigIntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'region': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Region']", 'null': 'True', 'blank': 'True'}),
            'search_names': ('cities_light.models.ToSearchTextField', [], {'default': "''", 'max_length': '4000', 'db_index': 'True', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"})
        },
        u'cities_light.country': {
            'Meta': {'ordering': "['name']", 'object_name': 'Country'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'code2': ('django.db.models.fields.CharField', [], {'max_length': '2', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'code3': ('django.db.models.fields.CharField', [], {'max_length': '3', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'continent': ('django.db.models.fields.CharField', [], {'max_length': '2', 'db_index': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '200'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"}),
            'tld': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '5', 'blank': 'True'})
        },
        u'cities_light.region': {
            'Meta': {'ordering': "['name']", 'unique_together': "(('country', 'name'), ('country', 'slug'))", 'object_name': 'Region'},
            'alternate_names': ('django.db.models.fields.TextField', [], {'default': "''", 'null': 'True', 'blank': 'True'}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Country']"}),
            'display_name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'geoname_code': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '50', 'null': 'True', 'blank': 'True'}),
            'geoname_id': ('django.db.models.fields.IntegerField', [], {'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '200', 'db_index': 'True'}),
            'name_ascii': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '200', 'blank': 'True'}),
            'slug': ('autoslug.fields.AutoSlugField', [], {'unique_with': '()', 'max_length': '50', 'populate_from': "'name_ascii'"})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'course.attachedfiles': {
            'Meta': {'object_name': 'AttachedFiles'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'})
        },
        u'course.class': {
            'Meta': {'ordering': "[u'course', u'number']", 'unique_together': "((u'course', u'number'),)", 'object_name': 'Class'},
            'course': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'classes'", 'to': u"orm['course.Course']"}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            'duration': ('django.db.models.fields.PositiveIntegerField', [], {'default': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'left_students': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'left_students'", 'symmetrical': 'False', 'to': u"orm['profiles.User']"}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'number': ('django.db.models.fields.IntegerField', [], {}),
            'time_begin': ('django.db.models.fields.TimeField', [], {})
        },
        u'course.course': {
            'Meta': {'unique_together': "((u'teacher', u'title'),)", 'object_name': 'Course'},
            'amount_classes': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'category_courses'", 'to': u"orm['profiles.StudySubject']"}),
            'city': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.City']"}),
            'country': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['cities_light.Country']"}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'cr_courses'", 'to': u"orm['profiles.User']"}),
            'currency': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['course.Currency']"}),
            'date_begin': ('django.db.models.fields.DateField', [], {}),
            'date_end': ('django.db.models.fields.DateField', [], {}),
            'detail': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'duration': ('django.db.models.fields.PositiveIntegerField', [], {'default': '60'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'materials_is_free': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'matterials_for_learning': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'course_materials'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['course.MaterialForLearning']"}),
            'max_sits_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '20'}),
            'min_sits_count': ('django.db.models.fields.PositiveIntegerField', [], {'default': '10'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'price_each_class': ('course.fields.PriceField', [], {'default': '0', 'max_digits': '10', 'decimal_places': '2'}),
            'students': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "u'students'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['profiles.User']"}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'th_courses'", 'to': u"orm['profiles.User']"}),
            'time_begin': ('django.db.models.fields.TimeField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'course.currency': {
            'Meta': {'object_name': 'Currency'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'course.materialforlearning': {
            'Meta': {'object_name': 'MaterialForLearning'},
            'attached_files': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'material_files'", 'to': u"orm['course.AttachedFiles']"}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            'description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'course.message': {
            'Meta': {'object_name': 'Message'},
            'course': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'messages'", 'to': u"orm['course.Course']"}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profiles.User']"})
        },
        u'course.note': {
            'Meta': {'object_name': 'Note'},
            'attached_files': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "u'course_files'", 'symmetrical': 'False', 'to': u"orm['course.AttachedFiles']"}),
            'course': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "u'notes'", 'to': u"orm['course.Course']"}),
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'text': ('django.db.models.fields.TextField', [], {})
        },
        u'profiles.studysubject': {
            'Meta': {'object_name': 'StudySubject'},
            'created': ('model_utils.fields.AutoCreatedField', [], {'default': 'datetime.datetime.now'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('model_utils.fields.AutoLastModifiedField', [], {'default': 'datetime.datetime.now'}),
            'title': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '255'})
        },
        u'profiles.user': {
            'Meta': {'object_name': 'User'},
            'activation_key': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'activation_key_created': ('django.db.models.fields.DateField', [], {'null': 'True'}),
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        }
    }

    complete_apps = ['course']