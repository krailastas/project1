from __future__ import unicode_literals

from datetime import datetime

from django.db.models import Count

from rest_framework import filters

from course.models import Course, Class


class FilterCourseMyFinishedBackend(filters.DjangoFilterBackend):
    class Meta:
        model = Course

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterCourseMyFinishedBackend, self).filter_queryset(request, queryset, view)
        if request.user and request.user.is_authenticated():
            my_finished = request.QUERY_PARAMS.get('my_finished', None)
            if (my_finished is not None):
                queryset = request.user.students.filter(date_end__lt=datetime.today())
                courses = []
                for course in queryset:
                    if request.user.left_classes.filter(course=course).count() != course.amount_classes:
                        courses.append(course)
                return courses
        return queryset


class FilterClassPastUpcomingBackend(filters.DjangoFilterBackend):

    class Meta:
        model = Class

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterClassPastUpcomingBackend, self).filter_queryset(request, queryset, view)

        upcoming = request.QUERY_PARAMS.get('upcoming', None)
        if (upcoming is not None):
            queryset = queryset.filter(date__gte=datetime.today())

        past = request.QUERY_PARAMS.get('past', None)
        if (past is not None):
            queryset = queryset.filter(date__lt=datetime.today())

        return queryset.order_by('date')


class FilterCourseOngoingUpcomingBackend(filters.DjangoFilterBackend):

    class Meta:
        model = Course

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterCourseOngoingUpcomingBackend, self).filter_queryset(request, queryset, view)

        ongoing = request.QUERY_PARAMS.get('ongoing', None)
        upcoming = request.QUERY_PARAMS.get('upcoming', None)

        if (ongoing is not None) or (upcoming is not None):
            if (ongoing is not None):
                queryset = queryset.filter(date_end__gt=datetime.today(), date_begin__lte=datetime.today())
            else:
                queryset = queryset.filter(date_begin__gte=datetime.today())

        return queryset


class FilterCoursePastBackend(filters.DjangoFilterBackend):

    class Meta:
        model = Course

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterCoursePastBackend, self).filter_queryset(request, queryset, view)

        past = request.QUERY_PARAMS.get('past', None)
        if past is not None:
            return queryset.filter(date_end__lt=datetime.today())

        return queryset


class FilterCoursePopularityBackend(filters.DjangoFilterBackend):
    class Meta:
        model = Course

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterCoursePopularityBackend, self).filter_queryset(request, queryset, view)

        most_popular = request.QUERY_PARAMS.get('most_popular', None)
        if most_popular is not None:
            queryset = queryset.annotate(students_count=Count('students')).order_by('-students_count')

        return queryset


class FilterCoursePriceBackend(filters.DjangoFilterBackend):
    class Meta:
        model = Course

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterCoursePriceBackend, self).filter_queryset(request, queryset, view)

        price = request.QUERY_PARAMS.get('price', None)
        if price is not None:
            queryset = queryset.extra(select={'total_price': 'amount_classes * price_each_class'}).extra(order_by=['total_price'])

        return queryset


class FilterCourseFollowedTeacherBackend(filters.DjangoFilterBackend):

    class Meta:
        model = Course

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterCourseFollowedTeacherBackend, self).filter_queryset(request, queryset, view)

        if request.user and request.user.is_authenticated():
            followed = request.QUERY_PARAMS.get('followed', None)
            if followed is not None:
                queryset = queryset.filter(teacher__in=request.user.get_followed_teachers())

        return queryset


class FilterCourseMyTeachingBackend(filters.DjangoFilterBackend):

    class Meta:
        model = Course

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterCourseMyTeachingBackend, self).filter_queryset(request, queryset, view)

        if request.user and request.user.is_authenticated():
            my_teaching = request.QUERY_PARAMS.get('my_teaching', None)
            if my_teaching is not None:
                queryset = queryset.filter(teacher=request.user)

        return queryset


class FilterCourseRegisteredBackend(filters.DjangoFilterBackend):

    class Meta:
        model = Course

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterCourseRegisteredBackend, self).filter_queryset(request, queryset, view)
        if request.user and request.user.is_authenticated():
            registered = request.QUERY_PARAMS.get('registered', None)
            if registered is not None:
                queryset = queryset.filter(students=request.user)

        return queryset


class FilterMyClassesBackend(filters.DjangoFilterBackend):

    class Meta:
        model = Class

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterMyClassesBackend, self).filter_queryset(request, queryset, view)

        if request.user and request.user.is_authenticated():
            my_classes = request.QUERY_PARAMS.get('my_classes', None)
            if my_classes is not None:
                queryset = queryset.filter(course__in=request.user.students.all())

        return queryset
