class CourseErrorMesageMixin(object):
    ERR_MSG = {
        'creator_only_current_user': 'Only request.user can be creator.',
        'must_be_teacher': 'must be a teacher',
        'students_count_more_than_max_sits_count': "student's count can't be more than max_sits_count",
        'course_class_count_error': 'course must have at least two classes and less than 99',
        'must_be_in_future': 'must be in the future',
        'course_duration_error': 'duration of the course must be at least one day and less than 365 days',
        'class_duration_error': "class duration can't be more than 1440 minutes",
        'amount_classes_error': 'amount classes must be less than days count in course',
        'min_sits_count_error': 'min sits count must be less than max sits count',
    }
