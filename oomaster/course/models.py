# encoding: utf-8
from __future__ import unicode_literals

from datetime import timedelta

from django.db.models.signals import post_save
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

from model_utils.models import TimeStampedModel
from model_utils import Choices
from cities_light.models import City, Country
from south.modelsinspector import add_introspection_rules

from profiles.models import User, StudySubject, Image
from course.fields import PriceField


add_introspection_rules([], ["^course\.fields\.PriceField"])


class Currency(TimeStampedModel):
    """
    **Currency Model**

        Model for store currencies. Currencies are created in admin panel.
        **list fields:**

        * `title` - CharField
        * `symbol` - CharField
    """
    title = models.CharField(verbose_name=_('title'), max_length=255, unique=True)
    symbol = models.CharField(verbose_name=_('symbol'), max_length=16, unique=True)

    class Meta:
        verbose_name = _('Currency')
        verbose_name_plural = _('Currencies')

    def __str__(self):
        return self.title


class AttachedFiles(TimeStampedModel):
    """
    **AttachedFiles Model**
        Model for store attached files. Teachers can attach files to materials or learning and notes.

        **list fields:**

        * `file` - FileField(upload_to='course/attachedfiles')
    """
    file = models.FileField(verbose_name=_('file'), upload_to='course/attachedfiles')

    class Meta:
        verbose_name = _('Attached file')
        verbose_name_plural = _('Attached files')

    def __str__(self):
        return self.file.url


class Course(TimeStampedModel):
    """
    **Course Model**
        Model for store courses. Only teacher can create course. Only course's owner can modify course.
        Classes for every course will be generated automatically after the creation of the course.

        **list fields:**

        * `creator` - ForeignKey - profiles.User
        * `teacher` - ForeignKey - profiles.User
        * `title` - CharField
        * `detail` - TextField
        * `date_begin` - DateField
        * `date_end` - DateField
        * `amount_classes` - PositiveIntegerField
        * `city` - ForeignKey - cities_light.City
        * `country` - ForeignKey - cities_light.Country
        * `matterials_for_learning` - CharField
        * `min_sits_count` - PositiveIntegerField
        * `max_sits_count` - PositiveIntegerField
        * `currency` - ForeignKey - Currency
        * `price_each_class` - PriceField
        * `price_matterials_for_learning` - PriceField
        * `location` - CharField(coordinates)
        * `students` - ManyToManyField - profiles.User
        * `time_begin` - TimeField
        * `duration` - PositiveIntegerField(minutes)
        * `image` - ForeignKey - Image
        * `category` - ForeignKey - StudySubject
        * `materials_is_free` - BooleanField
        * `is_canceled` - BooleanField
        * `target_students` - CharField(level of students)
    """
    creator = models.ForeignKey(User, verbose_name=_('creator'), related_name='cr_courses')
    teacher = models.ForeignKey(User, verbose_name=_('teacher'), related_name='th_courses')
    title = models.CharField(verbose_name=_('title'), max_length=255)
    detail = models.TextField(verbose_name=_('detail'), blank=True)
    date_begin = models.DateField(verbose_name=_('date begin'))
    date_end = models.DateField(verbose_name=_('date end'))
    amount_classes = models.PositiveIntegerField(verbose_name=_('amount classes'), default=1)
    city = models.ForeignKey(City, verbose_name=_('city'))
    country = models.ForeignKey(Country, verbose_name=_('country'))
    matterials_for_learning = models.CharField(max_length=512, blank=True)
    min_sits_count = models.PositiveIntegerField(verbose_name=_('min sits count'), default=10)
    max_sits_count = models.PositiveIntegerField(verbose_name=_('max sits count'), default=20)
    currency = models.ForeignKey(Currency, verbose_name=_('currency'))
    price_each_class = PriceField(verbose_name=_('price each class'), max_digits=10, decimal_places=2, default=0)
    location = models.CharField(verbose_name=_('location'), max_length=255)
    students = models.ManyToManyField(User, verbose_name=_('students'), related_name='students', null=True, blank=True)
    time_begin = models.TimeField(verbose_name=_('time_begin'))
    duration = models.PositiveIntegerField(verbose_name=_('duration'), default=60)
    image = models.ForeignKey(Image, related_name='course_image', null=True, blank=True)
    category = models.ForeignKey(StudySubject, verbose_name=_('category'), related_name='category_courses')
    materials_is_free = models.BooleanField(verbose_name=_('free materials'), default=False)
    is_canceled = models.BooleanField(verbose_name=_('course is canceled'), default=False)
    price_matterials_for_learning = PriceField(verbose_name=_('price'), max_digits=10, decimal_places=2, default=0)
    target_students = models.CharField(max_length=256, blank=True)
    is_published = models.BooleanField(default=False)

    class Meta:
        verbose_name = _('Course')
        verbose_name_plural = _('Courses')
        unique_together = ('teacher', 'title')

    def __str__(self):
        return self.title


class CourseAttachedFile(TimeStampedModel):
    course = models.ForeignKey(Course, related_name='course_attached_files')
    title = models.CharField(max_length=128)
    attached_file = models.ForeignKey(
        AttachedFiles,
        related_name='course_attached_files',
        null=True,
    )
    description = models.TextField(blank=True)

    class Meta:
        unique_together = ('course', 'title')

    def __str__(self):
        return self.title


class CourseInWishlist(TimeStampedModel):
    """
    **CourseInWishlist Model**
        Model for store user's wishlists.

        **list fields:**

        * `user` - ForeignKey - profiles.User
        * `course` - ForeignKey - course.Course
    """
    user = models.ForeignKey(User, related_name='wishlist')
    course = models.ForeignKey(Course, related_name='users_wishlist')

    class Meta:
        unique_together = ('user', 'course')

    def __str__(self):
        return 'user {} wish {} course'.format(self.user.id, self.course.id)


class Review(TimeStampedModel):
    """
    **Review Model**
        Model for store user's reviws. Then course was ended
        only course member can create one review for this course.

        **list fields:**

        * `author` - ForeignKey - profiles.User
        * `course` - ForeignKey - course.Course
        * `message` - TextField
        * `rating` - PositiveIntegerField - rating, min 1, max 5
    """
    RATING = Choices(*range(1, 6))

    author = models.ForeignKey(User, related_name='author_reviews')
    course = models.ForeignKey(Course, related_name='course_reviews')
    message = models.TextField(blank=True)
    rating = models.PositiveIntegerField(
        choices=RATING,
        default=3
    )

    class Meta:
        unique_together = ('author', 'course')

    def __str__(self):
        return 'author {}, course{}'.format(self.author, self.course)


class Class(TimeStampedModel):
    """
    **Class Model**
        Model for store classes.
        Every course consist of lessons.
        Classes are created automatically when course will be created.
        Only course.teacher and course.creator can modify classes.

        **list fields:**

        * `course` - ForeignKey - Course
        * `number` - IntegerField
        * `date` - DateField
        * `teacher` - ForeignKey - profiles.User
        * `location` - CharField
        * `left_students` - ManyToManyField - profiles.User
        * `time_begin` - TimeField
        * `duration` - PositiveIntegerField(minutes)
        * `description` - TextField
    """
    course = models.ForeignKey(Course, verbose_name=_('course'), related_name='classes')
    title = models.CharField(max_length=255)
    number = models.IntegerField(verbose_name=_('number'))
    teacher = models.ForeignKey(User, verbose_name=_('teacher'), related_name='th_classes')
    date = models.DateField(verbose_name=_('date'))
    location = models.CharField(verbose_name=_('location'), max_length=255)
    left_students = models.ManyToManyField(User, verbose_name=_('left students'), related_name='left_classes', blank=True)
    time_begin = models.TimeField(verbose_name=_('time_begin'))
    duration = models.PositiveIntegerField(verbose_name=_('duration'), default=60)
    description = models.TextField(blank=True)

    class Meta:
        verbose_name = _('Class')
        verbose_name_plural = _('Classes')
        unique_together = ('course', 'number')
        ordering = ['course', 'number']

    def __str__(self):
        return 'course: {} - number: {}'.format(self.course_id, self.number)


class Note(TimeStampedModel):
    """
    **Note Model**
        Model for store notes. It is footnote for courses.
        Only course's teacher and course's creator can create, modify and delete it.

        **list fields:**

        * `text` - TextField
        * `course` - ForeignKey - Course
        * `attached_files` - ManyToManyField - AttachedFiles
    """
    text = models.TextField(verbose_name=_('text'))
    course = models.ForeignKey(Course, verbose_name=_('course'), related_name='notes')
    attached_files = models.ManyToManyField(
        AttachedFiles,
        verbose_name=_('attached files'),
        related_name='notes',
        null=True,
    )

    class Meta:
        verbose_name = _('Note')
        verbose_name_plural = _('Notes')

    def __str__(self):
        return self.text


class Message(TimeStampedModel):
    """
    **Message Model**
        Model for store messages. Every course member and course teacher can create and modify course's message.

        **list fields:**

        * `text` - TextField
        * `course` - ForeignKey - Course
        * `author` - ForeignKey - User
        * `is_active` - BooleanField
    """
    text = models.TextField(verbose_name=_('text'))
    course = models.ForeignKey(Course, verbose_name=_('course'), related_name='messages')
    author = models.ForeignKey(User, verbose_name=_('user'))
    is_active = models.BooleanField(verbose_name=_('active'), default=True)

    class Meta:
        verbose_name = _('Message')
        verbose_name_plural = _('Messages')

    def __str__(self):
        return self.text


class FollowedTeacher(TimeStampedModel):
    """
    **Message Model**
        Model for store information about followed teacher. User can't follow himself and can't follow not teacher.

        **list fields:**

        * `user` - ForeignKey - profiles.User
        * `teacher` - ForeignKey - profiles.User
    """
    user = models.ForeignKey(User, related_name='followed_teachers')
    teacher = models.ForeignKey(User, related_name='followers')

    class Meta:
        unique_together = ('teacher', 'user')

    def __str__(self):
        return '{0} follow {1}'.format(self.user.id, self.teacher.id)

    def clean(self):
        if self.teacher == self.user:
            raise ValidationError("User can't follow himself")

        if self.teacher.profile.is_teacher is False:
            raise ValidationError("Can't follow not teacher")


class TeacherInTeacherListManager(models.Manager):
    def is_teacher_in_list(self, list_owner, teacher_in_list):
        return self.filter(list_owner=list_owner, teacher_in_list=teacher_in_list).count() > 0


class TeacherInTeacherList(TimeStampedModel):
    """
    **TeacherInTeacherList Model**
        Model for store teacher's lists with other teachers.

        **list fields:**

        * `list_owner` - ForeignKey - profiles.User
        * `teacher_in_list` - ForeignKey - profiles.User
    """

    list_owner = models.ForeignKey(User, related_name='list_owners')
    teacher_in_list = models.ForeignKey(User, related_name='list_members')

    objects = TeacherInTeacherListManager()

    class Meta:
        unique_together = ('list_owner', 'teacher_in_list')

    def __str__(self):
        return "user {} in {}'s list".format(self.list_owner, self.teacher_in_list)


class TeacherInvitation(TimeStampedModel):
    """
    **TeacherInvitation Model**
        Model for store teacher's invitation to teachers list.
        It is like friendship invitation in Facebook.
        If user accept our invitation to your list,
        automaticaly will be created new record in TeacherInTeacherList.

        **list fields:**

        * `from_user` - ForeignKey - profiles.User
        * `to_user` - ForeignKey - profiles.User
        * `status` - CharField - ('Accepted', 'Declined', 'Sent')
    """
    INVITATION_STATUSES = Choices('Accepted', 'Declined', 'Sent')

    from_user = models.ForeignKey(User, related_name='invitation_from')
    to_user = models.ForeignKey(User, related_name='invitation_to')
    status = models.CharField(
        _('Status'),
        choices=INVITATION_STATUSES,
        default=INVITATION_STATUSES.Sent,
        max_length=100
    )

    class Meta:
        unique_together = ('from_user', 'to_user')

    def __str__(self):
        return 'from {} to {}'.format(self.from_user, self.to_user)

    def accept(self):
        user_in_list = TeacherInTeacherList(list_owner=self.from_user, teacher_in_list=self.to_user)
        user_in_list.save()

    def save(self, *args, **kwargs):
        if self.status == self.INVITATION_STATUSES.Accepted:
            self.accept()
        super(TeacherInvitation, self).save(*args, **kwargs)


def create_clases(sender, instance, created, **kwargs):
    if created and instance.amount_classes:
        ins = instance
        interval_between_classes = (ins.date_end - ins.date_begin + timedelta(1)) / ins.amount_classes

        for i in range(ins.amount_classes):
            new_class = Class.objects.create(
                course=instance,
                title="{} class of {}".format(i + 1, instance.title),
                number=i + 1,
                location=ins.location,
                time_begin=ins.time_begin,
                duration=ins.duration,
                teacher=ins.teacher,
                date=ins.date_begin + (interval_between_classes * i)
            )

            # if last and amount_classes more than 1 then date = course.date_end
            if i == ins.amount_classes - 1 and ins.amount_classes > 1:
                new_class.date = ins.date_end

            new_class.save()

post_save.connect(create_clases, sender=Course)
