from datetime import datetime, timedelta
import os

from django.conf import settings

from rest_framework.test import APITestCase
from rest_framework.reverse import reverse
from rest_framework import status

from custom_cities_light.factories import CityFactory, CountryFactory
from profiles.models import User, Profile
from profiles.factories import UserFactory, StudySubjectFactory

from course.models import (Course, Currency, Class,
                           CourseAttachedFile, CourseInWishlist, Review,
                           AttachedFiles, FollowedTeacher, TeacherInvitation,
                           Note, Message)

from course.factories import CourseFactory
from course.mixins import CourseErrorMesageMixin


ERR_MSG = CourseErrorMesageMixin.ERR_MSG


class TestCourseViewSet(APITestCase):
    list_url = reverse('courses-list')
    detail_url = ''

    def setUp(self):
        super(TestCourseViewSet, self).setUp()
        CourseFactory.create_batch(5)

        user = UserFactory.create()
        user.profile.is_teacher = True
        user.profile.save()
        self.user = user

        self.second_user = UserFactory.create()

        self.course = CourseFactory.create(teacher=self.user)

        self.detail_url = reverse('courses-detail', kwargs={'pk': self.course.id})

        self.city = CityFactory.create()

        self.country = CountryFactory.create()

        cur = Currency(title='QWE', symbol='$$')
        cur.save()
        self.currency = cur

        self.study_sbj = StudySubjectFactory.create()

    def test_get(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 6)

    def test_post(self):
        # unauthorized user can't create course
        response = self.client.post(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # success creating course
        user = self.user
        self.client.force_authenticate(user)
        valid_data = {
            'creator': user.id,
            'teacher': user.id,
            'title': 'qwerty',
            'detail': 'qwerty detail',
            'date_begin': datetime.today().date(),
            'date_end': datetime.today().date() + timedelta(10),
            'amount_classes': 5,
            'city': self.city.pk,
            'country': self.country.pk,
            'matterials_for_learning': 'qwerty, qwerty',
            'min_sits_count': 2,
            'max_sits_count': 4,
            'currency': self.currency.pk,
            'price_each_class': 100,
            'location': 'qwerty',
            'time_begin': '10:00',
            'duration': '60',
            'category': self.study_sbj.id,
            'materials_is_free': True,
            'is_canceled': False,
            'price_matterials_for_learning': 30,
            'target_students': 'low level',
            'is_published': True,
        }
        old_class_count = Class.objects.count()
        response = self.client.post(self.list_url, valid_data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.get(self.list_url)
        self.assertEqual(response.data['count'], 7)
        self.assertEqual(Class.objects.count(), old_class_count + 5)

        # invalid sits count
        data = valid_data
        data['min_sits_count'] = 5
        response = self.client.post(self.list_url, data)
        self.assertEqual(ERR_MSG['min_sits_count_error'] in str(response.data), True)

        # invalid amount classes
        data = valid_data
        data['amount_classes'] = 99
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['amount_classes_error'] in str(response.data), True)

        # too much amount classes
        # course creator must be teacher
        # course teacher must be teacher
        # date_begin is in past
        # date_end is in past
        # invalid price each class
        # invalid class duration
        user.profile.is_teacher = False
        user.profile.save()
        data = {
            'amount_classes': 999,
            'date_begin': datetime.today().date() - timedelta(10),
            'date_end': datetime.today().date() - timedelta(1),
            'creator': self.user.id,
            'teacher': self.user.id,
            'price_each_class': -30,
            'duration': 1441,
        }
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['class_duration_error'] in str(response.data), True)
        self.assertEqual('value must be a positive number' in str(response.data['price_each_class'][0]), True)
        self.assertEqual(ERR_MSG['course_class_count_error'] in str(response.data), True)
        self.assertEqual(str(response.data['creator'][0]), ERR_MSG['must_be_teacher'])
        self.assertEqual(str(response.data['teacher'][0]), ERR_MSG['must_be_teacher'])
        self.assertEqual(str(response.data['date_end'][0]), ERR_MSG['must_be_in_future'])
        self.assertEqual(str(response.data['date_begin'][0]), ERR_MSG['must_be_in_future'])

        # only request.user can be creator
        # course duration error
        data = {
            'date_begin': datetime.today().date(),
            'date_end': datetime.today().date() + timedelta(367),
            'creator': self.second_user.id,
        }
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['course_duration_error'] in str(response.data), True)
        self.assertEqual(ERR_MSG['creator_only_current_user'] in str(response.data), True)

        # too much students
        UserFactory.create_batch(5)
        data = valid_data
        data['students'] = User.objects.all().values_list('id', flat=True)
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['students_count_more_than_max_sits_count'] in str(response.data), True)


class TestClassesViewSet(APITestCase):
    list_url = reverse('classes-list')
    detail_url = ''

    def setUp(self):
        super(TestClassesViewSet, self).setUp()
        CourseFactory.create_batch(2)

        user = UserFactory.create()
        user.profile.is_teacher = True
        user.profile.save()
        self.user = user

        course = Course.objects.first()
        self.valid_data = {
            'course': course.id,
            'title': 'qwerty',
            'number': 1,
            'teacher': self.user.id,
            'date': datetime.today().date(),
            'location': 'qwerty',
            'time_begin': '10:00',
            'duration': '100',
        }

        self.detail_url = reverse('courses-detail', kwargs={'pk': course.classes.first().id})

    def test_get(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected_class_count = sum(Course.objects.all().values_list('amount_classes', flat=True))
        self.assertEqual(response.data['count'], expected_class_count)

    def test_post(self):
        self.client.force_authenticate(self.user)
        response = self.client.post(self.list_url, self.valid_data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class TestCourseAttachedFileViewSet(APITestCase):

    list_url = reverse('course_attache_files-list')

    def setUp(self):
        super(TestCourseAttachedFileViewSet, self).setUp()
        self.filepath = os.path.join(settings.MEDIA_ROOT, 'example.txt')
        self.file = open(self.filepath, 'w')
        self.file.write('qwerty qwerty')
        self.file.close()

        user = UserFactory.create()
        user.profile.is_teacher = True
        user.profile.save()
        self.user = user

        self.second_user = UserFactory.create()

        attached_file = AttachedFiles(file='example.txt')
        attached_file.save()

        self.course = CourseFactory.create(teacher=user)
        for i in xrange(10):
            at_file = CourseAttachedFile(
                course=self.course,
                title='title {}'.format(i),
                attached_file=attached_file,
            )
            at_file.save()

        self.attached_file = attached_file

    def test_get(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 10)

    def test_post(self):
        response = self.client.post(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # empty data
        self.client.force_authenticate(self.user)
        response = self.client.post(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # successfully creation of AttachedFiles
        data = {
            'course': self.course.id,
            'title': 'qwerty',
            'attached_file': self.attached_file.id,

        }
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.get(self.list_url)
        self.assertEqual(response.data['count'], 11)

        # only course creator can add file to course
        self.client.force_authenticate(self.second_user)
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def tearDown(self):
        os.remove(self.filepath)


class TestCourseInWishlistViewSet(APITestCase):
    list_url = reverse('wishlists-list')

    def setUp(self):
        super(TestCourseInWishlistViewSet, self).setUp()

        self.course = CourseFactory.create()
        self.user = UserFactory.create()
        c = CourseInWishlist(course=self.course, user=self.user)
        c.save()
        self.second_user = UserFactory.create()
        c = CourseInWishlist(course=self.course, user=self.second_user)
        c.save()

        self.second_course = CourseFactory.create()

    def test_get(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.data['count'], 2)

    def test_post(self):

        data = {
            'user': self.user.id,
            'course': self.second_course.id,
        }

        self.client.force_authenticate(self.user)
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = self.client.get(self.list_url)
        self.assertEqual(response.data['count'], 3)


class TestReviewViewSet(APITestCase):
    list_url = reverse('reviews-list')

    def setUp(self):
        super(TestReviewViewSet, self).setUp()

        # create course
        self.teacher = UserFactory.create()
        self.teacher.profile.is_teacher = True
        self.teacher.profile.save()
        self.course = CourseFactory.create(
            teacher=self.teacher,
            date_begin=datetime.today().date() - timedelta(10),
            date_end=datetime.today().date() - timedelta(1),
        )

        # create review
        self.review_creator = UserFactory.create()
        r = Review(
            author=self.review_creator,
            course=self.course,
            message='qwerty qwerty',
            rating=Review.RATING[4],
        )
        r.save()
        self.review = r

        self.second_review_creator = UserFactory.create()

        # set review_creator as course member
        self.course.students.add(self.second_review_creator)

    def test_get(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.data['count'], 1)

        # check teacher's rating
        response = self.client.get(reverse('users-list'))
        self.assertEqual(response.data['results'][0]['rating'], 4)

    def test_post(self):
        data = {
            'author': self.second_review_creator.id,
            'course': self.course.id,
            'rating': Review.RATING[2]
        }
        # no authorized
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # no course member
        self.client.force_authenticate(self.review_creator)
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # successfully creation
        self.client.force_authenticate(self.second_review_creator)
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # check review count
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)

        # check teacher rating
        response = self.client.get(reverse('users-list'))
        self.assertEqual(response.data['results'][0]['rating'], 3)

        # can't create second review for course
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestFollowedTeacherViewSet(APITestCase):
    list_url = reverse('followed_teacher-list')

    def setUp(self):
        super(TestFollowedTeacherViewSet, self).setUp()

        self.teacher, self.secont_teacher = UserFactory.create_batch(2)
        for profile in Profile.objects.all():
            profile.is_teacher = True
            profile.save()
        self.user = UserFactory.create()
        self.followed_teacher = FollowedTeacher(
            user=self.user,
            teacher=self.teacher
        )
        self.followed_teacher.save()

        self.third_teacher = UserFactory.create()

    def test_get(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 1)

    def test_post(self):
        data = {
            'user': self.user.id,
            'teacher': self.secont_teacher.id,
        }

        # no authorized
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # successfully creation
        self.client.force_authenticate(self.user)
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # check FollowedTeacher count
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 2)

        # can't follow twice
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # can't create not a teacher
        data['teacher'] = self.third_teacher.id
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestTeacherList(APITestCase):
    invitation_list_url = reverse('teacher_invitation-list')
    teacher_list_list_url = reverse('teacher_in_teacher_list-list')

    def setUp(self):
        super(TestTeacherList, self).setUp()

        self.inviter = UserFactory.create()
        self.teacher = UserFactory.create()
        Profile.objects.update(is_teacher=True)
        self.user = UserFactory.create()

    def test_post(self):
        data = {
            'from_user': self.inviter.id,
            'to_user': self.teacher.id,
        }

        # no authorized
        response = self.client.post(self.invitation_list_url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        response = self.client.post(self.teacher_list_list_url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # successfully creation
        self.client.force_authenticate(self.inviter)
        response = self.client.post(self.invitation_list_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # check invitations count
        response = self.client.get(self.invitation_list_url)
        self.assertEqual(response.data['count'], 1)

        # check TeacherInTeacherList count
        response = self.client.get(self.teacher_list_list_url)
        self.assertEqual(response.data['count'], 0)

        # check acceptance invitation
        invitation = TeacherInvitation.objects.first()
        invitation.status = TeacherInvitation.INVITATION_STATUSES.Accepted
        invitation.save()
        response = self.client.get(self.teacher_list_list_url)
        self.assertEqual(response.data['count'], 1)

        # can't create invitation for not a teacher
        data['to_user'] = self.user.id
        response = self.client.post(self.invitation_list_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # not a teacher can't create invitation
        self.client.force_authenticate(self.user)
        data['to_user'] = self.inviter.id
        response = self.client.post(self.invitation_list_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class TestCurrencyViewSet(APITestCase):
    list_url = reverse('currencies-list')

    def setUp(self):
        super(TestCurrencyViewSet, self).setUp()
        Currency.objects.all().delete()
        for i in xrange(5):
            c = Currency(title='currency_{}'.format(i), symbol='symbol_{}'.format(i))
            c.save()
        self.user = UserFactory.create()

    def test_get(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 5)

    def test_post(self):
        self.client.force_authenticate(self.user)
        response = self.client.post(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class TestNoteViewSet(APITestCase):
    list_url = reverse('notes-list')

    def setUp(self):
        super(TestNoteViewSet, self).setUp()
        self.filepath = os.path.join(settings.MEDIA_ROOT, 'example.txt')
        self.file = open(self.filepath, 'w')
        self.file.write('qwerty qwerty')
        self.file.close()

        self.attached_file = AttachedFiles.objects.create(file='example.txt')

        self.teacher, self.second_teacher = UserFactory.create_batch(2)
        Profile.objects.update(is_teacher=True)

        self.course = CourseFactory.create(teacher=self.teacher)

        for i in xrange(10):
            note = Note.objects.create(course=self.course, text='qwerty')
            note.attached_files.add(self.attached_file)

    def test_get(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 10)
        self.assertEqual(response.data['results'][0]['attached_files'], [AttachedFiles.objects.first().id])

    def test_post(self):
        data = {
            'course': self.course.id,
            'text': 'qwerty'
        }

        # unauthorized user can't create
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # successfully creation
        self.client.force_authenticate(self.teacher)
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Note.objects.count(), 11)

        # only course creator can add notes for this course
        self.client.force_authenticate(self.second_teacher)
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def tearDown(self):
        os.remove(self.filepath)


class TestMessageViewSet(APITestCase):
    list_url = reverse('messages-list')

    def setUp(self):
        super(TestMessageViewSet, self).setUp()

        self.teacher = UserFactory.create()
        Profile.objects.update(is_teacher=True)

        self.student = UserFactory.create()
        self.course = CourseFactory(teacher=self.teacher)
        self.course.students.add(self.student)

        for i in xrange(15):
            Message.objects.create(
                text='blah blah',
                course=self.course,
                author=self.student
            )
        # mark 5 messages as inactive
        Message.objects.filter(id__gt=10).update(is_active=False)

        self.no_member = UserFactory.create()

    def test_get(self):
        response = self.client.get(self.list_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['count'], 10)

    def test_post(self):
        data = {
            'text': 'putin huylo',
            'course': self.course.id,
            'author': self.student.id,
        }

        # unauthorized user can't create
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

        # successfully creation for course member
        self.client.force_authenticate(self.student)
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Message.objects.count(), 16)

        # successfully creation for teacher
        data['author'] = self.teacher.id
        self.client.force_authenticate(self.teacher)
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Message.objects.count(), 17)

        # only course member can create message
        self.client.force_authenticate(self.no_member)
        response = self.client.post(self.list_url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
