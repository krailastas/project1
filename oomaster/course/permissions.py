# encoding: utf-8
from __future__ import unicode_literals

from rest_framework.permissions import BasePermission, SAFE_METHODS


class CourseAttachedFilePermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        return request.user.profile.is_teacher

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return request.user in (obj.course.teacher, obj.course.creator)


class WishlistPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return request.user == obj.user


class ReviewPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return request.method == 'POST'


class FollowedTeacherPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return request.user == obj.user


class CourseIsCreatorPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.user and request.user.is_authenticated():
            return obj.creator == request.user
        return False


class UserIsTeacherPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return request.user.profile.is_teacher


class CourseIsTeacherPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj.teacher == request.user


class ClassPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return request.user in (obj.teacher, obj.course.creator, obj.course.teacher) or request.user in obj.course.students.all()


class IsCreatorOrTeacherNotePermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        return request.user in (obj.course.creator, obj.course.teacher)


class IsCreatorOrTeacherCoursePermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS and obj.is_published:
            return True
        return request.user in (obj.teacher, obj.creator)


class MessagePermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        if request.user and request.user.is_authenticated():
            if request.method == 'POST':
                return (request.user in obj.course.students.all()) or (request.user in (obj.course.creator, obj.course.teacher))
            if request.method == 'PUT':
                return False
            if request.method == 'DELETE':
                return request.user == obj.author
        return False


class TeacherInvitationPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True
        if request.user and request.user.is_authenticated():
            if request.method == 'POST':
                return request.user.profile.is_teacher
            if request.method == 'PUT':
                return obj.to_user == request.user
        return False


class TeacherInTeacherListPermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS or request.method == 'DELETE':
            return True
        return False

    def has_object_permission(self, request, view, obj):
        if request.method in SAFE_METHODS:
            return True

        if request.method == 'DELETE':
            return request.user in (obj.list_owner, obj.teacher_in_list)
        return False
