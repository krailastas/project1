# encoding: utf-8
from __future__ import unicode_literals

import random
from decimal import Decimal
from datetime import datetime, timedelta, time

from django.contrib.webdesign import lorem_ipsum

import factory

from custom_cities_light.factories import CityFactory, CountryFactory
from profiles.factories import UserFactory, StudySubjectFactory
from course.models import Course, Currency


class CourseFactory(factory.DjangoModelFactory):
    class Meta:
        model = Course

    @factory.lazy_attribute
    def title(self):
        return lorem_ipsum.words(3, False) + str(random.choice(range(10))) + str(random.choice(range(10)))

    @factory.lazy_attribute
    def date_begin(self):
        return datetime.today().date() + timedelta(days=random.choice(range(10)))

    @factory.lazy_attribute
    def date_end(self):
        return datetime.today().date() + timedelta(days=random.choice(range(11, 20)))

    @factory.lazy_attribute
    def amount_classes(self):
        return random.choice(range(3, 10))

    @factory.lazy_attribute
    def city(self):
        return CityFactory.create()

    @factory.lazy_attribute
    def country(self):
        return CountryFactory.create()

    @factory.lazy_attribute
    def min_sits_count(self):
        return random.choice(range(3, 10))

    @factory.lazy_attribute
    def max_sits_count(self):
        return random.choice(range(11, 20))

    @factory.lazy_attribute
    def currency(self):
        return Currency.objects.first() if Currency.objects.first() else None

    @factory.lazy_attribute
    def price_each_class(self):
        return Decimal(10 * random.choice(range(1, 20)))

    @factory.lazy_attribute
    def time_begin(self):
        return time(hour=10, minute=10)

    @factory.lazy_attribute
    def location(self):
        return lorem_ipsum.words(1, False)

    @factory.lazy_attribute
    def duration(self):
        return 10 * random.choice(range(1, 10))

    @factory.lazy_attribute
    def category(self):
        return StudySubjectFactory.create_batch(1)[0]

    @factory.lazy_attribute
    def creator(self):
        user = UserFactory.create_batch(1)[0]
        user.profile.is_teacher = True
        user.profile.save()
        return user

    @factory.lazy_attribute
    def teacher(self):
        user = UserFactory.create_batch(1)[0]
        user.profile.is_teacher = True
        user.profile.save()
        return user

    @factory.lazy_attribute
    def is_published(self):
        return True
