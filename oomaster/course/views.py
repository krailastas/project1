from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.viewsets import ReadOnlyModelViewSet, ModelViewSet, mixins, GenericViewSet
from rest_framework import filters
from common.views import ListMixinView

from course.models import (Currency, AttachedFiles, Course,
                           Class, Note, TeacherInTeacherList,
                           TeacherInvitation, Message, FollowedTeacher,
                           Review, CourseInWishlist, CourseAttachedFile)

from course.serializers import (CurrencySerializer, AttachedFilesSerializer, ReviewSerializer,
                                CourseSerializer, ClassSerializer, NoteSerializer,
                                MessageSerializer, TeacherInTeacherListSerializer, TeacherInvitationSerializer,
                                FollowedTeacherSerializer, CourseInWishlistSerializer,
                                CourseAttachedFileSerializer)

from course.permissions import (ClassPermission, IsCreatorOrTeacherCoursePermission, TeacherInvitationPermission,
                                UserIsTeacherPermission, MessagePermission, IsCreatorOrTeacherNotePermission,
                                TeacherInTeacherListPermission, FollowedTeacherPermission,
                                ReviewPermission, WishlistPermission, CourseAttachedFilePermission)

from course.filters import (FilterCourseOngoingUpcomingBackend, FilterCourseFollowedTeacherBackend,
                            FilterCourseMyTeachingBackend, FilterCourseRegisteredBackend,
                            FilterMyClassesBackend, FilterCoursePastBackend,
                            FilterCoursePopularityBackend, FilterCoursePriceBackend,
                            FilterClassPastUpcomingBackend, FilterCourseMyFinishedBackend)

from profiles.models import Image


class CourseAttachedFileViewSet(ModelViewSet):
    model = CourseAttachedFile
    serializer_class = CourseAttachedFileSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, CourseAttachedFilePermission)
    filter_fields = ('id', 'course')


class CourseInWishlistViewSet(ModelViewSet):
    """
    **CourseInWishlist View Set**

        Return list of CourseInWishlist.

        **returns:**

        * `user`
        * `course`

        **filters:**

        * `?user=<int>`
        * `?course=<int>`
    """
    model = CourseInWishlist
    serializer_class = CourseInWishlistSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, WishlistPermission)
    filter_fields = ('user', 'course')


class ReviewViewSet(ModelViewSet):
    """
    **Review View Set**

        Return list of Review.

        **returns:**

        * `author`
        * `course`
        * `message`
        * `rating`

        **filters:**

        * `&id=<int>`
        * `&author=<int>`
        * `&course=<int>`
        * `&course__teacher=<int>`
        * `&course__creator=<int>`
    """
    model = Review
    serializer_class = ReviewSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, ReviewPermission)
    filter_fields = ('id', 'author', 'course', 'course__teacher', 'course__creator')


class FollowedTeacherViewSet(ModelViewSet):
    """
    **FollowedTeacher View Set**

        Return list of FollowedTeacher.

        **returns:**

        * `user`
        * `teacher`

        **filters:**

        * `?user=<int>`
        * `?teacher=<int>`
    """
    model = FollowedTeacher
    serializer_class = FollowedTeacherSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, FollowedTeacherPermission)
    filter_fields = ('user', 'teacher')


class TeacherInTeacherListViewSet(ModelViewSet):
    """
    **TeacherInTeacherList View Set**

        Return list of TeacherInTeacherList.

        **returns:**

        * `list_owner`
        * `teacher_in_list`

        **filters:**

        * `?list_owner=<int>`
        * `?teacher_in_list=<int>`
    """
    model = TeacherInTeacherList
    serializer_class = TeacherInTeacherListSerializer
    permission_classes = (IsAuthenticated, TeacherInTeacherListPermission)
    filter_fields = ('list_owner', 'teacher_in_list')


class TeacherInvitationViewSet(ModelViewSet):
    """
    **TeacherInvitation View Set**

        Return list of TeacherInvitation.

        **returns:**

        * `from_user`
        * `to_user`
        * `status`

        **filters:**

        * `?from_user=<int>`
        * `?to_user=<int>`
        * `?status=[Accepted|Declined|Sent]`
    """
    model = TeacherInvitation
    serializer_class = TeacherInvitationSerializer
    permission_classes = (IsAuthenticated, TeacherInvitationPermission)
    filter_fields = ('from_user', 'to_user', 'status')


class CurrencyViewSet(ReadOnlyModelViewSet):
    """
    **Currency View Set**

        Return list of currencies. Read only view.

        **returns:**

        * `title`
        * `symbol`
    """
    model = Currency
    serializer_class = CurrencySerializer


class AttachedFilesViewSet(ModelViewSet):
    """
    **Attached Files View Set**

        Return list of study subjects.
        Update study subject.
        Create new study subject.
        Delete study subject.
        Only authenticated teacher can create/update/delete materials.


        **returns:**

        * `file`
    """
    model = AttachedFiles
    serializer_class = AttachedFilesSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, UserIsTeacherPermission)


class CourseViewSet(ListMixinView, ModelViewSet):
    """
    **Course View Set**
        Only teacher can create course.
        Only course's teacher or creator can update/delete it.

        Return list of courses.
        Update course.
        Create new course.
        Delete course.

        **returns:**


        **list fields:**

        * `creator`
        * `teacher`
        * `title`
        * `detail`
        * `date_begin`
        * `date_end`
        * `amount_classes`
        * `city`
        * `country`
        * `matterials_for_learning`
        * `min_sits_count`
        * `max_sits_count`
        * `currency`
        * `price_each_class`
        * `price_matterials_for_learning`
        * `location`
        * `students`
        * `time_begin`
        * `duration`
        * `image`
        * `category`
        * `materials_is_free`
        * `is_canceled`
        * `target_students`

        **filters:**
            * `?id=<int>`
            * `?category=<int>`
            * `?city=<int>`
            * `?date_begin=<date>`
            * `?upcoming` - date_begin in future
            * `?ongoing` - date_end in past
            * `?page=<int>` - number of page
            * `?page_size=<int>` - count courses for one page, default 6 courses
            * `?is_canceled=<[True|False]>`


        **filters for authenticated user:**
            * `?registered` - request.user in course.students
            * `?followed` - courses from followed teacher of request.user
            * `?my_teaching` - course.teacher = request.user
            * `?my_finished` - my not abandoned courses with date_end in past

        **ordering fields**
            * `ordering=date_begin`
            * `ordering=created`
            * `most_popular` - ordering by students count
            * `price` - by total price
            * `price_each_class`
    """
    queryset = Course.objects.all().select_related('teacher', 'creator', 'city', 'country', 'currency',
                                                   'category').prefetch_related('students')
    serializer_class = CourseSerializer
    paginate_by = 6
    permission_classes = (IsAuthenticatedOrReadOnly, IsCreatorOrTeacherCoursePermission, UserIsTeacherPermission)
    filter_fields = ('category', 'city', 'date_begin', 'is_canceled')
    filter_backends = (
        FilterCourseRegisteredBackend,
        FilterCourseOngoingUpcomingBackend,
        FilterCourseFollowedTeacherBackend,
        FilterCourseMyTeachingBackend,
        FilterCoursePastBackend,
        FilterCoursePopularityBackend,
        FilterCoursePriceBackend,
        FilterCourseMyFinishedBackend,
        filters.SearchFilter,
        filters.OrderingFilter,
    )
    ordering_fields = ('date_begin', 'created', 'price_each_class')
    search_fields = ('title', 'detail')

    def list(self, request, *args, **kwargs):
        kwargs.update({'qs': self.queryset.filter(is_published=True)})
        return super(CourseViewSet, self).list(request, *args, **kwargs)

    def pre_save(self, obj):
        if obj.image:
            if not obj.image.is_active:
                Image.objects.filter(pk=obj.image.pk).update(is_active=True)


class ClassViewSet(mixins.RetrieveModelMixin,
                   mixins.UpdateModelMixin,
                   mixins.ListModelMixin,
                   GenericViewSet):
    """
    **Class View Set**

        Return list of classes.
        Update class.
        Create new class.
        Delete class.
        Only course's teacher or creator can update/delete it.
        Classes are created automatically when course will be created.

        **returns:**

        * `id`
        * `created`
        * `modified`
        * `course`
        * `number`
        * `date`
        * `teacher`
        * `location`
        * `left_students`
        * `time_begin`
        * `duration`
        * `description`

        **filters:**
            * `?id=<int>`
            * `?course=<int>`
            * `?course__creator=<int>`
            * `?course__teacher=<int>`
            * `?my_classes`
    """
    queryset = Class.objects.select_related('course', 'teacher').prefetch_related('left_students')
    serializer_class = ClassSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, ClassPermission)
    filter_fields = ('id', 'course', 'course__creator', 'course__teacher')
    filter_backends = (FilterMyClassesBackend, FilterClassPastUpcomingBackend)


class NoteViewSet(ModelViewSet):
    """
    **Note View Set**

        Return list of notes.
        Update note.
        Create new note.
        Delete note.
        Only course's teacher or creator can create/update/delete it.

        **returns:**

        * `text`
        * `course`
        * `attached_files`

        **filters:**
            * `?id=<int>`
            * `?course=<int>`
    """
    model = Note
    serializer_class = NoteSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, IsCreatorOrTeacherNotePermission, )
    filter_fields = ('id', 'course')


class MessageViewSet(ModelViewSet):
    """
    **Message View Set**

        Return list of messages for all users(only active messages).
        Create new message(only if user is authenticated course member or course teacher or course creator).
        Delete message(only is user is message author).

        **returns:**

        * `id`
        * `created`
        * `modified`
        * `text`
        * `course`
        * `user`
        * `is_active`

        **filters:**
            * `?id=<int>`
            * `?user=<int>`
            * `?is_active=<[True|False]>`
    """
    queryset = Message.objects.filter(is_active=True)
    serializer_class = MessageSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, MessagePermission)
    filter_fields = ('id', 'author', 'is_active', 'course')
