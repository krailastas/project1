# encoding: utf-8
from __future__ import unicode_literals

from rest_framework.routers import SimpleRouter

from course.views import (CurrencyViewSet, AttachedFilesViewSet, ReviewViewSet,
                          CourseViewSet, ClassViewSet, NoteViewSet,
                          MessageViewSet, TeacherInTeacherListViewSet, TeacherInvitationViewSet,
                          FollowedTeacherViewSet, CourseInWishlistViewSet, CourseAttachedFileViewSet)


router = SimpleRouter(trailing_slash=False)

router.register('course_attache_files', CourseAttachedFileViewSet, base_name='course_attache_files')
router.register('wishlists', CourseInWishlistViewSet, base_name='wishlists')
router.register('currencies', CurrencyViewSet, base_name='currencies')
router.register('attached_files', AttachedFilesViewSet, base_name='attached_files')
router.register('courses', CourseViewSet, base_name='courses')
router.register('classes', ClassViewSet, base_name='classes')
router.register('notes', NoteViewSet, base_name='notes')
router.register('messages', MessageViewSet, base_name='messages')
router.register('teacher_in_teacher_list', TeacherInTeacherListViewSet, base_name='teacher_in_teacher_list')
router.register('teacher_invitation', TeacherInvitationViewSet, base_name='teacher_invitation')
router.register('followed_teacher', FollowedTeacherViewSet, base_name='followed_teacher')
router.register('reviews', ReviewViewSet, base_name='reviews')

urlpatterns = router.urls
