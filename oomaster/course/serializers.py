# encoding: utf-8
from __future__ import unicode_literals

from datetime import datetime

from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist

from rest_framework import serializers

from course.mixins import CourseErrorMesageMixin
from common.utils import humansize_file_size
from course.models import (Currency, AttachedFiles, Review,
                           Course, Class, Note, Message,
                           TeacherInTeacherList, TeacherInvitation,
                           FollowedTeacher, CourseInWishlist,
                           CourseAttachedFile)


class CourseAttachedFileSerializer(serializers.ModelSerializer):
    file_size = serializers.SerializerMethodField('get_file_size')

    class Meta:
        model = CourseAttachedFile

    def get_file_size(self, obj):
        if obj.attached_file:
            return humansize_file_size(obj.attached_file.file.file.size)
        return None

    def validate(self, attrs):
        request_user = self.context['request'].user
        course = attrs.get('course')

        if request_user != course.teacher:
            raise serializers.ValidationError(_("you can create attached files only if you is course's teacher"))

        return attrs


class CourseInWishlistSerializer(serializers.ModelSerializer):
    """
    **CourseInWishlist Serializer**
        Serializer for CourseInWishlist model.
        User can add course to own wishlist if he doesn't course member.

        **fields:**

        * `user`
        * `course`
    """

    class Meta:
        model = CourseInWishlist

    def validate(self, attrs):
        request_user = self.context['request'].user
        user = attrs.get('user')
        course = attrs.get('course')

        if user != request_user:
            raise serializers.ValidationError(_('you can add course only to own wishlist'))

        if course in request_user.students.all():
            raise serializers.ValidationError(_('you already paid for this course'))

        if course.date_begin < datetime.today().date():
            raise serializers.ValidationError(_("you can't add past course to your wishlist"))

        return attrs


class ReviewSerializer(serializers.ModelSerializer):
    """
    **Review Serializer**
        Serializer for Review model.
        Then course was ended course member can create one review for this course,
        if student didn't left all course's classes.

        **fields:**

        * `author`
        * `course`
        * `message`
        * `rating`
    """

    class Meta:
        model = Review

    def validate(self, attrs):
        request_user = self.context['request'].user

        author = attrs.get('author')
        course = attrs.get('course')

        if request_user not in course.students.all():
            raise serializers.ValidationError(_("you doesn't member of this course"))

        if course.date_end > datetime.now().date():
            raise serializers.ValidationError(_("you can't create review, this course is not ended"))

        if author != request_user:
            raise serializers.ValidationError(_('you can create only own review'))

        if request_user.left_classes.filter(course=course).count() == course.amount_classes:
            raise serializers.ValidationError(_("you left all classes of this course, you can't create review"))

        return attrs


class FollowedTeacherSerializer(serializers.ModelSerializer):
    """
    **FollowedTeacher Serializer**
        Serializer for FollowedTeacher model.

        **fields:**

        * `user`
        * `teacher`
    """

    class Meta:
        model = FollowedTeacher

    def validate(self, attrs):
        method = self.context['request'].method
        user = attrs.get('user')
        teacher = attrs.get('teacher')
        request_user = self.context['request'].user

        if method in ('POST', 'PUT'):
            if user != request_user:
                raise serializers.ValidationError(_('you can only follow other user'))

        if teacher.profile.is_teacher is False:
            raise serializers.ValidationError(_("followed user isn't teacher"))

        return attrs


class TeacherInTeacherListSerializer(serializers.ModelSerializer):
    """
    **TeacherInTeacherList Serializer**
        Serializer for TeacherInTeacherList model.

        **fields:**

        * `list_owner`
        * `teacher_in_list`
    """

    class Meta:
        model = TeacherInTeacherList


class TeacherInvitationSerializer(serializers.ModelSerializer):
    """
    **TeacherInvitation Serializer**
        Serializer for TeacherInvitation model.
        It is like friendship invitation in Facebook.
        If user accept our invitation to your list,
        automaticaly will be created new record in TeacherInTeacherList.

        **list fields:**

        * `from_user` - ForeignKey - profiles.User
        * `to_user` - ForeignKey - profiles.User
        * `status` - CharField - ('Accepted', 'Declined', 'Sent')
    """

    class Meta:
        model = TeacherInvitation

    def validate(self, attrs):
        method = self.context['request'].method
        from_user = attrs.get('from_user')
        status = attrs.get('status')
        user = self.context['request'].user
        to_user = attrs.get('to_user')

        if method == 'POST':
            if not (user == from_user and status == TeacherInvitation.INVITATION_STATUSES.Sent and from_user.profile.is_teacher and to_user.profile.is_teacher):
                raise serializers.ValidationError(_("you can only create invitation with status 'sent' to other user"))

        if method == 'PUT':
            if self.object.to_user != to_user or self.object.from_user != from_user:
                raise serializers.ValidationError(_('you can change only status'))

        return attrs


class CurrencySerializer(serializers.ModelSerializer):
    """
    **Currency Serializer**
        Serializer for Currency model.

        **fields:**

        * `title`
        * `created`
        * `modified`
    """

    class Meta:
        model = Currency


class AttachedFilesSerializer(serializers.ModelSerializer):
    """
    **AttachedFiles Serializer**
        Serializer for AttachedFiles model.

        **fields:**

        * `file`
        * `created`
        * `modified`
    """

    class Meta:
        model = AttachedFiles


class CourseSerializer(CourseErrorMesageMixin, serializers.ModelSerializer):
    """
    **Course Serializer**
        Serializer for Course model. Only teacher can be teacher or creator of course. Course must have at least two classes.
        Date_begin and date_end must be in the future. Duration of the course must be at least one day and less than one year.
        Class duration can't be more than 24 hours. Min sits count must be less than max sits count.
        Price must be a positive number.

        **fields:**

        * `creator`
        * `teacher`
        * `title`
        * `detail`
        * `date_begin`
        * `date_end`
        * `amount_classes`
        * `city`
        * `country`
        * `matterials_for_learning`
        * `min_sits_count`
        * `max_sits_count`
        * `currency`
        * `price`
        * `location`
        * `students`
        * `time_begin`
        * `duration`
        * `image`
        * `category`
        * `created`
        * `modified`
        * `student_to_begin_count` - serializers method
        * `student_to_full_count` - serializers method
        * `left_classes` - serializers method
        * `status` - status by date - past, upcoming, ongoing
        * `is_my_course` - returns True if request user is a course's teacher
    """
    student_to_begin_count = serializers.SerializerMethodField('get_student_to_begin_count')
    student_to_full_count = serializers.SerializerMethodField('get_student_to_full_count')
    left_classes = serializers.SerializerMethodField('get_left_classes')
    next_class = serializers.SerializerMethodField('get_next_class')
    status = serializers.SerializerMethodField('get_status')
    is_my_course = serializers.SerializerMethodField('check_is_my_course')

    class Meta:
        model = Course

    def get_status(self, obj):
        if obj.date_end < datetime.today().date():
            return 'past'
        if obj.date_begin > datetime.today().date():
            return 'upcoming'
        return 'ongoing'

    def check_is_my_course(self, obj):
        if self.context['request'].user.is_authenticated():
            return obj.teacher == self.context['request'].user
        return None

    def get_left_classes(self, obj):
        return obj.classes.filter(date__gte=datetime.today().date()).count()

    def get_next_class(self, obj):
        next_class = obj.classes.filter(date__gte=datetime.today().date()).order_by('date').first()
        return next_class.number if next_class else None

    def get_student_to_begin_count(self, obj):
        if obj.students.count() < obj.min_sits_count:
            return obj.min_sits_count - obj.students.count()
        return None

    def get_student_to_full_count(self, obj):
        if obj.students.count() > obj.min_sits_count:
            return obj.max_sits_count - obj.students.count()
        return None

    def validate_creator(self, attrs, source):
        creator = attrs[source]
        user = self.context['request'].user
        if creator != user:
            raise serializers.ValidationError(_(self.ERR_MSG['creator_only_current_user']))
        if not creator.profile.is_teacher:
            raise serializers.ValidationError(_(self.ERR_MSG['must_be_teacher']))
        return attrs

    def validate_teacher(self, attrs, source):
        teacher = attrs[source]
        if not teacher.profile.is_teacher:
            raise serializers.ValidationError(_(self.ERR_MSG['must_be_teacher']))
        return attrs

    def validate_students(self, attrs, source):
        students = attrs[source]
        max_sits_count = attrs.get('max_sits_count')
        if len(students) > max_sits_count:
            raise serializers.ValidationError(_(self.ERR_MSG['students_count_more_than_max_sits_count']))
        return attrs

    def validate_amount_classes(self, attrs, source):
        amount_classes = attrs[source]
        if not (1 <= amount_classes <= 99):
            raise serializers.ValidationError(_(self.ERR_MSG['course_class_count_error']))
        return attrs

    def validate_date_begin(self, attrs, source):
        method = self.context['request'].method
        if method == 'PUT':
            return attrs
        date_begin = attrs[source]
        if date_begin < datetime.today().date():
            raise serializers.ValidationError(_(self.ERR_MSG['must_be_in_future']))
        return attrs

    def validate_date_end(self, attrs, source):
        date_end = attrs[source]
        date_begin = attrs.get('date_begin')
        if date_end < datetime.today().date():
            raise serializers.ValidationError(_(self.ERR_MSG['must_be_in_future']))

        if not (1 <= (date_end - date_begin).days <= 365):
            raise serializers.ValidationError(_(self.ERR_MSG['course_duration_error']))

        return attrs

    def validate_duration(self, attrs, source):
        duration = attrs[source]
        if duration > 1440:
            raise serializers.ValidationError(_(self.ERR_MSG['class_duration_error']))
        return attrs

    def validate(self, attrs):
        date_begin = attrs.get('date_begin')
        date_end = attrs.get('date_end')

        amount_classes = attrs.get('amount_classes')
        duration = (date_end - date_begin).days
        if duration < amount_classes:
            raise serializers.ValidationError(_(self.ERR_MSG['amount_classes_error']))

        min_sits_count = attrs.get('min_sits_count')
        max_sits_count = attrs.get('max_sits_count')
        if max_sits_count < min_sits_count:
            raise serializers.ValidationError(_(self.ERR_MSG['min_sits_count_error']))

        return attrs


class ClassSerializer(serializers.ModelSerializer):
    """
    **Class Serializer**
        Serializer for Class model. Course and number can't be modified.
        Date must be after previous class and before next class.
        Class duration cant be more than 24 hours.

        **fields:**

        * `course`
        * `number`
        * `date`
        * `price`
        * `location`
        * `left_students`
        * `time_begin`
        * `duration`
        * `created`
        * `modified`
    """
    course = serializers.Field(source='course_id')
    number = serializers.Field(source='number')

    class Meta:
        model = Class

    def validate_date(self, attrs, sourse):
        new_date = attrs[sourse]

        if not (self.object.course.date_begin <= new_date <= self.object.course.date_end):
            raise serializers.ValidationError(_("date must be after course's date begin and before date end"))

        if self.object.number > 1:
            try:
                previos_class = Class.objects.get(course=self.object.course, number=self.object.number - 1)
            except ObjectDoesNotExist:
                pass
            else:
                if new_date < previos_class.date:
                    raise serializers.ValidationError(_('this class must be after previous class'))

        if self.object.number != self.object.course.classes.count():
            next_class = Class.objects.get(course=self.object.course, number=self.object.number + 1)
            if new_date > next_class.date:
                raise serializers.ValidationError(_('this class must be before next class'))

        return attrs

    def validate_duration(self, attrs, sourse):
        duration = attrs[sourse]
        if duration > 1440:
            raise serializers.ValidationError(_('class duration cant be more than 24 hours'))
        return attrs

    def validate_teacher(self, attrs, sourse):
        user = self.context['request'].user
        old_teacher = self.object.teacher
        new_teacher = attrs[sourse]

        # if new teacher
        if new_teacher != old_teacher:
            if user != self.object.course.creator:
                raise serializers.ValidationError(_('only creator of class can change teacher'))

        teacher = attrs[sourse]
        if teacher.profile.is_teacher is False:
            raise serializers.ValidationError(_('teacher must have profile.is_teacher=True'))
        return attrs

    def validate_left_students(self, attrs, sourse):
        """
        student can change only own record
        """
        left_students_old = list(self.object.left_students.all())
        left_students_new = attrs[sourse]

        if left_students_old != left_students_new:
            from math import fabs

            if fabs(len(left_students_old) - len(left_students_new)) != 1:
                raise serializers.ValidationError(_('student can change only own record'))

            left_sudent_difference = set(left_students_old) ^ set(left_students_new)
            left_student_new_id = left_sudent_difference.pop().id
            if left_student_new_id != self.context['request'].user.id:
                raise serializers.ValidationError(_('student can change only own record'))

        return attrs

    def validate(self, attrs):
        """
        Can not edit the past class.
        If student try change not only
        'left_students' field raise ValidationError
        """
        date = attrs.get('date')
        if date < datetime.today().date():
            raise serializers.ValidationError(_("can not edit the past classes"))

        user = self.context['request'].user
        if user in self.object.course.students.all() and user != self.object.course.creator:

            from django.forms.models import model_to_dict

            old_obj_dict = model_to_dict(self.Meta.model.objects.get(id=self.object.id))

            for key, value in attrs.iteritems():
                if key in old_obj_dict.keys():
                    if key not in ('teacher', 'left_students'):
                        if value != old_obj_dict[key]:
                            raise serializers.ValidationError(_("student can change only 'left_students' field"))
        return attrs


class NoteSerializer(serializers.ModelSerializer):
    """
    **Note Serializer**
        Serializer for Note model. Only course's creator or teacher can create create/edit/delete note.

        **fields:**

        * `text`
        * `course`
        * `user`
        * `created`
        * `modified`
        * `attached_files`
    """

    class Meta:
        model = Note

    def validate(self, attrs):
        course = attrs.get('course')
        user = self.context['request'].user
        if user not in (course.teacher, course.creator):
            raise serializers.ValidationError(_("Only course's creator or teacher can create create/edit/delete note."))
        return attrs


class MessageSerializer(serializers.ModelSerializer):
    """
    **Message Serializer**
        Serializer for Message model. Only course member can create/edit/delete message.

        **fields:**

        * `text`
        * `course`
        * `author`
        * `created`
        * `modified`
        * `is_active`
    """

    class Meta:
        model = Message

    def validate(self, attrs):
        author = attrs.get('author')
        if self.context['request'].user != author:
            raise serializers.ValidationError('Only request.user can be message.author')

        course = attrs.get('course')
        if author not in course.students.all() and author != course.teacher:
            raise serializers.ValidationError('Only course member can create or delete message.')
        return attrs
