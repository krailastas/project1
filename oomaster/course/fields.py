from decimal import Decimal

from django.db import models
from django.core import exceptions
from django.utils.translation import ugettext_lazy as _


class PriceField(models.DecimalField):
    __metaclass__ = models.SubfieldBase

    default_error_messages = {
        'price_less_than_zero': _("'%(value)s' value must be a positive number."),
    }

    def to_python(self, value):
        if value < Decimal(0):
            raise exceptions.ValidationError(
                self.error_messages['price_less_than_zero'],
                code='price_less_than_zero',
                params={'value': value},
            )
        try:
            return super(PriceField, self).to_python(value).quantize(Decimal("0.01"))
        except AttributeError:
            return None
