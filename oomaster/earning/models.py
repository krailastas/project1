# encoding: utf-8
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.db.models.signals import post_save
from django.utils import timezone
from model_utils.models import TimeStampedModel
from constance import config

from profiles.models import User
from course.fields import PriceField
from course.models import Course


STATUS_CHOICES = (
    (0, _('In progress')),
    (1, _('Submitted')),
    (2, _('Canceled')),
    (3, _('Finished')),
)


class Balance(models.Model):
    """
    **Balance model**

        **list fields:**

        * `user` - OneToOneField - User
        * `total_balance` - PriceField
        * `total_allowed_withdrawal` - PriceField
    """
    user = models.OneToOneField(User, verbose_name=_('user'), related_name='balance')
    total_balance = PriceField(verbose_name=_('total_balance'), max_digits=10, decimal_places=2, default=0)
    total_allowed_withdrawal = PriceField(verbose_name=_('total_allowed_withdrawal'), max_digits=10, decimal_places=2,
                                          default=0)

    class Meta:
        verbose_name = _("Balance")
        verbose_name_plural = _("Balance")


class ItemBalance(TimeStampedModel):
    """
    **ItemBalance model**

        **list fields:**

        * `balance` - ForeignKey - Balance
        * `course` - ForeignKey - Course
        * `earnings` - PriceField
        * `allowed_withdrawal` - PriceField
        * `withdrawal` - PriceField
        * `is_active` - BooleanField
    """
    balance = models.ForeignKey(Balance, verbose_name=_('balance'), related_name='b_items')
    course = models.ForeignKey(Course, verbose_name=_('course'), related_name='c_items')
    earnings = PriceField(verbose_name=_('earnings'), max_digits=10, decimal_places=2, default=0)
    allowed_withdrawal = PriceField(verbose_name=_('allowed withdrawal'), max_digits=10, decimal_places=2, default=0)
    withdrawal = PriceField(verbose_name=_('withdrawal'), max_digits=10, decimal_places=2, default=0)
    is_active = models.BooleanField(verbose_name=_('active'), default=False)

    class Meta:
        verbose_name = _("ItemBalance")
        verbose_name_plural = _("ItemBalances")

    def save(self, *args, **kwargs):
        self.course.date_end
        if ItemBalance.objects.filter(pk=self.pk, course__date_end__gte=timezone.now()).exists():
            self.allowed_withdrawal = 0
        else:
            self.is_active = True
            if self.earnings >= self.withdrawal:
                self.allowed_withdrawal = self.earnings - self.withdrawal
            else:
                self.allowed_withdrawal = 0
        super(ItemBalance, self).save(*args, **kwargs)


class Withdrawal(TimeStampedModel):
    """
    **Withdrawal model**

        **list fields:**

        * `item_balance` - ForeignKey - ItemBalance
        * `amount` - PriceField
        * `service_charge` - PriceField
        * `status` - SmallIntegerField
    """
    item_balance = models.ForeignKey(ItemBalance, verbose_name=_('item balance'), related_name='withdrawns')
    amount = PriceField(verbose_name=_('amount'), max_digits=10, decimal_places=2, default=0)
    service_charge = PriceField(verbose_name=_('service_charge'), max_digits=10, decimal_places=2, default=0)
    status = models.SmallIntegerField(_('status'), choices=STATUS_CHOICES, default=0)

    class Meta:
        verbose_name = _("Withdrawal")
        verbose_name_plural = _("Withdrawals")

    def save(self, *args, **kwargs):
        self.service_charge = self.amount * int(config.service_charge) / 100
        super(Withdrawal, self).save(*args, **kwargs)


def update_balanse_and_allowed_withdrawal(sender, instance, created, **kwargs):
    obj = Balance.objects.filter(user=instance.balance.user)[0]
    total_balance = 0
    total_allowed_withdrawal = 0
    for item in obj.b_items.all():
        total_balance += item.earnings
        total_allowed_withdrawal += item.allowed_withdrawal
    obj.total_balance = total_balance
    obj.total_allowed_withdrawal = total_allowed_withdrawal
    obj.save()


post_save.connect(update_balanse_and_allowed_withdrawal, sender=ItemBalance)


def allowed_withdrawal(sender, instance, created, **kwargs):
    obj = ItemBalance.objects.filter(pk=instance.item_balance.pk)[0]
    withdrawal = 0
    for w in obj.withdrawns.all():
        withdrawal += w.amount
    obj.withdrawal = withdrawal
    if obj.earnings >= obj.withdrawal:
        obj.allowed_withdrawal = obj.earnings - obj.withdrawal
    else:
        obj.allowed_withdrawal = obj.earnings
    obj.save()


post_save.connect(allowed_withdrawal, sender=Withdrawal)