# encoding: utf-8
from __future__ import unicode_literals

from django.db.models.aggregates import Sum
from django.http.response import Http404
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.permissions import IsAuthenticated

from earning.models import Balance, Withdrawal, ItemBalance
from earning.serializers import BalanceSerializer, WithdrawalSerializer, \
    CourseBalanceSerializer, MonthBalanceSerializer
from earning.permissions import IsBalancePermission, IsWithdrawalPermission


class BalanceView(ReadOnlyModelViewSet):
    """
    **Balance View**

        Return list of balance.

        **returns:**

        * `id`
        * `user`
        * `total_balance`
        * `total_allowed_withdrawal`
    """
    queryset = Balance.objects.all()
    serializer_class = BalanceSerializer
    permission_classes = (IsBalancePermission,)

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)


class WithdrawalView(ReadOnlyModelViewSet):
    """
    **Withdrawal View**

        Return list of withdrawal.

        **returns:**
        
        * `id`
        * `amount`
        * `service_charge`
        * `status`
    """
    queryset = Withdrawal.objects.all().order_by('-created')
    serializer_class = WithdrawalSerializer
    permission_classes = (IsWithdrawalPermission,)

    def get_queryset(self):
        return self.queryset.filter(item_balance__balance__user=self.request.user)


class MonthBalanceView(ReadOnlyModelViewSet):
    """
    **MonthBalance View**

        **returns:**
        
        * `created`
        * `earnings`
        * `course`
        
        **order_by:**
        * `?ordering=created`
        * `?ordering=earnings`
        * `?ordering=course`
    """
    serializer_class = MonthBalanceSerializer
    permission_classes = (IsAuthenticated,)
    paginate_by = 9

    def get_queryset(self):
        y = m = earnings = 0
        i = 1
        request = []
        course = []
        month = None
        order_by = self.request.GET.get('ordering', '-created')
        item_balance = ItemBalance.objects.filter(balance__user=self.request.user).order_by(order_by)
        for item in item_balance:
            if i == 1:
                earnings += item.earnings
                course.append(item.course_id)
                if i == item_balance.count():
                    request.append({'created': item.created, 'earnings': earnings, 'course': 1})
            else:
                if y == item.created.year and m == item.created.month:
                    earnings += item.earnings
                    if not item.course_id in course:
                        course.append(item.course_id)
                else:
                    request.append({'created': month, 'earnings': earnings, 'course': len(course)})
                    earnings = item.earnings
                    course = []
                    course.append(item.course_id)
                if i == item_balance.count():
                    request.append({'created': item.created, 'earnings': earnings, 'course': len(course)})
            m = item.created.month
            y = item.created.year
            month = item.created
            i += 1
        sort = self.request.GET.get('ordering', None)
        if sort:
            try:
                if sort[0] == '-':
                    sort = sort[1:]
                    list = sorted(request, key=lambda x: x[sort], reverse=True)
                else:
                    list = sorted(request, key=lambda x: x[sort])
                return list
            except:
                raise Http404
        return request


class CourseBalanceView(ReadOnlyModelViewSet):
    """
    **CourseBalance View**

        **returns:**
        
        * `id_course`
        * `course__title`
        * `earnings`
        * `allowed_withdrawal`
        * `withdrawal`
        
        **order_by:**
        * `?ordering=course__title`
        * `?ordering=earnings`
        * `?ordering=allowed_withdrawal`
        * `?ordering=withdrawal`
    """
    serializer_class = CourseBalanceSerializer
    permission_classes = (IsAuthenticated,)
    paginate_by = 9
    queryset = ItemBalance.objects.select_related('course')

    def get_queryset(self):
        request = []
        item_balance = self.queryset.filter(balance__user=self.request.user)
        for item_id in set(item_balance.values_list('course_id', flat=True)):
            it = item_balance.filter(course_id=item_id)
            sum = it.aggregate(Sum('earnings'), Sum('allowed_withdrawal'), Sum('withdrawal'))
            request.append({'id_course': item_id,
                            'course__title': it[0].course.title,
                            'earnings': sum['earnings__sum'],
                            'allowed_withdrawal': sum['allowed_withdrawal__sum'],
                            'withdrawal': sum['withdrawal__sum']})
        sort = self.request.GET.get('ordering', None)
        if sort:
            try:
                if sort[0] == '-':
                    sort = sort[1:]
                    list = sorted(request, key=lambda x: x[sort], reverse=True)
                else:
                    list = sorted(request, key=lambda x: x[sort])
                return list
            except:
                raise Http404
        return request