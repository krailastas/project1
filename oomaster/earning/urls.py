# encoding: utf-8
from __future__ import unicode_literals

from rest_framework.routers import SimpleRouter

from earning.views import BalanceView, WithdrawalView, \
    MonthBalanceView, CourseBalanceView


router = SimpleRouter(trailing_slash=False)
router.register('balances', BalanceView, base_name='balances')
router.register('course-earning', CourseBalanceView, base_name='course_earning')
router.register('withdrawals', WithdrawalView, base_name='withdrawals')
router.register('month-earning', MonthBalanceView, base_name='month')
urlpatterns = router.urls