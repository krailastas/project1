from django.contrib import admin

from earning.models import Balance, ItemBalance, Withdrawal


# Register your models here.
admin.site.register(Balance)
admin.site.register(ItemBalance)
admin.site.register(Withdrawal)