# encoding: utf-8
from __future__ import unicode_literals

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from profiles.factories import UserFactory
from earning.factories import BalanceFactory, WithdrawalFactory, \
    ItemBalanceFactory
from earning.models import Balance


class TestBalanceView(APITestCase):
    url = reverse('balances-list')

    def setUp(self):
        super(TestBalanceView, self).setUp()
        BalanceFactory.create_batch(5)
        self.user = UserFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestWithdrawalView(APITestCase):
    url = reverse('withdrawals-list')

    def setUp(self):
        super(TestWithdrawalView, self).setUp()
        WithdrawalFactory.create_batch(5)
        self.user = UserFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestMonthBalanceView(APITestCase):
    url = reverse('month-list')

    def setUp(self):
        super(TestMonthBalanceView, self).setUp()
        self.user = UserFactory.create()
        balance = Balance.objects.create(user=self.user)
        ItemBalanceFactory.create(balance=balance)

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)


class TestCourseBalanceView(APITestCase):
    url = reverse('course_earning-list')

    def setUp(self):
        super(TestCourseBalanceView, self).setUp()
        self.user = UserFactory.create()
        balance = Balance.objects.create(user=self.user)
        ItemBalanceFactory.create(balance=balance)

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)