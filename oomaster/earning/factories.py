# encoding: utf-8
from __future__ import unicode_literals

import random
from decimal import Decimal

import factory

from profiles.factories import UserFactory
from earning.models import Balance, ItemBalance, Withdrawal
from course.models import Course
from course.factories import CourseFactory


class BalanceFactory(factory.DjangoModelFactory):
    class Meta:
        model = Balance

    @factory.lazy_attribute
    def user(self):
        return UserFactory.create()

    @factory.lazy_attribute
    def total_balance(self):
        return int(Decimal(random.choice(range(999))))

    @factory.lazy_attribute
    def total_allowed_withdrawal(self):
        return int(Decimal(random.choice(range(999))))


class ItemBalanceFactory(factory.DjangoModelFactory):
    class Meta:
        model = ItemBalance

    @factory.lazy_attribute
    def balance(self):
        if not Balance.objects.all().exists():
            BalanceFactory.create_batch(5)
        return Balance.objects.all().order_by('?')[0]

    @factory.lazy_attribute
    def allowed_withdrawal(self):
        return float(Decimal(random.choice(range(999))))

    @factory.lazy_attribute
    def withdrawal(self):
        return float(Decimal(random.choice(range(999))))

    @factory.lazy_attribute
    def course(self):
        if not Course.objects.all().exists():
            CourseFactory.create_batch(5)
        return Course.objects.all().order_by('?')[0]

    @factory.lazy_attribute
    def earnings(self):
        return float(Decimal(random.choice(range(999))))


class WithdrawalFactory(factory.DjangoModelFactory):
    class Meta:
        model = Withdrawal

    @factory.lazy_attribute
    def item_balance(self):
        if not ItemBalance.objects.all().exists():
            ItemBalanceFactory.create_batch(5)
        return ItemBalance.objects.all().order_by('?')[0]

    @factory.lazy_attribute
    def amount(self):
        return float(Decimal(random.choice(range(10))))