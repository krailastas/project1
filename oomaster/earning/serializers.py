# encoding: utf-8
from __future__ import unicode_literals

from rest_framework import serializers

from earning.models import Balance, Withdrawal


class BalanceSerializer(serializers.ModelSerializer):
    """
    **Balance Serializer**
        Serializer for Balance model.

        **fields:**
        
        * `user`
        * `total_balance`
        * `total_allowed_withdrawal`
    """

    class Meta:
        model = Balance


class CourseBalanceSerializer(serializers.Serializer):
    """
    **CourseBalance Serializer**

        **fields:**
        
        * `id_course`
        * `course__title`
        * `earnings`
        * `allowed_withdrawal`
        * `withdrawal`
    """
    id_course = serializers.IntegerField()
    course__title = serializers.CharField()
    earnings = serializers.DecimalField()
    allowed_withdrawal = serializers.DecimalField()
    withdrawal = serializers.DecimalField()


class MonthBalanceSerializer(serializers.Serializer):
    """
    **MonthBalance Serializer**

        **fields:**
        
        * `created`
        * `earnings`
        * `course`
    """
    created = serializers.DateTimeField()
    earnings = serializers.DecimalField()
    course = serializers.IntegerField()


class WithdrawalSerializer(serializers.ModelSerializer):
    """
    **Withdrawal Serializer**
        Serializer for Withdrawal model.

        **fields:**
        
        * `id`
        * `amount`
        * `service_charge`
        * `status`
        * `created`
        * `modified`
    """

    class Meta:
        model = Withdrawal
        exclude = ('item_balance',)