from rest_framework.serializers import HyperlinkedModelSerializer
from rest_framework import relations
from cities_light.models import Country, City


class CitySerializer(HyperlinkedModelSerializer):
    """
    **City serializer.**
        City serializer.

        **fields:**

        * `url`
        * `id`
        * `name`
        * `display_name`
        * `country`
    """
    url = relations.HyperlinkedIdentityField(view_name='cities-light-api-region-detail')
    country = relations.HyperlinkedRelatedField(view_name='cities-light-api-country-detail')

    class Meta:
        model = City
        fields = ('url', 'id', 'name', 'display_name', 'country')


class CountrySerializer(HyperlinkedModelSerializer):
    """
    **Country serializer.**
        Country serializer.

        **fields:**

        * `url`
        * `id`
        * `name`
    """
    url = relations.HyperlinkedIdentityField(view_name='cities-light-api-country-detail')

    class Meta:
        model = Country
        fields = ('url', 'id', 'name')
