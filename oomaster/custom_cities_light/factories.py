# encoding: utf-8
from __future__ import unicode_literals

import random

import factory

from cities_light.models import Country, City


class CountryFactory(factory.DjangoModelFactory):
    class Meta:
        model = Country

    @factory.lazy_attribute
    def name(self):
        return 'country-{}'.format(random.choice(range(1000000, 9999999)))


class CityFactory(factory.DjangoModelFactory):
    class Meta:
        model = City

    @factory.lazy_attribute
    def name(self):
        return 'country-{}'.format(random.choice(range(1000000, 9999999)))

    country = CountryFactory.create()
