from rest_framework import viewsets

from cities_light.models import Country, City

from .serializers import CountrySerializer, CitySerializer


class CountryModelViewSet(viewsets.ReadOnlyModelViewSet):
    """
    **Country View**
        This view returns a list of countries or country detail by country_id. Read only view.
    """
    serializer_class = CountrySerializer
    queryset = Country.objects.all()


class CityModelViewSet(viewsets.ReadOnlyModelViewSet):
    """
    **City View**
        This view returns a list of cities or country detail by country_id. Read only view.

        **Supported next filters:**

        * `?country_id=<id>` - returns a list of cities wich have country with id equal country_id.
    """
    serializer_class = CitySerializer
    queryset = City.objects.all()

    def get_queryset(self):
        queryset = super(CityModelViewSet, self).get_queryset()
        if self.request.GET.get('country_id', None):
            return queryset.filter(country__id=self.request.GET['country_id'])
        return queryset
