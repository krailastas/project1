# -*- encoding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from cities_light.models import Country

from custom_cities_light.factories import CityFactory, CountryFactory


class CityTestCase(APITestCase):
    url = reverse('city-list')

    def setUp(self):
        super(CityTestCase, self).setUp()
        country_1, country_2 = CountryFactory.create_batch(2)
        CityFactory.create_batch(3, country=country_1)
        CityFactory.create_batch(3, country=country_2)

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 6)

        country_id = Country.objects.first().id
        response = self.client.get(self.url + '?country_id=' + str(country_id))
        self.assertEqual(len(response.data['results']), 3)
