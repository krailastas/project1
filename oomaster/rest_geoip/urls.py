# encoding: utf-8
from __future__ import unicode_literals

from rest_framework.routers import SimpleRouter

from rest_geoip.views import IpRangeView


router = SimpleRouter(trailing_slash=False)
router.register('location', IpRangeView, base_name='location')
urlpatterns = router.urls