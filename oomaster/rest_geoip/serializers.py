# encoding: utf-8
from __future__ import unicode_literals

from rest_framework import serializers


class IpRangeSerializer(serializers.Serializer):
    """
    **IpRange Serializer**
        Serializer for IpRange model.

        **fields:**
        
        * `country`
        * `region`
        * `city`
    """
    country = serializers.CharField()
    region = serializers.CharField()
    city = serializers.CharField()