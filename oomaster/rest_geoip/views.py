# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.permissions import IsAuthenticated

from rest_geoip.serializers import IpRangeSerializer
from rest_geoip.models import IpRange


class IpRangeView(ReadOnlyModelViewSet):
    """
    **IpRange View**

        Return list of IP Range.

        **returns:**
        
        * `country`
        * `region`
        * `city`
    """
    serializer_class = IpRangeSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        try:
            real_ip = self.request.META['HTTP_X_FORWARDED_FOR']
            real_ip = real_ip.split(',')[0]
        except KeyError:
            real_ip = self.request.META['REMOTE_ADDR']
        try:
            l = IpRange.objects.by_ip(real_ip)
            return [{'country': l.country, 'region': l.region.pk, 'city': l.city.pk}]
        except:
            return [{'country': None, 'region': None, 'city': None}]



