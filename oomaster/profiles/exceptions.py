# encoding: utf-8
from __future__ import unicode_literals


class BaseAccountException(Exception):
    def __init__(self, msg=None):
        self.msg = msg

    def __str__(self):
        return self.msg


class IncorrectProviderException(BaseAccountException):
    def __str__(self):
        return 'Incorrect provider\'s name'


class MissingParameterException(BaseAccountException):
    def __init__(self, arg):
        self.arg = arg

    def __str__(self):
        return 'Missing needed parameter: {0}'.format(self.arg)


class NotAllowedHttpVerbException(BaseAccountException):
    def __str__(self):
        return 'Not allowed http verb'
