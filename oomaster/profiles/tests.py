# encoding: utf-8

from __future__ import unicode_literals

import random
from datetime import datetime, timedelta

from django.core import mail
from django.conf import settings
from django.contrib.auth.tokens import default_token_generator
from django.utils.http import urlsafe_base64_encode

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from profiles.factories import UserFactory, UserStaffFactory,\
    TeacherInformationFactory, StudySubjectFactory, CertificateFactory,\
    TeachingSubjectFactory, PortfolioFactory, TeachingExperianceFactory,\
    WorkExperianceFactory, EducationFactory
from profiles.models import Profile, User, TeacherInformation, Certificate,\
    Portfolio, TeachingSubject, TeachingExperiance, WorkExperiance, Education

from profiles.mixins import ProfileErrorMesageMixin


ERR_MSG = ProfileErrorMesageMixin.ERR_MSG


class TestUserViewSet(APITestCase):
    url = reverse('users-list')

    def setUp(self):
        super(TestUserViewSet, self).setUp()
        UserFactory.create_batch(5)
        self.user = UserFactory.create()
        self.user_staff = UserStaffFactory.create()

    def test_get(self):
        self.user.profile.is_teacher = True
        self.user.profile.save()

        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertContains(response, self.user.id)
        self.assertEqual(response.data['count'], 1)

        staff_count = 0
        not_staff = 0
        for us in User.objects.all():
            if us.is_staff:
                staff_count += 1
            else:
                not_staff += 1
        self.assertEqual(staff_count, 1)
        self.assertEqual(not_staff, 6)

    def test_post(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.client.force_authenticate(self.user)
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class TestProfileViewSet(APITestCase):
    url = reverse('profile-list')

    def setUp(self):
        super(TestProfileViewSet, self).setUp()
        UserFactory.create_batch(5)
        self.user = UserFactory.create()
        self.user_staff = UserStaffFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, self.user.id)
        self.assertEqual(len(response.data['results']), 7)

    def test_post(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_delete(self):
        up = Profile.objects.all().last()
        pk = up.pk
        url = reverse('profile-detail', kwargs={'pk': pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_put(self):
        user_profile_pk = Profile.objects.filter(user=self.user_staff).first().pk
        url = reverse('profile-detail', kwargs={'pk': user_profile_pk})
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user_staff)
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = {
            'bio': 'bio',
        }
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestTeacherInformationViewSet(APITestCase):
    url = reverse('teacher_information-list')

    def setUp(self):
        super(TestTeacherInformationViewSet, self).setUp()
        TeacherInformationFactory.create_batch(5)
        self.user = UserFactory.create()
        self.teacher_information = TeacherInformationFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, self.teacher_information.pk)
        self.assertEqual(len(response.data['results']), 6)

    def test_post(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_delete(self):
        ti = TeacherInformation.objects.all().last()
        pk = ti.pk
        url = reverse('teacher_information-detail', kwargs={'pk': pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_put(self):
        ti = TeacherInformation.objects.all().last()
        pk = ti.pk
        url = reverse('teacher_information-detail', kwargs={'pk': pk})
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(ti.user)
        data = {}
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestStudySubjectViewSet(APITestCase):
    url = reverse('study-list')

    def setUp(self):
        super(TestStudySubjectViewSet, self).setUp()
        StudySubjectFactory.create_batch(5)
        self.user = UserFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class TestCertificateViewSet(APITestCase):
    url = reverse('certificate-list')

    def setUp(self):
        super(TestCertificateViewSet, self).setUp()
        TeachingSubjectFactory.create_batch(5)
        CertificateFactory.create_batch(5)
        self.user = UserFactory.create()
        self.certificate = CertificateFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, self.certificate.title)

    def test_post(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            'teaching_subject': TeachingSubjectFactory.create().pk,
            'title': 'title',
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_delete(self):
        cf = Certificate.objects.all().last()
        pk = cf.pk
        url = reverse('certificate-detail', kwargs={'pk': pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(cf.teaching_subject.teacher_information.user)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_put(self):
        cf = Certificate.objects.all().last()
        pk = cf.pk
        url = reverse('certificate-detail', kwargs={'pk': pk})
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(cf.teaching_subject.teacher_information.user)
        data = {
            'teaching_subject': TeachingSubjectFactory.create().pk,
            'title': 'title',
        }
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestPortfolioViewSet(APITestCase):
    url = reverse('portfolio-list')

    def setUp(self):
        super(TestPortfolioViewSet, self).setUp()
        TeachingSubjectFactory.create_batch(5)
        PortfolioFactory.create_batch(5)
        self.user = UserFactory.create()
        self.certificateFactory = PortfolioFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, self.certificateFactory.title)
        self.assertEqual(len(response.data['results']), 6)

    def test_post(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            'teaching_subject': TeachingSubjectFactory.create().pk,
            'title': 'title',
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_delete(self):
        cf = Portfolio.objects.all().last()
        pk = cf.pk
        url = reverse('portfolio-detail', kwargs={'pk': pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(cf.teaching_subject.teacher_information.user)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_put(self):
        cf = Portfolio.objects.all().last()
        pk = cf.pk
        url = reverse('portfolio-detail', kwargs={'pk': pk})
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(cf.teaching_subject.teacher_information.user)
        data = {
            'teaching_subject': TeachingSubjectFactory.create().pk,
            'title': 'title',
        }
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestTeachingSubjectViewSet(APITestCase):
    url = reverse('teacher_subject-list')

    def setUp(self):
        super(TestTeachingSubjectViewSet, self).setUp()
        TeachingSubjectFactory.create_batch(5)
        self.user = UserFactory.create()
        self.teacher_subject = TeachingSubjectFactory.create()
        TeacherInformationFactory.create_batch(5)
        StudySubjectFactory.create_batch(5)

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 6)

    def test_post(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        data = {
            'teacher_information': TeacherInformationFactory.create().pk,
            'study': StudySubjectFactory.create().pk
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_delete(self):
        ti = TeachingSubject.objects.all().last()
        pk = ti.pk
        url = reverse('teacher_subject-detail', kwargs={'pk': pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(ti.teacher_information.user)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)


class TestTeachingExperianceView(APITestCase):
    url = reverse('teaching_experience-list')

    def setUp(self):
        super(TestTeachingExperianceView, self).setUp()
        TeachingSubjectFactory.create_batch(5)
        TeachingExperianceFactory.create_batch(5)
        self.user = UserFactory.create()
        self.certificateFactory = TeachingExperianceFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, self.certificateFactory.title)
        self.assertEqual(len(response.data['results']), 6)

    def test_post(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            'teaching_subject': TeachingSubjectFactory.create().pk,
            'title': 'title',
            'how_long': random.choice(['less half year', '0.5-1', '2', '5', '10', ])
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_delete(self):
        te = TeachingExperiance.objects.all().last()
        pk = te.pk
        url = reverse('teaching_experience-detail', kwargs={'pk': pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(te.teaching_subject.teacher_information.user)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_put(self):
        te = TeachingExperiance.objects.all().last()
        pk = te.pk
        url = reverse('teaching_experience-detail', kwargs={'pk': pk})
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(te.teaching_subject.teacher_information.user)
        data = {
            'teaching_subject': TeachingSubjectFactory.create().pk,
            'title': 'title',
            'how_long': random.choice(['less half year', '0.5-1', '2', '5', '10', ])
        }
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestWorkExperianceView(APITestCase):
    url = reverse('work_experience-list')

    def setUp(self):
        super(TestWorkExperianceView, self).setUp()
        TeachingSubjectFactory.create_batch(5)
        WorkExperianceFactory.create_batch(5)
        self.user = UserFactory.create()
        self.certificateFactory = WorkExperianceFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, self.certificateFactory.title)
        self.assertEqual(len(response.data['results']), 6)

    def test_post(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            'teaching_subject': TeachingSubjectFactory.create().pk,
            'title': 'title',
            'how_long': random.choice(['less half year', '0.5-1', '2', '5', '10', ])
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_delete(self):
        we = WorkExperiance.objects.all().last()
        pk = we.pk
        url = reverse('work_experience-detail', kwargs={'pk': pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(we.teaching_subject.teacher_information.user)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_put(self):
        we = WorkExperiance.objects.all().last()
        pk = we.pk
        url = reverse('work_experience-detail', kwargs={'pk': pk})
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(we.teaching_subject.teacher_information.user)
        data = {
            'teaching_subject': TeachingSubjectFactory.create().pk,
            'title': 'title',
            'how_long': random.choice(['less half year', '0.5-1', '2', '5', '10', ])
        }
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestEducationView(APITestCase):
    url = reverse('education-list')

    def setUp(self):
        super(TestEducationView, self).setUp()
        TeachingSubjectFactory.create_batch(5)
        EducationFactory.create_batch(5)
        self.user = UserFactory.create()
        self.certificateFactory = EducationFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, self.certificateFactory.title)
        self.assertEqual(len(response.data['results']), 6)

    def test_post(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        data = {
            'teacher_information': TeacherInformationFactory.create().pk,
            'title': 'title',
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_delete(self):
        ed = Education.objects.all().last()
        pk = ed.pk
        url = reverse('education-detail', kwargs={'pk': pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(ed.teacher_information.user)
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_put(self):
        ed = Education.objects.all().last()
        pk = ed.pk
        url = reverse('education-detail', kwargs={'pk': pk})
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(ed.teacher_information.user)
        data = {
            'teacher_information': TeacherInformationFactory.create().pk,
            'title': 'title',
        }
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class TestUserRegistration(APITestCase):
    url = reverse('register-user')

    def test_post(self):
        # test successful registarion
        data = {
            'username': 'bob',
            'email': 'bob@example.com',
            'password': 'secret12',
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'token')

        # test created user's fields
        new_user = User.objects.get(username='bob')
        self.failUnless(new_user.check_password('secret12'))
        self.assertEqual(new_user.email, 'bob@example.com')

        # new user must not be active.
        self.failIf(new_user.is_active)

        # check profile was created and email sended
        self.assertEqual(Profile.objects.count(), 1)
        self.assertEqual(len(mail.outbox), 1)

        # not uniq email
        data = {
            'username': 'bob_2',
            'email': 'bob@example.com',
            'password': 'secret12',
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['email_already_exists'] in str(response.data), True)

        # short password
        data = {
            'username': 'bob_2',
            'email': 'bob@example2.com',
            'password': 'secret',
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['invalid_password'] in str(response.data), True)

        # no password
        data = {
            'username': 'bob_2',
            'email': 'bob@example2.com',
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual('This field is required.' in str(response.data), True)

        # no email
        data = {
            'username': 'bob_2',
            'password': 'secret',
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual('This field is required.' in str(response.data), True)

        # no username
        data = {
            'email': 'bob@example_2.com',
            'password': 'secret',
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual('This field is required.' in str(response.data), True)


class TestUserLogin(APITestCase):
    url = reverse('user-login')

    def setUp(self):
        super(TestUserLogin, self).setUp()
        UserFactory.create_batch(1)
        user = User.objects.first()
        user.set_password('qweqwe12')
        user.save()

    def test_post(self):
        user = User.objects.first()

        # test successful login
        data = {
            'email': user.email,
            'password': 'qweqwe12',
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'token')
        self.assertEqual(response.data['user_id'], user.id)

        # no password
        data = {
            'email': user.email,
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual('This field is required.' in str(response.data), True)

        # invalid password
        data = {
            'email': user.email,
            'password': 'qweqwe121',
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['cant_login'] in str(response.data), True)

        # user doesn't exist
        data = {
            'email': 'qweqwe@qwe.qwe',
            'password': 'qweqwe12',
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['user_not_exists'] in str(response.data), True)

        # user isn't active
        user.is_active = False
        user.save()
        data = {
            'email': user.email,
            'password': 'qweqwe12',
        }
        response = self.client.post(self.url, data=data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['user_inactive'] in str(response.data), True)


class TestResendActivationKey(APITestCase):
    url = reverse('resend-activation-key')

    def setUp(self):
        today_date = datetime.today().date()
        UserFactory.create_batch(1, activation_key_created=today_date, is_active=False)

    def test_post(self):
        user = User.objects.first()

        # successful resend
        response = self.client.post(self.url, data={'email': user.email})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(mail.outbox), 1)
        user = User.objects.first()
        self.assertNotEqual(user.activation_key, '')

        # user with email doesn't exist
        response = self.client.post(self.url, data={'email': user.email + 'a'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['user_not_exists'] in str(response.data), True)

        # user already active
        user.is_active = True
        user.save()
        response = self.client.post(self.url, data={'email': user.email})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual('has already activated' in str(response.data), True)


class TestActivateUser(APITestCase):
    url_name = 'activate_user'

    def setUp(self):
        today_date = datetime.today().date()
        user = UserFactory.create_batch(1, activation_key_created=today_date, is_active=False)[0]
        user.activation_key = User.objects._generate_activation_key(user.username)
        user.save()

    def test_get(self):
        user = User.objects.first()
        url = reverse(self.url_name, kwargs={'activation_key': user.activation_key})

        # successful user activation
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user = User.objects.first()
        self.assertEqual(user.is_active, True)

        user.activation_key = User.objects._generate_activation_key(user.username)
        user.is_active = False
        user.save()

        # invalid activation key
        invalid_url = reverse(self.url_name, kwargs={'activation_key': '3d7c452cbc39bc663389218054c3d9f204c4872e'})
        response = self.client.get(invalid_url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['invalid_activation_key'] in str(response.data), True)

        # too old activation key
        too_old_date = datetime.today().date() - timedelta(days=settings.ACCOUNT_ACTIVATION_DAYS + 1)
        user.activation_key_created = too_old_date
        user.save()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['invalid_activation_key'] in str(response.data), True)


class TestPasswordReset(APITestCase):
    url = reverse('password-reset')

    def setUp(self):
        UserFactory.create_batch(1)

    def test_post(self):
        user = User.objects.first()

        # successful send password reset email
        response = self.client.post(self.url, data={'email': user.email})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(mail.outbox), 1)

        # user with email doesn't exist
        response = self.client.post(self.url, data={'email': user.email + 'a'})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['user_not_exists'] in str(response.data), True)
        self.assertEqual(len(mail.outbox), 1)


class TestPasswordResetComplete(APITestCase):
    def setUp(self):
        UserFactory.create_batch(2)

    def test_post(self):
        user_1 = User.objects.first()

        # create valid link
        uid = urlsafe_base64_encode(str(user_1.id))
        token = default_token_generator.make_token(user_1)
        url_in_email = reverse('password_reset_confirm', kwargs={'uidb64': uid, 'token': token})

        new_password = 'qweqwe13'

        # set new password
        data = {
            'password': new_password,
            'confirm_password': new_password
        }
        response = self.client.post(url_in_email, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # check new password
        data = {
            'email': user_1.email,
            'password': new_password,
        }
        response = self.client.post(reverse('user-login'), data=data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'token')

        # for unsuccessful attempts
        user_2 = User.objects.all().exclude(id=user_1.id).first()
        uid = urlsafe_base64_encode(str(user_2.id))
        token = default_token_generator.make_token(user_2)
        url_in_email = reverse('password_reset_confirm', kwargs={'uidb64': uid, 'token': token})

        # different_passwords
        data = {
            'password': new_password,
            'confirm_password': new_password + 'a'
        }
        response = self.client.post(url_in_email, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['different_passwords'] in str(response.data), True)

        # only one password
        data = {
            'password': new_password,
        }
        response = self.client.post(url_in_email, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual('required' in str(response.data), True)


class TestPasswordChange(APITestCase):
    url = reverse('password-change')

    def setUp(self):
        import base64
        user = UserFactory.create()
        user.save()
        current_password = 'qweqwe12'
        user.set_password(current_password)
        user.save()
        credentials = base64.b64encode('{}:{}'.format(user.username, current_password))

        self.client.defaults['HTTP_AUTHORIZATION'] = 'Basic ' + credentials

    def test_post(self):

        current_password = 'qweqwe12'
        new_password = 'qweqwe13'

        # current password is wrong
        data = {
            'current_password': current_password + 'a',
            'new_password': new_password,
            'confirm_new_password': new_password
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(ERR_MSG['current_wrong_password'] in str(response.data), True)

        # only one new password
        data = {
            'current_password': current_password,
            'new_password': new_password,
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual('required' in str(response.data), True)

        # set new password
        data = {
            'current_password': current_password,
            'new_password': new_password,
            'confirm_new_password': new_password,
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
