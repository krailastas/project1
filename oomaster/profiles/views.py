# encoding: utf-8
from __future__ import unicode_literals
import json

from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.contrib.auth import get_user_model
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.tokens import default_token_generator

from rest_framework.viewsets import ReadOnlyModelViewSet, GenericViewSet, ModelViewSet
from rest_framework.generics import GenericAPIView
from rest_framework import filters
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST
from rest_framework.views import APIView
from rest_framework import viewsets, mixins
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from social.actions import do_auth

from .utils import strategy
from .permissions import EmailIsOwnerPermission
from .models import Profile

from notification.models import DeviceTokenNotification

from profiles.serializers import (AuthSerializer, PasswordResetSerializer, TeachingSubjectSerializer,
                                  PasswordResetCompleteSerializer, PasswordChangeSerializer,
                                  RegistrationSerializer, ResendActivationKeySerializer, ActivationSerializer,
                                  EmailSerializer, UserSerializer, ProfileSerializer, EducationSerializer,
                                  TeacherInformationSerializer, ImageSerializer, WorkExperianceSerializer,
                                  StudySubjectSerializer, CertificateSerializer, PortfolioSerializer,
                                  TeachingExperianceSerializer)

from profiles.permissions import (IsTeacherPermission, IsProfilePermission, IsTeacherInformationPermission,
                                  IsTeachingSubjectPermission, IsEducationPermission, IsUserPermission)

from profiles.filters import (FilterTeacherInformationBackend, FilterTeachingExperienceBackend,
                              FilterTeachingSubjectBackend, FilterPortfolioBackend, FilterUserBakend,
                              FilterCertificateBackend, FilterWorkExperienceBackend)

from profiles.models import (Image, TeacherInformation, StudySubject,
                             Certificate, Portfolio, TeachingExperiance,
                             Education, WorkExperiance, TeachingSubject)


UserModel = get_user_model()


class UserAuthView(GenericAPIView):
    """
    **User Auth View**
        View for user's authentication.
        Add deviceToken and deviceType.Field is not required.

        **list fields:**

        * `email`
        * `password`
        * `deviceType`
        * `deviceToken`

        **returns:**

        * `user's token`
        * `user's id`
    """
    serializer_class = AuthSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.DATA)
        if serializer.is_valid():
            return Response(serializer.object)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class PasswordResetView(GenericAPIView):
    """
    **Password Reset View**
        Send email to user's email with link to 'set new password' page.
        Required email of existing user. Link for ressetting password
        is valid for the number of days specified in settings.PASSWORD_RESET_TIMEOUT_DAYS

        **list fields:**

        * `email`

        **returns:**

        * `user's email address`
    """
    serializer_class = PasswordResetSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.DATA, context={'request': request})

        if serializer.is_valid():
            opts = {
                'from_email': None,
            }
            serializer.save(**opts)
            return Response(serializer.data)

        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class PasswordResetCompleteView(GenericAPIView):
    """
    **Password Reset Complete View**
        View for resetting user's password.

        **list fields:**

        * `password`
        * `confirm_password`

        **returns:**

        * `new password`
    """
    serializer_class = PasswordResetCompleteSerializer

    def post(self, request, *args, **kwargs):
        UserModel = get_user_model()
        try:
            uid = urlsafe_base64_decode(kwargs['uidb64'])
            user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            user = None

        if user and default_token_generator.check_token(user, kwargs['token']):
            serializer = self.serializer_class(data=request.DATA, context={'request': request, 'user': user})
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                if 'non_field_errors' in serializer.errors:
                    return Response(serializer.errors['non_field_errors'], status=HTTP_400_BAD_REQUEST)
                return Response(serializer.error_messages, status=HTTP_400_BAD_REQUEST)
        return Response({'errors': _('The given user\'s id is wrong')}, status=HTTP_400_BAD_REQUEST)


class PasswordChangeView(GenericAPIView):
    serializer_class = PasswordChangeSerializer
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        UserModel = get_user_model()
        try:
            user = request.user
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            user = None
        if user:
            serializer = self.serializer_class(data=request.DATA, context={'request': request, 'user': user})
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                if 'non_field_errors' in serializer.errors:
                    return Response(serializer.errors['non_field_errors'], status=HTTP_400_BAD_REQUEST)
                return Response(serializer.error_messages, status=HTTP_400_BAD_REQUEST)
        return Response({'errors': _('The given user\'s id is wrong')}, status=HTTP_400_BAD_REQUEST)


class UserRegistrationView(GenericAPIView):
    """
    **User Registration View**
        View for registration new users. Create new user and
        send email for activation account. Create new
        instance User model and related Profile model.
        Unique email and username are required.
        Activation key in email is valid for the number
        of days specified in settings.ACCOUNT_ACTIVATION_DAYS.
        Add deviceToken and deviceType.Field is not required.

        **list fields:**

        * `email`
        * `username`
        * `password`
        * `deviceType`
        * `deviceToken`

        **returns:**

        * `user's email`
        * `user's token`
    """
    serializer_class = RegistrationSerializer

    def post(self, request, *args, **kwargs):
        deviceType = request.DATA.get('deviceType', None)
        deviceToken = request.DATA.get('deviceToken', None)
        serializer = self.serializer_class(data=request.DATA, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            user = serializer.object
            if deviceType and deviceToken:
                if not DeviceTokenNotification.objects.filter(user=user, deviceToken=deviceToken).exists():
                    DeviceTokenNotification.objects.create(user=user, deviceToken=deviceToken, deviceType=deviceType)
            return Response({'email': user.email, 'token': user.auth_token.key})
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class ResendActivationKeyView(GenericAPIView):
    """
    **Resend Activation Key View**
        View for resending email with activation key to specified email.
        Required email of existing non active user.
        Link in email is valid for the number of days
        specified in settings.ACCOUNT_ACTIVATION_DAYS.

        **list fields:**

        * `email`

        **returns:**

        * `user's email`
        * `user's token`
    """
    serializer_class = ResendActivationKeySerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.DATA, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            user = serializer.object
            return Response({'email': user.email, 'token': user.auth_token.key})
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class ActivateUserView(GenericAPIView):
    """
    **Activate User View**
        View to activate user. To activate user should create
        get request by link in activation email. Link in email is valid
        for the number of days specified in settings.ACCOUNT_ACTIVATION_DAYS.
        After successful request user becomes active.

        **list fields:**

        * `activation_key`

        **returns:**

        * `activation_key`
    """
    serializer_class = ActivationSerializer

    def get(self, request, *args, **kwargs):
        serializer = self.serializer_class(data={'activation_key': kwargs['activation_key']})
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.object)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class ChangeEmailView(viewsets.ModelViewSet):
    """
    **Change Email View**
        View for changing email. View can set new user's email if current
        user is email owner. Required valid and unique email.

        **list fields:**

        * `email`

        **returns:**

        * `email`
    """
    queryset = get_user_model().objects.all()
    serializer_class = EmailSerializer
    permission_classes = (EmailIsOwnerPermission, )


class AuthSocialView(APIView):
    """
    **Auth Social View**
        View for user’s authentification via social networks.
        Autorization via Facebook and google+ is supported.
        New user will be created if email from social networks is unique.

        **returns:**

        * `email`
        * `user's id`
        * `user's token`
    """
    @method_decorator(strategy())
    def post(self, request):
        token = request.DATA.get('access_token', None)
        if token:
            backend = request.strategy.backend
            try:
                json_token = json.loads(token)
            except ValueError:
                pass
            else:
                token = json_token
            user = backend.do_auth(token)
            if user:
                return Response({'email': user.email, 'token': user.auth_token.key, 'user_id': user.id})
        return Response(status=HTTP_400_BAD_REQUEST)


@strategy('proxy_complete')
def auth(request):
    return do_auth(request.social_strategy, redirect_name=REDIRECT_FIELD_NAME)


@strategy()
def proxy_complete(request):
    backend = request.strategy.backend
    token = backend.get_unauthorized_token()
    try:
        access_token = json.dumps(backend.access_token(token))
    except ValueError:
        access_token = token

    state = request.GET.get('state', None)

    if not state:
        raise Exception(_('Missing state'))

    return HttpResponseRedirect('/?access_token={0}&state={1}'.format(access_token, state))


class UserViewSet(mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.ListModelMixin,
                  GenericViewSet):
    """
    **User View**

        Return list of users
        Update user

        **returns:**

        * `profile`
        * `id`
        * `username`
        * `email`

        **filters:**

        * `id=<int>`
        * `?email=<text>`
        * `?username=<text>`
        * `?my_teachers` - list all users in teacher list of authenticated user
        * `?teachers` - list all teachers
    """
    queryset = UserModel.objects.select_related('profile', 'teacher_info').prefetch_related('profile', 'teacher_info')
    serializer_class = UserSerializer
    permission_classes = (IsUserPermission, )
    filter_fields = ('id', 'username', 'email')
    filter_backends = (filters.SearchFilter, FilterUserBakend, )
    search_fields = ('first_name', 'last_name', 'username')

    def get_queryset(self):
        """
        Unauthorized user can see only teacher's page.
        """

        queryset = super(UserViewSet, self).get_queryset()
        if not (self.request.user and self.request.user.is_authenticated()):
            queryset = queryset.filter(profile__is_teacher=True)
        return queryset


class ProfileViewSet(mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.ListModelMixin,
                     GenericViewSet):
    """
    **Profile View**

        Return list of profiles
        Update profile

        **returns:**

        * `user`
        * `profile_image`
        * `bio`
        * `gender`
        * `country`
        * `region`
        * `city`
        * `date_birthday`
        * `is_teacher`
        * `avatar`

        **filters:**

        * `?user=<pk>`
        * `?is_teacher=<True/False>`
    """
    queryset = Profile.objects.select_related()
    serializer_class = ProfileSerializer
    permission_classes = (IsProfilePermission, )
    filter_fields = ('user', 'is_teacher')

    def pre_save(self, obj):
        if obj.avatar:
            if not obj.avatar.is_active:
                Image.objects.filter(pk=obj.avatar.pk).update(is_active=True)

    def get_queryset(self):
        """
        Unauthorized user can see only teacher's page.
        """
        queryset = super(ProfileViewSet, self).get_queryset()
        if not (self.request.user and self.request.user.is_authenticated()):
            queryset = queryset.filter(is_teacher=True)
        return queryset


class TeacherInformationViewSet(mixins.RetrieveModelMixin,
                                mixins.UpdateModelMixin,
                                mixins.ListModelMixin,
                                GenericViewSet):
    """
    **Teacher Information View**

        Return list of teacher informations.
        Update teacher information.

        **returns:**

        * `user`
        * `file`

        **filters:**

        * `?user=<pk>`
    """
    queryset = TeacherInformation.objects.select_related('user')
    serializer_class = TeacherInformationSerializer
    permission_classes = (IsTeacherInformationPermission,)
    filter_backends = (FilterTeacherInformationBackend,)


class ImageView(mixins.CreateModelMixin,
                mixins.RetrieveModelMixin,
                mixins.ListModelMixin,
                GenericViewSet):
    """
    **Images View**

        Return list of images.
        Create image.

        **returns:**

        * `image`
    """
    queryset = Image.objects.all()
    serializer_class = ImageSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, )


class StudySubjectView(ReadOnlyModelViewSet):
    """
    **Study Subject View**

        Return list of study subjects

        **returns:**

        * `title`
    """
    model = StudySubject
    serializer_class = StudySubjectSerializer


class CertificateView(ModelViewSet):
    """
    **Certificate View**

        Return list of certificates.
        Update certificate.
        Create new certificate.
        Delete certificate.

        **returns:**

        * `teaching_subject`
        * `title`
        * `certificate_image`

        **filters:**

        * `?user=<pk>`
        * `?teaching_subject=<pk>`
        * `?title=<text>`
    """
    queryset = Certificate.objects.select_related('teaching_subject')
    serializer_class = CertificateSerializer
    permission_classes = (IsTeachingSubjectPermission,)
    filter_backends = (FilterCertificateBackend,)


class PortfolioView(ModelViewSet):
    """
    **Portfolio View**

        Return list of portfolios.
        Update portfolio.
        Create new portfolio.
        Delete portfolio.

        **returns:**

        * `title`
        * `teaching_subject`
        * `portfolio_image`
        * `portfolio_url`

        **filters:**

        * `?user=<pk>`
        * `?teaching_subject=<pk>`
        * `?title=<text>`
    """
    queryset = Portfolio.objects.select_related().prefetch_related('teaching_subject')
    serializer_class = PortfolioSerializer
    permission_classes = (IsTeachingSubjectPermission,)
    filter_backends = (FilterPortfolioBackend,)


class TeachingExperienceView(ModelViewSet):
    """
    **Teaching Experience View**

        Return list of teaching experiances.
        Update teaching experiance.
        Create new teaching experiance.
        Delete teaching experiance.

        **returns:**

        * `teaching_subject`
        * `title`
        * `how_long`

        **filters:**

        * `?user=<pk>`
        * `?teaching_subject=<pk>`
        * `?title=<text>`
    """
    queryset = TeachingExperiance.objects.select_related('teaching_subject')
    serializer_class = TeachingExperianceSerializer
    permission_classes = (IsTeachingSubjectPermission,)
    filter_backends = (FilterTeachingExperienceBackend,)


class EducationView(ModelViewSet):
    """
    **Education View**

        Return list of educations.
        Update teaching education.
        Create new teaching education.
        Delete teaching education.

        **returns:**

        * `teacher_information`
        * `title`
        * `diploma_image`

        **filters:**

        * `?teacher_information__user=<int>`
    """
    queryset = Education.objects.select_related('teacher_information')
    serializer_class = EducationSerializer
    permission_classes = (IsEducationPermission,)
    filter_fields = ('teacher_information__user', )


class WorkExperienceView(ModelViewSet):
    """
    **Work Experience View**

        Return list of work experiances.
        Update teaching work experiance.
        Create new teaching work experiance.
        Delete teaching work experiance.

        **returns:**

        * `teaching_subject`
        * `title`
        * `how_long`

        **filters:**

        * `?user=<pk>`
        * `?teaching_subject=<pk>`
        * `?title=<text>`
    """
    model = WorkExperiance
    serializer_class = WorkExperianceSerializer
    permission_classes = (IsTeachingSubjectPermission,)
    filter_backends = (FilterWorkExperienceBackend,)


class TeachingSubjectViewSet(ModelViewSet):
    """
    **Teaching Subject View**

        Return list of work teaching subjects.
        Update teaching work teaching subject.
        Create new teaching subject.
        Delete teaching subject.

        **returns:**

        * `teacher_information `
        * `study`
        * `url_website`
        * `image_website`

        **filters:**

        * `?user=<pk>`
    """
    queryset = TeachingSubject.objects.select_related('profile')
    serializer_class = TeachingSubjectSerializer
    permission_classes = (IsTeacherPermission,)
    filter_backends = (FilterTeachingSubjectBackend,)


register_by_access_token = AuthSocialView.as_view()
change_email = ChangeEmailView.as_view({'patch': 'partial_update'})

activate_user = ActivateUserView.as_view()
register_user = UserRegistrationView.as_view()
resend_activation_key = ResendActivationKeyView.as_view()

password_reset_complete = PasswordResetCompleteView.as_view()
password_change = PasswordChangeView.as_view()
password_reset = PasswordResetView.as_view()
user_auth = UserAuthView.as_view()
