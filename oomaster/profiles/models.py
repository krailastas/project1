import hashlib
import random
import re
from datetime import datetime, timedelta

from django.core.files import File
from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser, UserManager
from django.db.models.signals import post_save
from django.utils import timezone
from django.core.exceptions import ObjectDoesNotExist
from django.utils.translation import ugettext_lazy as _
from django.template.loader import get_template
from django.template import Context

import cStringIO as StringIO
import ho.pisa as pisa
from rest_framework.authtoken.models import Token

from cities_light.models import Country, Region, City
from model_utils import Choices
from model_utils.models import TimeStampedModel
from notification_settings.models import NotificationSettings, \
    NotificationUserSettings


SHA1_RE = re.compile(r'^[a-f0-9]{40}$')


class CustomUserManager(UserManager):
    def _generate_activation_key(self, username):
        """
        Generating hash key for activation.
        Of course, There could be collisions that's why we check in db if it exists there.
        In result, we will get unique activation key without collisions in database.
        Hash key is generated in hex format, which is fixed at 40 characters.
        """
        while True:
            salt = hashlib.sha1(str(random.random())).hexdigest()[:5]
            if isinstance(username, unicode):
                username = username.encode('utf-8')
            hash_key = hashlib.sha1(salt + username).hexdigest()
            try:
                self.model.objects.get(activation_key=hash_key)
            except ObjectDoesNotExist:
                break
        return hash_key

    def create_inactive_user(self, username, email,
                             password, is_staff=False, is_active=False,
                             is_superuser=False, **extra_fields):
        now = timezone.now()
        if not username:
            raise ValueError(_('The given username must be set'))

        if not email:
            raise ValueError(_('The given email must be set'))

        email = self.normalize_email(email)
        user = self.model(username=username, email=email, is_staff=is_staff, is_active=is_active,
                          is_superuser=is_superuser, last_login=now,
                          activation_key=self._generate_activation_key(username),
                          activation_key_created=datetime.today().date(), date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def activate_user(self, activation_key):
        """
        Activating user by hash key.
        :return: False if key is invalid or User's instance is activated
        """

        if SHA1_RE.search(activation_key):
            try:
                user = self.model.objects.get(activation_key=activation_key, is_active=False)
            except ObjectDoesNotExist:
                return False
            else:
                last_valid_activation_date = user.activation_key_created + timedelta(
                    days=settings.ACCOUNT_ACTIVATION_DAYS)
                if last_valid_activation_date >= datetime.today().date():
                    user.activation_key = ''
                    user.is_active = True
                    user.save(using=self._db)
                    return user
                else:
                    return False
        return False

    def check_activation_key(self, activation_key):
        """
        Checking whether activation key is valid and user can be activated by that key
        """
        return self.model.objects.filter(activation_key=activation_key, is_active=False).exists()


AbstractUser._meta.get_field('email')._unique = True


class User(AbstractUser):
    """
    **User Model**
        Default model for User that replaces auth.User.
        Email and username are required.

        **list fields:**

        * `username` - CharField
        * `first_name` - CharField
        * `last_name` - CharField
        * `email` - EmailField
        * `is_staff` - BooleanField
        * `is_active` -BooleanField
        * `date_joined` - DateTimeField
        * `password` - CharField
        * `last_login` - DateTimeField
        * `is_active` - BooleanField
        * `activation_key` - CharField
        * `activation_key_created` - DateField
    """
    activation_key = models.CharField(_('activation key'), max_length=40, blank=True)
    activation_key_created = models.DateField(null=True)

    objects = CustomUserManager()

    def __str__(self):
        return self.username

    def get_followed_teachers(self):
        return self.__class__.objects.filter(followers__user=self.id)


class StudySubject(TimeStampedModel):
    """
    **StudySubject Model**
        Class to store study subjects.

        **list fields:**

        * `title` - CharField
    """
    title = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.title


class Image(TimeStampedModel):
    """
    **Image Model**

        **list fields:**

        * `avatar` - ImageField
        * `is_active` - BooleanField
    """
    image = models.ImageField(upload_to='images')
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return self.image.url


class Profile(TimeStampedModel):
    """
    **Profile Model**
        Class to extend User model. Has one-to-one relation with User model.

        **list fields:**

        * `user` - OneToOneField - User
        * `bio` - TextField
        * `gender` - CharField
        * `country` - ForeignKey - cities_light.models.Country
        * `region` - ForeignKey - cities_light.models.Region
        * `city` - ForeignKey - cities_light.models.City
        * `date_birthday` - DateField
        * `is_teacher` - BooleanField
        * `avatar` - ForeignKey - Image
        * `phone_number` - CharField
    """
    GENDER_CHOICES = Choices(
        'Male',
        'Female',
    )
    user = models.OneToOneField(User, related_name='profile')
    bio = models.TextField(blank=True)
    gender = models.CharField(choices=GENDER_CHOICES, max_length=10, blank=True)
    country = models.ForeignKey(Country, blank=True, null=True)
    region = models.ForeignKey(Region, blank=True, null=True)
    city = models.ForeignKey(City, blank=True, null=True)
    date_birthday = models.DateField(blank=True, null=True)
    is_teacher = models.BooleanField(default=False)
    avatar = models.ForeignKey(Image, related_name='avatars', blank=True, null=True)
    phone_number = models.CharField(max_length=100, blank=True)
    is_public = models.BooleanField(default=True)

    def __str__(self):
        return "{}'s profile".format(self.user_id)

    def become_teacher(self):
        if self.is_teacher is False:
            self.is_teacher = True
            self.save()
            return True
        else:
            return False


class TeacherInformation(TimeStampedModel):
    """
    **TeacherInformation Model**
        Model extends Profile model if Profile instance is teacher. (field is_teacher equal True).
        One-to-one relation with Profile model is required.

        **list fields:**

        * `user`
        * `file`
    """
    user = models.OneToOneField(User, related_name='teacher_info')
    file = models.FileField(upload_to='profiles/pdf', blank=True, null=True)

    def __str__(self):
        return 'teaching-information-{0}'.format(self.user.username)


class Education(TimeStampedModel):
    """
    **Education Model**
        Class to store educations of teachers.
        It has many-to-one relation with TeacherInformation model.

        **list fields:**

        * `teacher_information` - ForeignKey - TeacherInformation
        * `title` - CharField
        * `diploma_image` - ImageField
    """
    teacher_information = models.ForeignKey(TeacherInformation, related_name='educations')
    title = models.CharField(max_length=512)
    diploma_image = models.ImageField(upload_to='profile/diploma_images', blank=True, null=True)

    class Meta:
        unique_together = ('teacher_information', 'title')

    def __str__(self):
        return self.title


class TeachingSubject(TimeStampedModel):
    """
    **TeachingSubject Model**
        Class to store teaching subjects of teachers.
        Has many-to-one relation with TeacherInformation model.

        **list fields:**

        * `teacher_information` - ForeignKey - TeacherInformation
        * `study` - ForeignKey - StudySubject
        * `url_website` - URLField
        * `image_website` - ImageField
    """
    teacher_information = models.ForeignKey(TeacherInformation, related_name='teaching_subjects')
    study = models.ForeignKey(StudySubject, verbose_name=_('study'))
    url_website = models.URLField(blank=True, null=True)
    image_website = models.ImageField(upload_to='profile/image_website', blank=True, null=True)

    class Meta:
        unique_together = [('teacher_information', 'study')]

    def __str__(self):
        return 'teaching-subject-{0}'.format(self.teacher_information.user.username)


class Certificate(TimeStampedModel):
    """
    **Certificate Model**
        Class to store certificates of teaching subjects.
        Have many-to-one relation with TeachingSubject model.

        **list fields:**

        * `teaching_subject` - ForeignKey - TimeStampedModel
        * `title` - CharField
        * `certificate_image` - ImageField
    """
    teaching_subject = models.ForeignKey(TeachingSubject, related_name='certificates')
    title = models.CharField(max_length=512)
    certificate_image = models.ImageField(upload_to='profile/certificate_images', blank=True, null=True)

    class Meta:
        unique_together = [('teaching_subject', 'title')]

    def __str__(self):
        return self.title


class Portfolio(TimeStampedModel):
    """
    **Portfolio Model**
        Class to store portfolios of teaching subjects.
        It has many-to-one relation with TeachingSubject model.

        **list fields:**

        * `teaching_subject` - ForeignKey - TeachingSubject
        * `title` - CharField
        * `portfolio_image` - ImageField
        * `portfolio_url` - URLField
    """
    teaching_subject = models.ForeignKey(TeachingSubject, related_name='portfolios')
    title = models.CharField(max_length=512)
    portfolio_image = models.ImageField(upload_to='profile/certificate_images', blank=True, null=True)
    portfolio_url = models.URLField(blank=True)

    class Meta:
        unique_together = [('teaching_subject', 'title')]

    def __str__(self):
        return self.title


class ExperianceBase(TimeStampedModel):
    HOW_LONG_CHOICES = Choices(
        'less half year',
        '0.5-1',
        '2',
        '5',
        '10',
    )

    title = models.CharField(max_length=512, help_text='work & teaching')
    how_long = models.CharField(choices=HOW_LONG_CHOICES, max_length=30)

    class Meta:
        unique_together = [('teaching_subject', 'title')]
        abstract = True

    def __str__(self):
        return self.title


class WorkExperiance(ExperianceBase):
    """
    **WorkExperiance Model**
        Model for store work experience of teaching subject.
        Have many-to-one relation with TeachingSubject model.

        **list fields:**

        * `teaching_subject` - ForeignKey - TeachingSubject
        * `title` - CharField
        * `how_long` - CharField
    """
    teaching_subject = models.ForeignKey(TeachingSubject, related_name='work_experiances')


class TeachingExperiance(ExperianceBase):
    """
    **TeachingExperiance Model**
        Class to store teaching experience of teaching subjects.
        It has many-to-one relation with TeachingSubject model.

        **list fields:**

        * `teaching_subject` - ForeignKey - TeachingSubject
        * `title` - CharField
        * `how_long` - CharField
    """
    teaching_subject = models.ForeignKey(TeachingSubject, related_name='teaching_experiances')


def create_profile(sender, instance, created, **kwargs):
    if created:
        Token.objects.get_or_create(user=instance)
        profile, created = Profile.objects.get_or_create(user=instance)


post_save.connect(create_profile, sender=User)


def create_teacher_information(sender, instance, created, **kwargs):
    if not TeacherInformation.objects.filter(user=instance.user).exists() and instance.is_teacher:
        TeacherInformation.objects.create(user=instance.user)


post_save.connect(create_teacher_information, sender=Profile)


def create_pdf_cv(sender, instance, created, **kwargs):
    if instance.is_teacher:
        template = get_template('pdf/pdf.html')
        context_dict = {'user': instance.user}
        buffer = StringIO.StringIO()
        context = Context(context_dict)
        html = template.render(context)
        pisa.pisaDocument(StringIO.StringIO(html.encode("UTF-8")), dest=buffer,
                          link_callback='{0}/profiles/pdf/'.format(settings.MEDIA_ROOT), encoding='utf-8')
        pdf = buffer.getvalue()
        buffer.close()
        filepath = '{0}/profiles/pdf/{1}_resume.pdf'.format(settings.MEDIA_ROOT, instance.user_id)
        destination = open(filepath, "wb+")
        newfile = File(destination)
        newfile.write(pdf)
        t = TeacherInformation.objects.get(user=instance.user)
        t.file = newfile
        t.save()


post_save.connect(create_pdf_cv, sender=Profile)


def create_notification_settings_to_user(sender, instance, created, **kwargs):
    if created:
        for ns in NotificationSettings.objects.all():
            NotificationUserSettings.objects.create(user=instance, notification=ns, mode=ns.is_choice)


post_save.connect(create_notification_settings_to_user, sender=User)
