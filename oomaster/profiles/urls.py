# encoding: utf-8
from __future__ import unicode_literals

from django.conf.urls import patterns, url
from rest_framework.routers import SimpleRouter

from custom_cities_light.views import CountryModelViewSet, CityModelViewSet

from .views import UserViewSet, ProfileViewSet

from profiles.views import (TeacherInformationViewSet, StudySubjectView,
                            CertificateView, PortfolioView, TeachingSubjectViewSet,
                            TeachingExperienceView, EducationView, WorkExperienceView,
                            ImageView)


router = SimpleRouter(trailing_slash=False)
router.register('users', UserViewSet, base_name='users')
router.register('countries', CountryModelViewSet, base_name='country')
router.register('cities', CityModelViewSet, base_name='city')
router.register('profiles', ProfileViewSet, base_name='profile')
router.register('teacher-information', TeacherInformationViewSet, base_name='teacher_information')
router.register('studies', StudySubjectView, base_name='study')
router.register('certificates', CertificateView, base_name='certificate')
router.register('portfolios', PortfolioView, base_name='portfolio')
router.register('teacher-subjects', TeachingSubjectViewSet, base_name='teacher_subject')
router.register('teaching-experiences', TeachingExperienceView, base_name='teaching_experience')
router.register('educations', EducationView, base_name='education')
router.register('work-experiences', WorkExperienceView, base_name='work_experience')
router.register('images', ImageView, base_name='images')

urlpatterns = router.urls

urlpatterns += patterns('profiles.views',
                        url(r'^login/$', 'user_auth', name='user-login'),

                        url(r'^social_login/$', 'auth', name='social_auth'),  # social auth
                        url(r'^proxy_complete/$', 'proxy_complete', name='proxy_complete'),
                        url(r'^register_by_token/$', 'register_by_access_token', name='register_by_token'),

                        url(r'^password_reset/$', 'password_reset', name='password-reset'),

                        url(r'^password_change/$', 'password_change', name='password-change'),

                        url(r'^resend_activation_key/$', 'resend_activation_key', name='resend-activation-key'),

                        url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
                            'password_reset_complete', name='password_reset_confirm'),

                        url(r'^register/$', 'register_user', name='register-user'),
                        url(r'^register/activate/(?P<activation_key>[a-f0-9]{40})/$', 'activate_user',
                            name='activate_user'),

                        url(r'^change_email/(?P<pk>\d+)/$', 'change_email', name='change-email')
                        )
