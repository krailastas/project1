from __future__ import unicode_literals

from rest_framework import filters

from profiles.models import TeacherInformation, User, Profile, TeachingSubject,\
    Portfolio, Certificate, WorkExperiance, TeachingExperiance


class FilterTeacherInformationBackend(filters.DjangoFilterBackend):
    class Meta:
        model = TeacherInformation
        fields = ['user']

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterTeacherInformationBackend, self).filter_queryset(request, queryset, view)
        filter_kwargs = {}
        user = request.QUERY_PARAMS.get('user', None)
        if user:
            try:
                filter_kwargs['user_id'] = int(user)
            except:
                filter_kwargs = {}
        if filter_kwargs:
            queryset = queryset.filter(**filter_kwargs)
        return queryset


class FilterTeachingSubjectBackend(filters.DjangoFilterBackend):
    class Meta:
        model = TeachingSubject
        fields = ['teacher_information']

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterTeachingSubjectBackend, self).filter_queryset(request, queryset, view)
        filter_kwargs = {}
        user = request.QUERY_PARAMS.get('user', None)
        if user:
            try:
                filter_kwargs['teacher_information__user_id'] = int(user)
            except:
                filter_kwargs = {}
        if filter_kwargs:
            queryset = queryset.filter(**filter_kwargs)
        return queryset


class FilterProfileBackend(filters.DjangoFilterBackend):
    class Meta:
        model = Profile
        fields = ['user']

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterProfileBackend, self).filter_queryset(request, queryset, view)
        filter_kwargs = {}
        user = request.QUERY_PARAMS.get('user', None)
        if user:
            try:
                filter_kwargs['user_id'] = int(user)
            except:
                filter_kwargs = {}
        if filter_kwargs:
            queryset = queryset.filter(**filter_kwargs)
        return queryset


class FilterPortfolioBackend(filters.DjangoFilterBackend):
    class Meta:
        model = Portfolio

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterPortfolioBackend, self).filter_queryset(request, queryset, view)
        filter_kwargs = {}
        user = request.QUERY_PARAMS.get('user', None)
        if user:
            try:
                filter_kwargs['teaching_subject__teacher_information__user_id'] = int(user)
            except:
                filter_kwargs = {}
        ts = request.QUERY_PARAMS.get('teaching_subject', None)
        if ts:
            try:
                filter_kwargs['teaching_subject_id'] = int(ts)
            except:
                filter_kwargs = filter_kwargs
        title = request.QUERY_PARAMS.get('title', None)
        if title:
            filter_kwargs['title'] = title
        if filter_kwargs:
            queryset = queryset.filter(**filter_kwargs)
        return queryset


class FilterCertificateBackend(FilterPortfolioBackend):
    class Meta:
        model = Certificate


class FilterWorkExperienceBackend(FilterPortfolioBackend):
    class Meta:
        model = WorkExperiance


class FilterTeachingExperienceBackend(FilterPortfolioBackend):
    class Meta:
        model = TeachingExperiance


class FilterUserBakend(filters.DjangoFilterBackend):

    class Meta:
        model = User

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterUserBakend, self).filter_queryset(request, queryset, view)

        if request.user and request.user.is_authenticated():
            my_teachers = request.QUERY_PARAMS.get('my_teachers', None)
            if my_teachers is not None:
                queryset = queryset.filter(list_members__list_owner=request.user)

        teachers = request.QUERY_PARAMS.get('teachers', None)
        if teachers is not None:
            queryset = queryset.filter(profile__is_teacher=True)

        return queryset
