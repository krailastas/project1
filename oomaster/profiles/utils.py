# encoding: utf-8
from __future__ import unicode_literals
from functools import wraps
import json
import re

from django.http import HttpResponseRedirect
from django.http import Http404
from django.views.decorators.http import require_http_methods
from django.core.urlresolvers import reverse
from social.exceptions import MissingBackend
from social.apps.django_app.utils import load_strategy

from .exceptions import (IncorrectProviderException, MissingParameterException)


def get_backend_name_by_state(request):
    state = None

    if request.method == 'GET':
        state = request.GET.get('state', None)
        if not state:
            raise MissingParameterException('state')
        try:
            json_state = json.loads(state)
        except ValueError:
            raise Http404('Wrong state')
        else:
            provider = json_state

    elif request.method == 'POST':
        provider = request.DATA
    if 'network' in provider:
        backend = provider['network']
        if not re.match(r'^[a-z\-0-9]{3,20}$', backend):
            raise IncorrectProviderException
    else:
        raise MissingParameterException('network')

    return {'state': state, 'backend_name': backend}


def strategy(redirect_uri=None, load_strategy=load_strategy):
    def decorator(func):
        @require_http_methods(['GET', 'POST'])
        @wraps(func)
        def wrapper(request, *args, **kwargs):
            provider = get_backend_name_by_state(request)

            uri = redirect_uri
            if uri and not uri.startswith('/'):
                client_id = request.GET.get('client_id', None)
                uri = reverse(redirect_uri, kwargs={})
                uri += '?client_id={0}&state={1}'.format(client_id if client_id else "", provider['state'])

            try:
                request.social_strategy = load_strategy(
                    request=request, backend=provider['backend_name'],
                    redirect_uri=uri, *args, **kwargs
                )
            except MissingBackend:
                raise Http404('Backend not found')

            # backward compatibility in attribute name, only if not already
            # defined
            if not hasattr(request, 'strategy'):
                request.strategy = request.social_strategy

            if 'code' in request.GET:
                backend = request.strategy.backend
                backend.process_error(backend.data)

                try:
                    data = backend.auth_complete_params(provider['state'])
                    data['redirect_uri'] = 'http://{0}/'.format(request.get_host())
                    response = backend.request_access_token(
                        backend.ACCESS_TOKEN_URL, data=data, headers=backend.auth_headers(),
                        method=backend.ACCESS_TOKEN_METHOD)
                except:
                    raise Http404

                backend.process_error(response)

                if len(response['access_token']) <= 3 or not isinstance(response['access_token'], basestring):
                    raise Http404('Wrong access token')

                return HttpResponseRedirect(
                    '/?access_token={0}&state={1}'.format(response['access_token'], provider['state']))
            else:
                return func(request, *args, **kwargs)

        return wrapper

    return decorator
