# encoding: utf-8
from __future__ import unicode_literals

from django.contrib.sites.models import get_current_site
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.template import loader
from django.core.mail import send_mail
from django.core.validators import EmailValidator
from django.core.exceptions import ValidationError

from rest_framework import serializers


class EMailSenderMixin(object):
    @classmethod
    def send_email(cls, request, user, subject_template_name, email_template_name, from_email, **c_dict):
        try:
            EmailValidator(user.email)
        except ValidationError:
            raise serializers.ValidationError("There's no email address to send a post.")

        current_site = get_current_site(request)
        c = {
            'email': user.email,
            'domain': current_site.domain,
            'site_name': current_site.name,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'protocol': 'https' if request.is_secure() else 'http',
        }
        c.update(**c_dict)
        subject = loader.render_to_string(subject_template_name, c)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        email = loader.render_to_string(email_template_name, c)
        send_mail(subject, email, from_email, [user.email])


class ProfileErrorMesageMixin(object):
    ERR_MSG = {
        'user_not_exists': 'User with this email does not exist.',
        'email_already_exists': 'User with that email already exists.',
        'username_already_exists': 'User with that username already exists.',
        'invalid_password': '8-16 chars, one number at least.',
        'invalid_email': 'Provided email is incorrect.',
        'invalid_username': 'Username must contain only letters, numbers and @/./+/-/_ characters.',
        'username_too_long': 'Username 30 characters or fewer.',
        'user_inactive': 'User account is disabled.',
        'cant_login': 'Email or password is wrong.',
        'email_and_password_required': 'Must provide user\'s email and password.',
        'different_passwords': "The two password fields didn't match.",
        'passwords_required': "You didn't provide both passwords",
        'user_is_active': 'User with email {} has already activated.',
        'invalid_activation_key': 'Activation key does not exist. Try resend activation key.',
        'current_wrong_password': 'Current password wrong.',
        'invalid_phone_number': 'Phone numbers must be in +{XXX} XXX XXX-XX-XX, XXX-XXX-XX-XX, or (XXX) XXX-XX-XX format or other phone format.'
    }
