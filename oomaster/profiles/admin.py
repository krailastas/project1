from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from south.migration import MigrationHistory

from .models import (User, Profile, StudySubject, WorkExperiance,
                     TeachingExperiance, TeacherInformation, Education,
                     TeachingSubject, Certificate, Portfolio, Image)


class ProfileAdmin(admin.ModelAdmin):
    search_fields = ('user__username', 'user__email')
    list_display = ('id', 'user', 'user_email', 'is_teacher', 'city')

    def user_full_name(self, obj):
        return obj.user.get_full_name()

    def user_email(self, obj):
        return obj.user.email


class UserAdmin(UserAdmin):
    list_display = ('id', 'username', 'email', 'get_full_name', 'is_active', 'is_staff')


admin.site.register(Profile, ProfileAdmin)
admin.site.register(MigrationHistory)
admin.site.register(StudySubject)
admin.site.register(WorkExperiance)
admin.site.register(TeachingExperiance)
admin.site.register(TeacherInformation)
admin.site.register(Education)
admin.site.register(TeachingSubject)
admin.site.register(Certificate)
admin.site.register(Portfolio)
admin.site.register(Image)
admin.site.register(User, UserAdmin)
