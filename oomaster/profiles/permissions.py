# encoding: utf-8
from __future__ import unicode_literals

from rest_framework.permissions import BasePermission, SAFE_METHODS


class EmailIsOwnerPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        return obj == request.user


class IsTeacherPermission(BasePermission):
    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated()

    def has_object_permission(self, request, view, obj):
        return obj.teacher_information.user == request.user


class IsTeacherInformationPermission(IsTeacherPermission):
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


class IsProfilePermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True
        return request.user and request.user.is_authenticated()

    def has_object_permission(self, request, view, obj):
        """
        Unauthorized user can see only teacher's page.
        Only owner can change account.
        """
        if request.method in SAFE_METHODS:
            if request.user and request.user.is_authenticated():
                return True
            return obj.is_teacher

        return request.user and request.user.is_authenticated() and (obj.user == request.user)


class IsTeachingSubjectPermission(IsTeacherPermission):
    def has_object_permission(self, request, view, obj):
        return obj.teaching_subject.teacher_information.user == request.user


class IsEducationPermission(IsTeacherPermission):
    def has_object_permission(self, request, view, obj):
        return obj.teacher_information.user == request.user


class IsUserPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        """
        Unauthorized user can see only teacher's page.
        Only owner can change account.
        """
        if request.method in SAFE_METHODS:
            if request.user and request.user.is_authenticated():
                return True
            return obj.profile.is_teacher

        return request.user and request.user.is_authenticated() and (obj.pk == request.user.pk)
