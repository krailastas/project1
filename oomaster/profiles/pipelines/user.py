# encoding: utf-8
from __future__ import unicode_literals
from profiles.models import User

USER_FIELDS = ['username', 'email']


def create_user(strategy, details, user=None, *args, **kwargs):
    if user:
        return {'is_new': False}

    fields = dict((name, kwargs.get(name) or details.get(name))
                  for name in strategy.setting('USER_FIELDS', USER_FIELDS))
    if not fields:
        return

    if 'email' in fields:
        if User.objects.filter(email=fields['email']).exists():
            fields['email'] = None
    return {
        'is_new': True,
        'user': strategy.create_user(**fields)
    }
