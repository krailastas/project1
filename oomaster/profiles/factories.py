# encoding: utf-8
from __future__ import unicode_literals

import random
import factory

from django.contrib.auth.hashers import make_password
from django.contrib.webdesign import lorem_ipsum
from django.db.models import signals

from .models import User
from profiles.models import Profile, create_profile, TeacherInformation,\
    StudySubject, Certificate, TeachingSubject,\
    Portfolio, WorkExperiance, TeachingExperiance, Education
from cities_light.models import Country, Region


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User
        django_get_or_create = ('username', )

    @factory.lazy_attribute
    def username(self):
        return lorem_ipsum.words(1, False) + str(random.choice(range(100, 999))) + '_' + str(random.choice(range(100, 999)))

    @factory.lazy_attribute
    def first_name(self):
        return lorem_ipsum.words(1, False)

    @factory.lazy_attribute
    def last_name(self):
        return lorem_ipsum.words(1, False)

    @factory.lazy_attribute
    def email(self):
        return u'{}@example.com'.format(self.username)

    is_staff = False

    password = make_password('password')


class RegionFactory(factory.DjangoModelFactory):
    class Meta:
        model = Region

    @factory.lazy_attribute
    def name(self):
        return lorem_ipsum.words(4, False)

    @factory.lazy_attribute
    def display_name(self):
        return lorem_ipsum.words(4, False)

    @factory.lazy_attribute
    def country(self):
        return random.choice(Country.objects.all())


class UserStaffFactory(UserFactory):
    is_staff = True


class ProfileFactory(factory.DjangoModelFactory):
    class Meta:
        model = Profile

    @factory.lazy_attribute
    def user(self):
        signals.post_save.disconnect(create_profile, sender=User)
        user = UserFactory.create()
        signals.post_save.connect(create_profile, sender=User)
        return user

    @factory.lazy_attribute
    def bio(self):
        return lorem_ipsum.words(20, False)

    is_teacher = False

    @factory.lazy_attribute
    def gender(self):
        return random.choice(['Male', 'Female'])

    @factory.lazy_attribute
    def phone_number(self):
        return '{}-{}'.format(random.choice(range(10000, 99999)))


class ProfileTeacherFactory(ProfileFactory):
    is_teacher = True


class TeacherInformationFactory(factory.DjangoModelFactory):
    class Meta:
        model = TeacherInformation

    @factory.lazy_attribute
    def user(self):
        return UserFactory.create()


class StudySubjectFactory(factory.DjangoModelFactory):
    class Meta:
        model = StudySubject

    @factory.lazy_attribute
    def title(self):
        return lorem_ipsum.words(3, False)


class CertificateFactory(factory.DjangoModelFactory):
    class Meta:
        model = Certificate

    @factory.lazy_attribute
    def teaching_subject(self):
        return random.choice(TeachingSubject.objects.all())

    @factory.lazy_attribute
    def title(self):
        return lorem_ipsum.words(2, False)


class PortfolioFactory(factory.DjangoModelFactory):
    class Meta:
        model = Portfolio

    @factory.lazy_attribute
    def teaching_subject(self):
        return random.choice(TeachingSubject.objects.all())

    @factory.lazy_attribute
    def title(self):
        return lorem_ipsum.words(2, False)


class TeachingSubjectFactory(factory.DjangoModelFactory):
    class Meta:
        model = TeachingSubject

    @factory.lazy_attribute
    def teacher_information(self):
        return TeacherInformationFactory.create()

    @factory.lazy_attribute
    def study(self):
        return StudySubjectFactory.create()


class EducationFactory(factory.DjangoModelFactory):
    class Meta:
        model = Education

    @factory.lazy_attribute
    def teacher_information(self):
        return random.choice(TeacherInformation.objects.all())

    @factory.lazy_attribute
    def title(self):
        return lorem_ipsum.words(4, False)


class WorkExperianceFactory(factory.DjangoModelFactory):
    class Meta:
        model = WorkExperiance

    @factory.lazy_attribute
    def teaching_subject(self):
        return random.choice(TeachingSubject.objects.all())

    @factory.lazy_attribute
    def how_long(self):
        return random.choice(['less half year', '0.5-1', '2', '5', '10', ])

    @factory.lazy_attribute
    def title(self):
        return lorem_ipsum.words(2, False)


class TeachingExperianceFactory(WorkExperianceFactory):
    class Meta:
        model = TeachingExperiance
