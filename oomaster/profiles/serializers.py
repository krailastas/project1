# encoding: utf-8
from __future__ import unicode_literals
import re
from datetime import datetime

from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.models import BaseUserManager
from django.core.validators import EmailValidator
from django.core.exceptions import ValidationError
from django.contrib.auth.tokens import default_token_generator
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from rest_framework import serializers
from rest_framework.authtoken.models import Token

from .mixins import EMailSenderMixin, ProfileErrorMesageMixin
from .models import Profile
from course.models import Review
from profiles.models import TeacherInformation, Education, StudySubject,\
    TeachingSubject, Certificate, Portfolio, WorkExperiance, TeachingExperiance,\
    Image
from notification.models import DeviceTokenNotification


UserModel = get_user_model()


class AuthSerializer(ProfileErrorMesageMixin, serializers.Serializer):
    """
    **Auth Serializer**
        **fields:**

        * `email`
        * `password`
        * `deviceType`
        * `deviceToken`

        **returns:**

        * `token`
        * `user_id`
    """

    email = serializers.CharField(max_length=254)
    password = serializers.CharField(max_length=128)
    deviceType = serializers.CharField(max_length=254, required=False)
    deviceToken = serializers.CharField(max_length=254, required=False)

    def validate(self, attrs):
        """
            Email and password are required. User with such email should exist.
        """

        email = attrs.get('email')
        password = attrs.get('password')
        deviceType = attrs.get('deviceType')
        deviceToken = attrs.get('deviceToken')

        if email and password:
            credentials = {'password': password}

            try:
                EmailValidator(email)
            except ValidationError:
                pass
            else:
                try:
                    username = UserModel.objects.get(email=email).username
                except UserModel.DoesNotExist:
                    raise serializers.ValidationError(_(self.ERR_MSG['user_not_exists']))

            credentials.update({'username': username})

            user = authenticate(**credentials)

            if user:
                if deviceType and deviceToken:
                    if not DeviceTokenNotification.objects.filter(deviceToken=deviceToken, user=user).exists():
                        DeviceTokenNotification.objects.create(user=user, deviceToken=deviceToken, deviceType=deviceType)
                if user.is_active is False:
                    raise serializers.ValidationError(_(self.ERR_MSG['user_inactive']))
            else:
                raise serializers.ValidationError(_(self.ERR_MSG['cant_login']))
        else:
            raise serializers.ValidationError(_(self.ERR_MSG['email_and_password_required']))

        # Retrieving token for user
        token, _created = Token.objects.get_or_create(user=user)

        return {'token': token.key, 'user_id': user.id}


class PasswordResetSerializer(ProfileErrorMesageMixin, EMailSenderMixin, serializers.Serializer):
    """
    **Password Reset Serializer**
        It's used for recovering user's password when it's needed.
        Special URL for password recovery will be sent to user's email.

        **fields:**

        * `email`

        **returns:**

        * `token`
        * `user_id`
    """

    email = serializers.EmailField(max_length=254)

    def validate_email(self, attrs, source):
        """
            Check whether user’s email is valid and exists in database.
        """

        email = attrs[source] = BaseUserManager.normalize_email(attrs[source])

        try:
            EmailValidator(email)
        except ValidationError:
            raise serializers.ValidationError(_(self.ERR_MSG['invalid_email']))

        if not UserModel.objects.filter(email=email).exists():
            raise serializers.ValidationError(_(self.ERR_MSG['user_not_exists']))
        return attrs

    def save(self, **kwargs):
        """
            After validating data which is called by "is_valid" method send reset link to user's email.
        """

        subject_template_name = 'profiles/password_reset_subject.txt'
        email_template_name = 'profiles/password_reset_email.html'

        email = self.object['email']
        from_email = kwargs['from_email']

        try:
            user = UserModel.objects.get(email=email)
            self.send_email(self.context['request'], user, subject_template_name, email_template_name, from_email, **{
                'token': default_token_generator.make_token(user)
            })
        except UserModel.DoesNotExist:
            raise serializers.ValidationError(_('Provided user\'s email does not exist.'))


class PasswordResetCompleteSerializer(ProfileErrorMesageMixin, serializers.Serializer):
    """
    **Password Reset Complete Serializer**
        Finishing password reset. User must enter new password and confirm it.

        **fields:**

        * `password`
        * `confirm_password`
    """

    password = serializers.CharField(max_length=128)
    confirm_password = serializers.CharField(max_length=128)

    def validate(self, attrs):
        """
            Both passwords are required and must be equal.
        """
        password = attrs.get('password')
        confirm_password = attrs.get('confirm_password')

        if password and confirm_password:
            if password != confirm_password:
                raise serializers.ValidationError(_(self.ERR_MSG['different_passwords']))
        else:
            raise serializers.ValidationError(_(self.ERR_MSG['passwords_required']))
        return attrs

    def save(self, **kwargs):
        password = self.object['password']
        user = self.context['user']
        user.set_password(password)
        user.save()


class PasswordChangeSerializer(ProfileErrorMesageMixin, serializers.Serializer):
    current_password = serializers.CharField(max_length=128)
    new_password = serializers.CharField(max_length=128)
    confirm_new_password = serializers.CharField(max_length=128)

    def validate(self, attrs):
        username = self.context['request'].user.username
        current_password = attrs.get('current_password')

        user = authenticate(username=username, password=current_password)
        if user:
            new_password = attrs.get('new_password')
            confirm_new_password = attrs.get('confirm_new_password')

            if new_password and confirm_new_password:
                if new_password != confirm_new_password:
                    raise serializers.ValidationError(_(self.ERR_MSG['different_passwords']))
            else:
                raise serializers.ValidationError(_(self.ERR_MSG['passwords_required']))
            return attrs
        else:
            raise serializers.ValidationError(_(self.ERR_MSG['current_wrong_password']))

    def save(self, **kwargs):
        password = self.object['new_password']
        user = self.context['user']
        user.set_password(password)
        user.save()


class RegistrationSerializer(ProfileErrorMesageMixin, EMailSenderMixin, serializers.Serializer):
    """
    **Registration Serializer**
        Create new inactive user and send him email with activation link.

        **fields:**

        * `email`
        * `username`
        * `password`
        * `deviceType`
        * `deviceToken`
    """
    email = serializers.EmailField(max_length=254)
    username = serializers.CharField(max_length=35)
    password = serializers.CharField(max_length=128)
    deviceType = serializers.CharField(max_length=254, required=False)
    deviceToken = serializers.CharField(max_length=254, required=False)

    def validate(self, attrs):
        """
            Email, username and password are required fields. Username must be valid and unique.
            Email must be valid and unique too. Password must contain at least 8 characters
            and at least one of them must be number.
        """
        username = attrs.get('username')
        email = attrs.get('email')
        password = attrs.get('password')

        try:
            EmailValidator(email)
        except ValidationError:
            raise serializers.ValidationError(_(self.ERR_MSG['invalid_email']))

        if UserModel.objects.filter(email=email).exists():
            raise serializers.ValidationError(_(self.ERR_MSG['email_already_exists']))

        if UserModel.objects.filter(username=username).exists():
            raise serializers.ValidationError(_(self.ERR_MSG['username_already_exists']))

        if len(username) > 30:
            raise serializers.ValidationError(_(self.ERR_MSG['username_too_long']))

        password_regex = re.compile("^(?=.*\d)(?=.*[a-zA-z]).{8,16}$")
        if not password_regex.match(password):
            raise serializers.ValidationError(_(self.ERR_MSG['invalid_password']))

        username_regex = re.compile("^[\w.@+-]+$")
        if not username_regex.match(username):
            raise serializers.ValidationError(_(self.ERR_MSG['invalid_username']))

        return attrs

    def save(self, **kwargs):

        username, email, password = self.object['username'], self.object['email'], self.object['password']

        user = UserModel._default_manager.create_inactive_user(username, email, password)

        subject_template_name = 'profiles/activation_email_subject.txt'
        email_template_name = 'profiles/activation_email.txt'

        self.send_email(self.context['request'], user, subject_template_name, email_template_name, None, **{
            'activation_key': user.activation_key
        })

        # Creating token for user
        Token.objects.get_or_create(user=user)
        self.object = user


class ResendActivationKeySerializer(ProfileErrorMesageMixin, EMailSenderMixin, serializers.Serializer):
    """
    **Resend Activation Key Serializer**
        Resend mail with activation link to user's email.

        **fields:**

        * `email`
    """

    email = serializers.EmailField(max_length=254)

    def validate(self, attrs):
        """
            Inactive user with specified email should exist.
        """
        email = attrs.get('email')

        try:
            EmailValidator(email)
        except ValidationError:
            raise serializers.ValidationError(_(self.ERR_MSG['invalid_email']))

        try:
            user = UserModel.objects.get(email=email)
        except UserModel.DoesNotExist:
            raise serializers.ValidationError(_(self.ERR_MSG['user_not_exists']))

        if user.is_active:
            raise serializers.ValidationError(_(self.ERR_MSG['user_is_active'].format(email)))

        return attrs

    def save(self, **kwargs):
        email = self.object['email']
        user = UserModel.objects.get(email=email)
        activation_key = UserModel.objects._generate_activation_key(user.username)

        user.activation_key = activation_key
        user.activation_key_created = datetime.today().date()
        user.save()

        subject_template_name = 'profiles/activation_email_subject.txt'
        email_template_name = 'profiles/activation_email.txt'

        self.send_email(
            request=self.context['request'],
            user=user,
            subject_template_name=subject_template_name,
            email_template_name=email_template_name,
            from_email=settings.DEFAULT_FROM_EMAIL,
            **{'activation_key': user.activation_key}
        )

        # Creating token for user
        Token.objects.get_or_create(user=user)
        self.object = user


class ActivationSerializer(ProfileErrorMesageMixin, serializers.Serializer):
    """
    **Activation Serializer**
        Serializer sets user as an active user.

        **fields:**

        * `activation_key`
    """
    activation_key = serializers.CharField(max_length=40)

    def validate_activation_key(self, attrs, source):
        """
            Check existence of activation keys.
        """
        activation_key = attrs.get(source)

        if not UserModel.objects.check_activation_key(activation_key):
            raise serializers.ValidationError(_(self.ERR_MSG['invalid_activation_key']))

        return attrs

    def save(self, **kwargs):
        if not UserModel.objects.activate_user(self.object['activation_key']):
            raise serializers.ValidationError(_(self.ERR_MSG['invalid_activation_key']))


class EmailSerializer(ProfileErrorMesageMixin, serializers.ModelSerializer):
    """
    **Email Serializer**
        **fields:**

        * `email`
    """
    class Meta:
        model = UserModel
        fields = ('email',)

    def validate(self, attrs):
        """
            Email must be valid and unique.
        """
        email = attrs['email']

        try:
            EmailValidator(email)
        except ValidationError:
            raise serializers.ValidationError(_(self.ERR_MSG['invalid_email']))

        if UserModel.objects.filter(email=email).exists():
            raise serializers.ValidationError(_(self.ERR_MSG['email_already_exists']))

        return attrs


class ImageSerializer(serializers.ModelSerializer):
    """
    **Images Serializer**
        Serializer for Images model.

        **fields:**

        * `id`
        * `image`
    """
    class Meta:
        model = Image
        fields = ('id', 'image',)


class ProfileSerializer(serializers.ModelSerializer):
    """
    **Profile Serializer**
        Serializer for Profile model.

        **fields:**

        * `user`
        * `bio`
        * `gender`
        * `country`
        * `region`
        * `city`
        * `date_birthday`
        * `is_teacher`
        * `phone_number`
        * `avatar`
    """
    class Meta:
        model = Profile
        read_only_fields = ('user', )

    def validate_phone_number(self, attrs, source):
        phone_number = attrs.get(source)
        if phone_number:
            phone_digits = re.compile(r'^((8|\+\d{1,3})[\- ]?)?(\(?\d{1,8}\)?[\- ]?)?[\d\- ]{5,10}$')
            match = phone_digits.match(phone_number)
            if not match:
                raise serializers.ValidationError(_(self.ERR_MSG['invalid_phone_number']))
        return attrs


class TeacherInformationSerializer(serializers.ModelSerializer):
    """
    **TeacherInformation Serializer**
        Serializer for TeacherInformation model.
        **fields: all TeacherInformation's fields**
    """
    class Meta:
        model = TeacherInformation
        read_only_fields = ('user', 'file')
        depth = 1


class EducationSerializer(serializers.ModelSerializer):
    """
    **Education Serializer**
        Serializer for Education model.
        **fields: all Education's fields**
    """
    class Meta:
        model = Education


class StudySubjectSerializer(serializers.ModelSerializer):
    """
    **StudySubject Serializer**
        Serializer for StudySubject model.
        **fields: all StudySubject's fields**
    """
    class Meta:
        model = StudySubject


class TeachingSubjectSerializer(serializers.ModelSerializer):
    """
    **TeachingSubject Serializer**
        Serializer for TeachingSubject model.
        **fields: all TeachingSubject's fields**
    """
    class Meta:
        model = TeachingSubject


class CertificateSerializer(serializers.ModelSerializer):
    """
    **Certificate Serializer**
        Serializer for Certificate model.
        **fields: all Certificate's fields**
    """
    class Meta:
        model = Certificate


class UserSerializer(serializers.ModelSerializer):
    """
    **User Serializer**
        Serializer for User model.
        **fields:**

        * `id`
        * `username`
        * `email`
        * `profile`
        * `teacher_info`
        * `is_followed`
        * `first_name`
        * `last_name`
    """
    is_followed = serializers.SerializerMethodField('check_is_followed')
    is_my_student = serializers.SerializerMethodField('check_is_my_student')
    rating = serializers.SerializerMethodField('get_rating')

    class Meta:
        model = UserModel
        fields = (
            'id', 'username', 'profile', 'email',
            'teacher_info', 'last_name', 'first_name',
            'rating', 'is_my_student', 'is_followed'
        )
        read_only_fields = ('profile', 'teacher_info')
        depth = 1

    def check_is_my_student(self, obj):
        user = self.context['request'].user
        if user.is_authenticated() and user.profile.is_teacher:
            all_user_sdudents = user.th_courses.all().values_list('students', flat=True)
            return obj.id in all_user_sdudents
        return None

    def check_is_followed(self, obj):
        if self.context['request'].user.is_authenticated():
            return obj in self.context['request'].user.get_followed_teachers()
        return None

    def get_rating(self, obj):
        if obj.profile.is_teacher:
            ratings = Review.objects.filter(course__teacher=obj).values_list('rating', flat=True)
            if ratings:
                return round(sum(ratings) / float(len(ratings)), 2)
        return None


class PortfolioSerializer(serializers.ModelSerializer):
    """
    **Portfolio Serializer**
        Serializer for Portfolio model.
        **fields: all Portfolio's fields**
    """
    class Meta:
        model = Portfolio


class TeachingExperianceSerializer(serializers.ModelSerializer):
    """
    **TeachingExperiance Serializer**
        Serializer for TeachingExperiance model.
        **fields: all TeachingExperiance's fields**
    """
    class Meta:
        model = TeachingExperiance


class WorkExperianceSerializer(serializers.ModelSerializer):
    """
    **WorkExperiance Serializer**
        Serializer for WorkExperiance model.
        **fields: all WorkExperiance's fields**
    """
    class Meta:
        model = WorkExperiance
