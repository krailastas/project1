
from __future__ import unicode_literals

from collections import OrderedDict

from rest_framework.decorators import APIView
from rest_framework.response import Response
from rest_framework.reverse import reverse

from course.models import Course


class APIRoot(APIView):
    """
    ## This is the root of the API.

    It includes hyperlinks to the various other endpoints in order to
    make the API easier to browse.
    """
    permission_classes = ()

    def get(self, request):
        if not request.user.is_authenticated():
            return Response(OrderedDict([
                ('user', {
                    'register-user': reverse('register-user', request=request),
                    'password-reset': reverse('password-reset', request=request),
                    'login': reverse('user-login', request=request),
                    'resend-activation-key': reverse('resend-activation-key', request=request),
                }), ]))
        urls_list = [
            ('users', {
                'users-list-url': reverse('users-list', request=request),
                'studies-list-url': reverse('study-list', request=request),
                'teacher-information-list-url': reverse('teacher_information-list', request=request),
                'teacher-subject-list-url': reverse('teacher_subject-list', request=request),
                'profile-list-url': reverse('profile-list', request=request),
                'country-list-url': reverse('country-list', request=request),
                'city-list-url': reverse('city-list', request=request),
                'certificate-list-url': reverse('certificate-list', request=request),
                'portfolio-list-url': reverse('portfolio-list', request=request),
                'teaching-experience-list-url': reverse('teaching_experience-list', request=request),
                'education-list-url': reverse('education-list', request=request),
                'work-experience-list-url': reverse('work_experience-list', request=request),
                'password-change': reverse('password-change', request=request),
                'images-list-url': reverse('images-list', request=request),
            }),
            ('flat', {
                'flat-page-list-url': reverse('flat-list', request=request),
            }),
            ('notification_settings', {
                'notification-settings-list-url': reverse('notification_settings-list', request=request),
                'notification-user-settings-list-url': reverse('notification_user_settings-list', request=request),
            }),
            ('course', {
                'currencies-url': reverse('currencies-list', request=request),
                'attached_files-url': reverse('attached_files-list', request=request),
                'courses-list-url': reverse('courses-list', request=request),
                'classes-list-url': reverse('classes-list', request=request),
                'notes-list-url': reverse('notes-list', request=request),
                'messages-list-url': reverse('messages-list', request=request),
                'teacher_in_teacher_list-list-url': reverse('teacher_in_teacher_list-list', request=request),
                'teacher_invitation-list-url': reverse('teacher_invitation-list', request=request),
                'followed_teacher-list-url': reverse('followed_teacher-list', request=request),
                'reviews-list-url': reverse('reviews-list', request=request),
                'wishlists-list-url': reverse('wishlists-list', request=request),
                'course_attache_files-list-url': reverse('course_attache_files-list', request=request),
            }),
            ('course request', {
                'course-request-list-url': reverse('course_request-list', request=request),
                'course-request-like-list-url': reverse('course_request_like-list', request=request),
                'course-request-message-list-url': reverse('course_request_message-list', request=request),
            }),
            ('notification', {
                'notification-list-url': reverse('notifications-list', request=request),
                'notification-user-list-url': reverse('notification_user-list', request=request),
                'notification-course-students': reverse('notification-course-students', request=request),
            }),
            ('earning', {
                'balances-list-url': reverse('balances-list', request=request),
                'course-balance-list-url': reverse('course_earning-list', request=request),
                'month-balance-list-url': reverse('month-list', request=request),
                'withdrawals-list-url': reverse('withdrawals-list', request=request),
            }),
            ('geo ip', {
                'location-list-url': reverse('location-list', request=request),
            }),
        ]

        course = Course.objects.first()
        if course:
            urls_list.append(('payment', {
                'get-course-paypal-buy-button': reverse('get-course-paypal-buy-button', kwargs={'course_id': course.id}, request=request),
            }))

        return Response(OrderedDict(urls_list))


api_root = APIRoot.as_view()


class ListMixinView(object):
    def list(self, request, *args, **kwargs):
        if 'qs' not in kwargs:
            raise Exception('Queryset is not passed')

        self.object_list = self.filter_queryset(kwargs['qs'])

        # Default is to allow empty querysets.  This can be altered by setting
        # `.allow_empty = False`, to raise 404 errors on empty querysets.
        if not self.allow_empty and not self.object_list:
            warnings.warn(
                'The `allow_empty` parameter is due to be deprecated. '
                'To use `allow_empty=False` style behavior, You should override '
                '`get_queryset()` and explicitly raise a 404 on empty querysets.',
                PendingDeprecationWarning
            )
            class_name = self.__class__.__name__
            error_msg = self.empty_error % {'class_name': class_name}
            raise Http404(error_msg)

        # Switch between paginated or standard style responses
        page = self.paginate_queryset(self.object_list)
        if page is not None:
            serializer = self.get_pagination_serializer(page)
        else:
            serializer = self.get_serializer(self.object_list, many=True)

        return Response(serializer.data)