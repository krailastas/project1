from __future__ import unicode_literals, absolute_import
from django import template
from django.core.urlresolvers import reverse, NoReverseMatch
from django.utils.html import escape

register = template.Library()


@register.simple_tag
def common_optional_login(request):
    """
    Include a login snippet if REST framework's login view is in the URLconf.
    """
    try:
        login_url = reverse('login')
    except NoReverseMatch:
        return ''

    snippet = "<a href='%s?next=%s'>Log in</a>" % (login_url, escape(request.path))
    return snippet


@register.simple_tag
def common_optional_logout(request):
    """
    Include a logout snippet if REST framework's logout view is in the URLconf.
    """
    try:
        logout_url = reverse('logout')
    except NoReverseMatch:
        return ''

    snippet = "<a href='%s?next=%s'>Log out</a>" % (logout_url, escape(request.path))
    return snippet

