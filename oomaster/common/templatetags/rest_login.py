from __future__ import unicode_literals, absolute_import
from django import template
from django.core.urlresolvers import reverse, NoReverseMatch
from django.utils.html import escape

register = template.Library()


@register.simple_tag
def rest_optional_login(request):
    try:
        login_url = reverse('login')
    except NoReverseMatch:
        return ''
    snippet = '<a href="{0}?next={1}">Log in</a>'.format(login_url, escape(request.path))
    return snippet


@register.simple_tag
def rest_optional_logout(request):
    try:
        logout_url = reverse('logout')
    except NoReverseMatch:
        return ''
    snippet = '<a href="{0}?next={1}">Log out</a>'.format(logout_url, escape(request.path))
    return snippet
