import httplib
import json
import time

from django.conf import settings
from django.utils import timezone

from templated_email import send_templated_mail
from celery.app.task import Task
from celery.task.base import periodic_task
from constance import config

from profiles.models import Image
from celery.schedules import crontab
from earning.models import ItemBalance


class SendMailTask(Task):
    def send_mail(self, instance, ctx_dict):
        send_templated_mail(
            template_name='notification_user',
            from_email=settings.DEFAULT_FROM_EMAIL,
            recipient_list=[instance.user.email],
            context=ctx_dict,
        )
        instance.is_send = True
        instance.save()

    def run(self, *args, **kwargs):
        self.send_mail(*args, **kwargs)


class ParseNotificationTask(Task):
    def push_notification(self, instans, dict):
        connection = httplib.HTTPSConnection('api.parse.com', 443)
        connection.connect()
        connection.request('POST', '/1/installations', json.dumps({
            'deviceType': dict.get('deviceType'),
            'deviceToken': dict.get('deviceToken'),
            'data': {
                'alert': dict.get('alert')
            }
        }),
                           {
                               "X-Parse-Application-Id": config.X_Parse_Application_Id,
                               "X-Parse-REST-API-Key": config.X_Parse_REST_API_Key,
                               "Content-Type": "application/json"
                           })

    def run(self, *args, **kwargs):
        self.push_notification(*args, **kwargs)


class CourseRequestTask(Task):
    def update_request(self, *args, **kwargs):
        course = args[0]
        time.sleep(10)
        course.is_pre_active = False
        course.save()

    def run(self, *args, **kwargs):
        self.update_request(*args, **kwargs)


@periodic_task(run_every=crontab(hour='4', minute=0))
def delete_image():
    start_current_day = timezone.now().replace(hour=0, minute=0, second=0, microsecond=0)
    Image.objects.filter(is_active=False, created__lt=start_current_day).delete()


@periodic_task(run_every=crontab(hour='3', minute=0))
def update_earning():
    for item in ItemBalance.objects.filter(course__date_end__lte=timezone.now(), is_active=False):
        item.allowed_withdrawal = item.earnings
        item.is_active = True
        item.save()
