# encoding: utf-8
from __future__ import unicode_literals

import json
import httplib

from django.core.management.base import BaseCommand
from constance import config


class Command(BaseCommand):
    def handle(self, *args, **options):
        connection = httplib.HTTPSConnection('api.parse.com', 443)
        connection.connect()
        connection.request('GET', '/1/installations', '', {
            "X-Parse-Application-Id": config.X_Parse_Application_Id,
            "X-Parse-REST-API-Key": config.X_Parse_REST_API_Key
        })
        result = json.loads(connection.getresponse().read())
        print result