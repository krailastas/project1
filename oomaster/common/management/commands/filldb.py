# encoding: utf-8
from __future__ import unicode_literals

from django.conf import settings
from django.core.management.base import BaseCommand
from django.contrib.sites.models import Site

from profiles.factories import CertificateFactory, TeachingSubjectFactory, \
    PortfolioFactory, WorkExperianceFactory, TeachingExperianceFactory, \
    EducationFactory, RegionFactory
from flat.factories import FlatPagePublishedFactory, FlatPageFactory
from notification_settings.factories import NotificationSettingsActiveFactory
from custom_cities_light.factories import CountryFactory, CityFactory
from course.factories import CourseFactory
from notification.factories import NotificationFactory
from earning.factories import ItemBalanceFactory, BalanceFactory, \
    WithdrawalFactory
from course_request.factories import CourseRequestFactory, MessageRequestFactory


class Command(BaseCommand):
    def handle(self, *args, **options):
        site = Site.objects.get(pk=1)
        site.domain = site.name = settings.DOMAIN
        site.save()
        FlatPagePublishedFactory.create_batch(30)
        FlatPageFactory.create_batch(30)
        CountryFactory.create_batch(30)
        RegionFactory.create_batch(30)
        CityFactory.create_batch(30)
        TeachingSubjectFactory.create_batch(30)
        WorkExperianceFactory.create_batch(30)
        TeachingExperianceFactory.create_batch(30)
        CertificateFactory.create_batch(10)
        PortfolioFactory.create_batch(10)
        EducationFactory.create_batch(10)
        NotificationSettingsActiveFactory.create_batch(3)
        CourseFactory.create_batch(10)
        NotificationFactory.create_batch(15)
        BalanceFactory.create_batch(2)
        ItemBalanceFactory.create_batch(5)
        WithdrawalFactory.create_batch(10)
        CourseRequestFactory.create_batch(20)
        MessageRequestFactory.create_batch(20)