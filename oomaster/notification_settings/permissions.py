# encoding: utf-8
from __future__ import unicode_literals

from rest_framework.permissions import IsAuthenticated


class IsNotificationUserSettingsPermission(IsAuthenticated):
    def has_object_permission(self, request, view, obj):
        return obj.user == request.user
