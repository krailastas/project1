# encoding: utf-8
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


class NotificationSettingsManager(models.Manager):
    def get_query_set(self):
        return super(NotificationSettingsManager, self).get_query_set().filter(is_active=True)


class NotificationSettings(models.Model):
    """
    **NotificationSettings model**

        **list fields:**

        * `name` - CharField
        * `slug` - SlugField
        * `type` - SmallIntegerField
        * `is_choice` - BooleanField
        * `is_active` - BooleanField
    """
    TYPE_CHOICES = (
        (0, _('web')),
        (1, _('mob')),
    )
    name = models.CharField(verbose_name=_('name'), max_length=255)
    slug = models.SlugField(verbose_name=_('slug'), unique=True)
    type = models.SmallIntegerField(_('type'), choices=TYPE_CHOICES, default=0)
    is_choice = models.BooleanField(verbose_name=_('choice'), default=True)
    is_active = models.BooleanField(verbose_name=_('active'), default=False)

    objects = models.Manager()
    activated = NotificationSettingsManager()

    class Meta:
        verbose_name = _('Notification setting')
        verbose_name_plural = _('Notification settings')

    def __str__(self):
        return self.name


class NotificationUserSettings(models.Model):
    """
    **NotificationUserSettings model**

        **list fields:**

        - `user` - ForeignKey - profiles.User
        - `notification` - ForeignKey - NotificationSettings
        - `mode` - BooleanField
    """
    user = models.ForeignKey('profiles.User', related_name='to_users', verbose_name=_('to user'))
    notification = models.ForeignKey('NotificationSettings', related_name='notifications', verbose_name=_('notification setting'))
    mode = models.BooleanField(verbose_name=_('mode'))

    class Meta:
        verbose_name = _('Notification user setting')
        verbose_name_plural = _('Notification user settings')
        unique_together = ('user', 'notification')
