# encoding: utf-8
from __future__ import unicode_literals

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from notification_settings.factories import NotificationSettingsFactory,\
    NotificationSettingsActiveFactory
from notification_settings.models import NotificationSettings,\
    NotificationUserSettings
from profiles.factories import UserFactory


class TestNotificationSettingsViewSet(APITestCase):
    url = reverse('notification_settings-list')

    def setUp(self):
        super(TestNotificationSettingsViewSet, self).setUp()
        NotificationSettingsFactory.create_batch(5)
        NotificationSettingsActiveFactory.create_batch(5)
        self.user = UserFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_post(self):
        data = {
            'name': 'name',
            'slug': 'slug',
            'type': 1,
            'is_active': True,
            'is_choice': True,
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)


class TestNotificationUserSettingsViewSet(APITestCase):
    url = reverse('notification_user_settings-list')

    def setUp(self):
        super(TestNotificationUserSettingsViewSet, self).setUp()
        NotificationSettingsActiveFactory.create_batch(5)
        NotificationSettingsFactory.create_batch(5)
        self.user = UserFactory.create()

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        active = 0
        for ns in NotificationSettings.objects.all():
            if ns.is_active:
                self.assertContains(response, ns.id)
                active += 1
        self.assertEqual(active, 16)  # add from fixtures

    def test_post(self):
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_delete(self):
        nus = NotificationUserSettings.objects.all().last()
        pk = nus.pk
        url = reverse('notification_user_settings-detail', kwargs={'pk': pk})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_put(self):
        nus = NotificationUserSettings.objects.all().last()
        pk = nus.pk
        url = reverse('notification_user_settings-detail', kwargs={'pk': pk})
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.force_authenticate(self.user)
        response = self.client.put(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
