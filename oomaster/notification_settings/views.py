# encoding: utf-8
from __future__ import unicode_literals

from django.core.exceptions import ValidationError

from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ReadOnlyModelViewSet, GenericViewSet
from rest_framework.mixins import UpdateModelMixin, ListModelMixin,\
    RetrieveModelMixin

from notification_settings.models import NotificationSettings,\
    NotificationUserSettings
from notification_settings.serializers import NotificationSettingsSerializer,\
    NotificationUserSettingsSerializer
from notification_settings.filters import FilterNotificationUserSettingsBackend,\
    FilterNotificationSettingsBackend
from notification_settings.permissions import IsNotificationUserSettingsPermission


class NotificationSettingView(ReadOnlyModelViewSet):
    """
    **NotificationSetting View**

        Return list of notification settings

        **returns:**

        * `id`
        * `name`
        * `slug`
        * `type`

        **filters:**

        * `?slug=<text>`
    """
    queryset = NotificationSettings.activated.all()
    serializer_class = NotificationSettingsSerializer
    permission_classes = (IsAuthenticated,)
    filter_backends = (FilterNotificationSettingsBackend,)


class NotificationUserSettingsView(UpdateModelMixin, RetrieveModelMixin, ListModelMixin, GenericViewSet):
    """
    **NotificationUserSettings View**

        Return list of notification settings of user

        **returns:**

        * `id`
        * `user`
        * `notification`
        * `mode`

        **filters:**

        * `?mode=[True|False]`
        * `?user=<pk>`
    """
    serializer_class = NotificationUserSettingsSerializer
    queryset = NotificationUserSettings.objects.select_related()
    permission_classes = (IsNotificationUserSettingsPermission,)
    filter_backends = (FilterNotificationUserSettingsBackend,)

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        self.object = self.get_object_or_none()
        serializer = self.get_serializer(self.object, data=request.DATA,
                                         files=request.FILES, partial=partial)
        if not serializer.is_valid():
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        try:
            self.pre_save(serializer.object)
        except ValidationError as err:
            return Response(err.message_dict, status=status.HTTP_400_BAD_REQUEST)
        try:
            self.object = serializer.save(force_update=True)
            self.post_save(self.object, created=False)
        except:
            return Response('Error, user can not have duplicate settings', status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.data, status=status.HTTP_200_OK)
