# encoding: utf-8
from __future__ import unicode_literals

import random
import factory

from django.contrib.webdesign import lorem_ipsum

from pytils.translit import slugify

from notification_settings.models import NotificationSettings


class NotificationSettingsFactory(factory.DjangoModelFactory):
    class Meta:
        model = NotificationSettings

    @factory.lazy_attribute
    def name(self):
        return lorem_ipsum.words(3, False)

    @factory.lazy_attribute
    def slug(self):
        return slugify(self.name)

    @factory.lazy_attribute
    def type(self):
        return random.choice([0, 1])

    @factory.lazy_attribute
    def is_choice(self):
        return random.choice([True, False])


class NotificationSettingsActiveFactory(NotificationSettingsFactory):
    is_active = True
