# encoding: utf-8
from __future__ import unicode_literals

from rest_framework import serializers

from notification_settings.models import NotificationSettings,\
    NotificationUserSettings


class NotificationSettingsSerializer(serializers.ModelSerializer):
    """
    **NotificationSettings Serializer**
        Serializer for NotificationSettings model.

        **fields:**

        * `id`
        * `name`
        * `slug`
        * `type`
    """
    class Meta:
        model = NotificationSettings
        fields = ('id', 'name', 'slug', 'type',)


class NotificationUserSettingsSerializer(serializers.ModelSerializer):
    """
    **NotificationUserSettings Serializer**
        Serializer for NotificationUserSettings model.

        **fields:**

        * `id`
        * `user`
        * `notification`
        * `mode`
    """
    class Meta:
        model = NotificationUserSettings
        fields = ('id', 'user', 'notification', 'mode',)
        read_only_fields = ('user', 'notification',)
