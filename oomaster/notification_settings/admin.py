# encoding: utf-8
from __future__ import unicode_literals

from django.contrib import admin

from notification_settings.models import NotificationSettings,\
    NotificationUserSettings


class NotificationSettingAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'slug', 'type', 'is_choice', 'is_active',)
    list_display_links = ('name',)

    def has_delete_permission(self, request, obj=None):
        return False

admin.site.register(NotificationSettings, NotificationSettingAdmin)


class NotificationUserSettingAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'notification', 'mode',)
    list_display_links = ('id', 'user',)

admin.site.register(NotificationUserSettings, NotificationUserSettingAdmin)
