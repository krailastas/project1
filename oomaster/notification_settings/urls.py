# encoding: utf-8
from __future__ import unicode_literals

from rest_framework.routers import SimpleRouter
from notification_settings.views import NotificationSettingView,\
    NotificationUserSettingsView


router = SimpleRouter(trailing_slash=False)
router.register('notification-settings', NotificationSettingView, base_name='notification_settings')
router.register('notification-user-settings', NotificationUserSettingsView, base_name='notification_user_settings')

urlpatterns = router.urls
