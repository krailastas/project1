from __future__ import unicode_literals

from rest_framework import filters

from notification_settings.models import NotificationUserSettings,\
    NotificationSettings


class FilterNotificationUserSettingsBackend(filters.DjangoFilterBackend):

    class Meta:
        model = NotificationUserSettings
        fields = ['mode']

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterNotificationUserSettingsBackend, self).filter_queryset(request, queryset, view)
        filter_kwargs = {}
        user = request.QUERY_PARAMS.get('user', None)
        if user:
            try:
                filter_kwargs['user_id'] = int(user)
            except:
                filter_kwargs = {}
        mode = request.QUERY_PARAMS.get('mode', None)
        if mode:
            if mode == 'True':
                filter_kwargs['mode'] = True
            elif mode == 'False':
                filter_kwargs['mode'] = False
        if filter_kwargs:
            queryset = queryset.filter(**filter_kwargs)
        return queryset


class FilterNotificationSettingsBackend(filters.DjangoFilterBackend):

    class Meta:
        model = NotificationSettings
        fields = ['slug']

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterNotificationSettingsBackend, self).filter_queryset(request, queryset, view)
        filter_kwargs = {}
        slug = request.QUERY_PARAMS.get('slug', None)
        if slug:
            filter_kwargs['slug'] = slug
        if filter_kwargs:
            queryset = queryset.filter(**filter_kwargs)
        return queryset
