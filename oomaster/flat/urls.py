# encoding: utf-8
from __future__ import unicode_literals

from rest_framework.routers import SimpleRouter

from flat.views import FlatPageView


router = SimpleRouter(trailing_slash=False)
router.register('flat-page', FlatPageView, base_name='flat')

urlpatterns = router.urls
