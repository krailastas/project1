# encoding: utf-8
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _


class FlatPagePublilshedManager(models.Manager):
    def get_query_set(self):
        return super(FlatPagePublilshedManager, self).get_query_set().filter(is_published=True)


class FlatPage(models.Model):
    """
    **FlatPage model**
        Model to store flat pages. Ordering by title.

        **list fields:**

        * `title` - CharField
        * `slug` - SlugField
        * `body_html` - TextField
        * `is_published` - BooleanField
        * `created_at` - DateTimeField
    """
    title = models.CharField(_('title'), max_length=128)
    slug = models.SlugField(_('slug'), unique=True)
    body_html = models.TextField(_('HTML version'))
    is_published = models.BooleanField(_('published'), default=False)
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)

    objects = models.Manager()
    published = FlatPagePublilshedManager()

    class Meta:
        ordering = ['-title']
        verbose_name = _("Flat Page")
        verbose_name_plural = _("Flat Pages")

    def __str__(self):
        return self.title
