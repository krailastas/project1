# encoding: utf-8
from __future__ import unicode_literals

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from flat.factories import FlatPageFactory, FlatPagePublishedFactory
from flat.models import FlatPage


class TestFlatPageViewSet(APITestCase):
    url = reverse('flat-list')

    def setUp(self):
        super(TestFlatPageViewSet, self).setUp()
        FlatPageFactory.create_batch(5)
        FlatPagePublishedFactory.create_batch(5)

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        publish = 0
        for flat in FlatPage.objects.all():
            if flat.is_published:
                self.assertContains(response, flat.slug)
                publish += 1
            else:
                self.assertNotContains(response, flat.slug)
        self.assertEqual(publish, 5)

    def test_post(self):
        data = {
            'title': 'title',
            'slug': 'slug',
            'body_html': 'body_html',
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
