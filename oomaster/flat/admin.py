# encoding: utf-8
from __future__ import unicode_literals

from django.contrib import admin

from ckeditor.widgets import CKEditorWidget

from flat.models import FlatPage


class FlatPageAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'slug', 'is_published')
    list_display_links = ('id', 'title',)
    list_filter = ('is_published',)
    search_fields = ['title', 'slug']

    prepopulated_fields = {'slug': ('title',)}

    def formfield_for_dbfield(self, db_field, **kwargs):
        if db_field.name == 'body_html':
            return db_field.formfield(widget=CKEditorWidget())
        return super(FlatPageAdmin, self).formfield_for_dbfield(db_field, **kwargs)

admin.site.register(FlatPage, FlatPageAdmin)
