# encoding: utf-8
from __future__ import unicode_literals

import factory

from django.contrib.webdesign import lorem_ipsum

from pytils.translit import slugify

from flat.models import FlatPage


class FlatPageFactory(factory.DjangoModelFactory):
    class Meta:
        model = FlatPage

    @factory.lazy_attribute
    def title(self):
        return lorem_ipsum.words(3, False)

    @factory.lazy_attribute
    def slug(self):
        return slugify(self.title)

    @factory.lazy_attribute
    def body_html(self):
        return lorem_ipsum.words(50, False)


class FlatPagePublishedFactory(FlatPageFactory):
    is_published = True
