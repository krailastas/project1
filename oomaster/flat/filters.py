from __future__ import unicode_literals

from rest_framework import filters

from flat.models import FlatPage


class FilterFlatPageBackend(filters.DjangoFilterBackend):

    class Meta:
        model = FlatPage
        fields = ['slug']

    def filter_queryset(self, request, queryset, view):
        queryset = super(FilterFlatPageBackend, self).filter_queryset(request, queryset, view)
        filter_kwargs = {}
        slug = request.QUERY_PARAMS.get('slug', None)
        if slug:
            filter_kwargs['slug'] = slug
        if filter_kwargs:
            queryset = queryset.filter(**filter_kwargs)
        return queryset
