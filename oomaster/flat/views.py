# encoding: utf-8
from __future__ import unicode_literals

from rest_framework.viewsets import ReadOnlyModelViewSet

from flat.models import FlatPage
from flat.serializers import FlatPageSerializer
from flat.filters import FilterFlatPageBackend


class FlatPageView(ReadOnlyModelViewSet):
    """
    **Flat Page views**
        This view returns FlatPage list. Only published pages will be returned.

        **returns:**

        * `title`
        * `slug`
        * `body_html`
        * `created_at`

        **filters:**

        * `?slug=<text>`
    """
    queryset = FlatPage.published.all()
    serializer_class = FlatPageSerializer
    filter_backends = (FilterFlatPageBackend,)
