# encoding: utf-8
from __future__ import unicode_literals

from rest_framework import serializers

from flat.models import FlatPage


class FlatPageSerializer(serializers.ModelSerializer):
    """
    **FlatPage Serializer**
        Serializer for FlatPage model.

        **fields:**

        * `title`
        * `slug`
        * `body_html`
        * `created_at`
    """
    class Meta:
        model = FlatPage
        exclude = ('is_published',)
