from fabric.api import run, env, cd, prefix, task


@task
def test():
    env.hosts = ['']
    env.user = ''
    env.path = ''
    env.branch = ''
    env.database = ''
    env.sv_group = ''

# @task
# def production():
#     env.hosts = ['']
#     env.user = ''
#     env.path = ''
#     env.branch = ''

env.use_ssh_config = True

ENV_COMMAND = 'source ../env/bin/activate'


@task
def manage(command):
    with cd(env.path), prefix(ENV_COMMAND):
        run('python manage.py {} --settings=oomaster.settings.staging'.format(command))


@task
def run_test():
    with cd(env.path), prefix(ENV_COMMAND):
        manage('test')


@task
def update():
    with cd(env.path):
        run('git pull origin {}'.format(env.branch))
        run('find . -name "*.pyc" -exec rm -f {} \;')
        requirements()
        syncdb()
        migrate()
        collectstatic()
        restart()


@task
def requirements():
    with cd(env.path), prefix(ENV_COMMAND):
        run('pip install --exists-action=s -r ../requirements.txt')


@task
def migrate():
    with cd(env.path), prefix(ENV_COMMAND):
        manage('migrate --no-initial-data')


@task
def filldb():
    with cd(env.path), prefix(ENV_COMMAND):
        manage('filldb')


@task
def collectstatic():
    with cd(env.path), prefix(ENV_COMMAND):
        manage('collectstatic --noinput')


@task
def restart():
    run('supervisorctl restart {}:'.format(env.sv_group))


@task
def start():
    run('supervisorctl start {}:'.format(env.sv_group))


@task
def stop():
    run('supervisorctl stop {}:'.format(env.sv_group))


@task
def tail(app):
    run('supervisorctl tail -f {}:{}'.format(env.sv_group, app))


@task
def status():
    run('supervisorctl status')


@task
def cities():
    with cd(env.path), prefix(ENV_COMMAND):
        manage('cities_light')


@task
def rebuilddb():
    dropdb()
    createdb()
    syncdb()
    migrate()
    cities()


@task
def dropdb():
    run('dropdb --if-exists {}'.format(env.database))


@task
def createdb():
    run('createdb {}'.format(env.database))


@task
def syncdb():
    with cd(env.path), prefix(ENV_COMMAND):
        manage('syncdb  --noinput')
