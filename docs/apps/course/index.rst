Course module
==============

===============
``Models``
===============
.. automodule:: course.models
    :members:

=========
``Serializers``
=========
.. automodule:: course.serializers
    :members:

=========
``Views``
=========
.. automodule:: course.views
    :members:
