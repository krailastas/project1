Notification module
==============

===============
``Models``
===============
.. automodule:: notification.models
    :members:

=========
``Serializers``
=========
.. automodule:: notification.serializers
    :members:

=========
``Views``
=========
.. automodule:: notification.views
    :members:

===============
``Permissions``
===============
.. automodule:: notification.permissions
    :members:

===============
``Sockets``
===============
.. automodule:: notification.sockets
    :members:
