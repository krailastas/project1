Custom cities light module
==============

===============
``Serializers``
===============
.. automodule:: custom_cities_light.serializers
    :members:

=========
``Views``
=========
.. automodule:: custom_cities_light.views
    :members:
