Flat Pages module
==============

==========
``Models``
==========
.. automodule:: flat.models
    :members:

===============
``Serializers``
===============
.. automodule:: flat.serializers
    :members:

=========
``Views``
=========
.. automodule:: flat.views
    :members:
