Notification settings module
==============

===============
``Models``
===============
.. automodule:: notification_settings.models
    :members:

=========
``Serializers``
=========
.. automodule:: notification_settings.serializers
    :members:

=========
``Views``
=========
.. automodule:: notification_settings.views
    :members:

===============
``Permissions``
===============
.. automodule:: notification_settings.permissions
    :members:

===========
``Filters``
===========
.. automodule:: notification_settings.filters
    :members:
