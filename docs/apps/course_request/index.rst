Course Request module
==============

===============
``Models``
===============
.. automodule:: course_requets.models
    :members:

=========
``Serializers``
=========
.. automodule:: course_requets.serializers
    :members:

=========
``Views``
=========
.. automodule:: course_requets.views
    :members:
