Welcome to sphinx test's documentation!
=======================================

Contents:

.. toctree::
   :maxdepth: 2

   apps/profiles/index
   apps/course/index
   apps/course_request/index
   apps/notification_settings/index
   apps/flat/index
   apps/custom_cities_light/index
   apps/notification/index
   apps/earning/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
